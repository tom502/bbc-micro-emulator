#include "Interop.h"

namespace Emulator
{
	Interop::Interop()
	{
		beeb = new BBCEM(true);
		assembler = new Assembler();
	}

	Interop::Interop(bool useBASIC)
	{
		beeb = new BBCEM(useBASIC);
		assembler = new Assembler();
	}

	Interop::~Interop()
	{
		delete assembler;
		delete beeb;
	}

	//Reset the "computer"
	void Interop::reset()
	{
		beeb->reset();
		//beeb = new BBCEM();
	}

	//Return all the registers
	array<int ^> ^Interop::getRegisters()
	{
		array<int ^> ^ arr = gcnew array<int ^>(6);

		arr[0] = beeb->mpu.getA();
		arr[1] = beeb->mpu.getX();
		arr[2] = beeb->mpu.getY();
		arr[3] = beeb->mpu.getProgramCounterValue();
		arr[4] = beeb->mpu.getS();
		arr[5] = beeb->mpu.getStatusRegisterValue();

		return arr;
	}

	//Returns total number of cycles and the number of instructions executed
	array<int ^> ^Interop::getTotals()
	{
		array<int ^> ^ arr = gcnew array<int ^>(2);

		arr[0] = beeb->mpu.getTotalCycles();
		arr[1] = beeb->mpu.getTotalExecuted();

		return arr;
	}

	//Returns a block of memory of size 'length' starting from 'loc'
	array<int ^> ^Interop::getMemory(int loc, int length)
	{
		array<int ^> ^ arr = gcnew array<int ^>(length);

		for(int i = 0; i < length; i++)
		{
			if(i == 0xfe40)
			{
				int x = 0;
				x++;
			}
			arr[i] = (int)beeb->mpu.memory[i + loc];
		}
		
		return arr;
	}

	array<int ^> ^Interop::getTrace(int length)
	{
		array<int ^> ^ arr = gcnew array<int ^>(length);

		/*for(int i = 0; i < length; i++)
		{
			arr[i] = (int)beeb->mpu.memory[i + loc];
		}*/
		
		return arr;
	}

	//Get the number of bytes that follow the instruction i
	//Where i is the value of the instruction
	int Interop::getBytesToFollow(int i)
	{
		return (int)beeb->mpu.getBytesToFollow(i);
	}

	//Perform a single CPU Cycle
	void Interop::singleCycle()
	{
		beeb->mpu.cycle();
	}

	int Interop::getCycles()
	{
		return beeb->mpu.getTotalCycles();
	}

	int Interop::timetest()
	{
		return (int)beeb->timeToRunXInstructions(10000000);
	}

	void Interop::toggleBASIC(bool b)
	{
		beeb->toggleBASIC(b);
	}

	void Interop::keyDown(int row, int col)
	{
		beeb->keyDown(row, col);
	}

	void Interop::keyUp(int row, int col)
	{
		beeb->keyUp(row, col);
	}

	void Interop::setThrottle(bool b)
	{
		beeb->setThrottle(b);
	}

	void Interop::assembleThisCode(std::string code)
	{
		assembler->assembleThis(code);
	}
}