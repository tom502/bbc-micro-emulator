#pragma once
#include <BBC Micro.h>
#include <Assembler.h>

namespace Emulator
{
	public ref class Interop
	{
	private:
		Assembler *assembler;
		BBCEM *beeb;
		bool useBASIC;
	public:
		Interop();
		Interop(bool useBASIC);
		array<int ^> ^getRegisters();
		array<int ^> ^getTotals();
		array<int ^> ^getMemory(int loc, int length);
		array<int ^> ^getTrace(int length);
		int getBytesToFollow(int i);
		int getCycles();
		int timetest();
		void singleCycle();
		void reset();
		void toggleBASIC(bool b);
		void keyDown(int row, int col);
		void keyUp(int row, int col);
		void setThrottle(bool b);

		void assembleThisCode(std::string code);
		
		~Interop();
	};

}