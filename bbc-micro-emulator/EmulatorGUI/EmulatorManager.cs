﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emulator;
using System.Threading;

public class EmulatorManager
{
    public Interop m;

    private volatile bool runCPU;

    public bool Running
    {
        get { return runCPU; }
        set { runCPU = value; }
    }
    
    private Thread t;

    public EmulatorManager(bool useBASIC)
    {
        //Don't start the emulation until user wants it
        runCPU = false;
        m = new Interop(useBASIC);
        t = new Thread(new ThreadStart(Boot));
        t.IsBackground = true;
        t.Start();
    }
    
    public void stopCPU()
    {
        runCPU = false;
    }

    public void startCPU()
    {
        runCPU = true;
    }

    //Function to run in separate thread
    public void Boot()
    {
        while(true)
        {
            //When in debugging mode, slow down execution in this thread
            //Give the CPU a rest
            if(runCPU)
            {
                m.singleCycle();
            }
            else
            {
                Thread.Sleep(1000);
            }
        }
    }

    public void singleStep()
    {
        m.singleCycle();
    }

    public void reset()
    {
        bool wasRunning = runCPU;
        //Ensure the CPU stops before resetting values to stop corruption
        stopCPU();
        m.reset();

        if(wasRunning)
        {
            startCPU();
        }
    }

    public int getCycles()
    {
        return m.getCycles();
    }

    public void toggleBASIC(bool b)
    {
        m.toggleBASIC(b);
    }

    public void keyDown(int row, int col)
    {
        m.keyDown(row, col);
    }

    public void keyUp(int row, int col)
    {
        m.keyUp(row, col);
    }

    public void setThrottle(bool b)
    {
        m.setThrottle(b);
    }

}