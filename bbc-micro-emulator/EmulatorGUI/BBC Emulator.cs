﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using xColor = Microsoft.Xna.Framework.Color;
using Microsoft.Xna.Framework.Input;
using xKeyboard = Microsoft.Xna.Framework.Input.Keyboard;
using xKeys = Microsoft.Xna.Framework.Input.Keys;
using Emulator;
using System.Threading;

namespace EmulatorGUI
{
    public partial class BBC_Emulator : Form
    {
        EmulatorManager m;
        Disassembler d;
        MemoryViewer mv;

        xKeys[] keys;
        KeyboardState newState;
        KeyboardState old;

        Keyboard.KeyboardTranslation keyboard;

        float oldCycles;

        public BBC_Emulator()
        {
            InitializeComponent();
            m = new EmulatorManager(loadBASICToolStripMenuItem.Checked);
            d = new Disassembler(ref m);
            OpCodeHelper.initNames();
            oldCycles = 0;
            keyboard = new Keyboard.KeyboardTranslation();
            m.setThrottle(throttle2MzToolStripMenuItem.Checked);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        private void btnMemDump_Click(object sender, EventArgs e)
        {
            mv = new MemoryViewer(ref m);
            mv.Show();
        }

        private void btnDissassemble_Click(object sender, EventArgs e)
        {
            bool b = m.Running;
            m.stopCPU();

            d.ShowEx(b);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            m.reset();
        }

        private void SpeedCheck_Tick(object sender, EventArgs e)
        {
            float c, diff;
            c = m.getCycles();
            //Get the difference between the number of cycles
            diff = (c - oldCycles);
            //Save the number of cycles this time
            oldCycles = c;
            //Work out the speed in MHz
            c = diff / 1000000;
            //Don't allow negatives
            if(c < 0) c = 0;
            lblSpeed.Text = c.ToString("0.00");
        }

        private void BBC_Emulator_Load(object sender, EventArgs e)
        {
            SpeedCheck.Start();
            invTimer.Start();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m.reset();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void debuggerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool b = m.Running;
            m.stopCPU();

            d.ShowEx(b);
        }

        private void memoryDumpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mv = new MemoryViewer(ref m);
            mv.Show();
        }

        private void invTimer_Tick(object sender, EventArgs e)
        {
            int[] vid = MemoryHelper.getVideoMem(ref m.m);
            char[] cVid = new char[Constants.MODE7_LENGTH];
            for(int i = 0; i < Constants.MODE7_LENGTH; i++)
            {
                cVid[i] = (char)vid[i];
            }

            vduControl.updateVideoASCII(cVid);

            if(vduControl != null)
                vduControl.Invalidate();
        }

        private void loadBASICToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m.toggleBASIC(loadBASICToolStripMenuItem.Checked);
        }

        private void UpdateInput()
        {
            bool handled = false;
            newState = xKeyboard.GetState();

            keys = newState.GetPressedKeys();

            foreach(xKeys k in keys)
            {
                if(!old.IsKeyDown(k))
                {
                    //New key being pressed
                    handled = true;
                    if(keyboard.isAcceptableKey(k))
                    {
                        int[] i = keyboard.translateKey(k);
                        if(!d.Focused)
                        {
                            m.keyDown(i[0], i[1]);
                        }
                    }
                }
                else if(old.IsKeyDown(k))
                {
                    //Key being held
                    handled = true;
                }
                //Only take the first key it comes across
                //if(handled)
                //  break;
            }

            if(!handled)
            {
                foreach(xKeys k in old.GetPressedKeys())
                {
                    if(!newState.IsKeyDown(k))
                    {
                        //Key has been released
                        if(keyboard.isAcceptableKey(k))
                        {
                            int[] i = keyboard.translateKey(k);
                            if(!d.Focused)
                            {
                                m.keyUp(i[0], i[1]);
                            }
                        }
                    }
                }
            }
            old = newState;
        }

        private void throttle2MzToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m.setThrottle(throttle2MzToolStripMenuItem.Checked);
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            if(m.Running)
            {
                m.stopCPU();
            }
            else
            {
                m.startCPU();
            }
        }

    }
    public class Constants
    {
        public const int MODE7_START_ADDR = 0x7C00;
        public const int MODE7_LENGTH = 0x400;
    }
}
