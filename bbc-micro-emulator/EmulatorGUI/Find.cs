﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmulatorGUI
{
    public partial class Find : Form
    {
        public Find()
        {
            InitializeComponent();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            int i;

            if(!int.TryParse(txtAddress.Text, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out i))
            {
                lblError.Show();
            }
            else
            {
                if(i > 0xFFFF)
                {
                    lblError.Show();
                }
                else
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }
        }
    }
}
