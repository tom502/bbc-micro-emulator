﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmulatorGUI
{
    public enum AddressingType
    {
        Implied = 0,
        Immediate,
        ZeroPage,
        ZeroPageX,
        ZeroPageY,
        Absolute,
        AbsoluteX,
        AbsoluteY,
        Indirect,
        IndirectX,
        IndirectY,
        Accumulator,
        Relative,
    }

    class OpCodeHelper
    {
        public static Dictionary<int, KeyValuePair<AddressingType, string>> OpCodeNames = new Dictionary<int, KeyValuePair<AddressingType, string>>();
                
        public static void initNames()
        {
            OpCodeNames.Add(0x00, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "BRK"));
	        OpCodeNames.Add(0x01, new KeyValuePair<AddressingType, string>(AddressingType.IndirectX,    "ORA"));
	        OpCodeNames.Add(0x05, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "ORA"));
	        OpCodeNames.Add(0x06, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "ASL"));
	        OpCodeNames.Add(0x08, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "PHP"));
	        OpCodeNames.Add(0x09, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "ORA"));
	        OpCodeNames.Add(0x0A, new KeyValuePair<AddressingType, string>(AddressingType.Accumulator,  "ASL"));
	        OpCodeNames.Add(0x0D, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "ORA"));
	        OpCodeNames.Add(0x0E, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "ASL"));
	        OpCodeNames.Add(0x10, new KeyValuePair<AddressingType, string>(AddressingType.Relative,     "BPL"));
	        OpCodeNames.Add(0x11, new KeyValuePair<AddressingType, string>(AddressingType.IndirectY,    "ORA"));
	        OpCodeNames.Add(0x15, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "ORA"));
	        OpCodeNames.Add(0x16, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "ASL"));
	        OpCodeNames.Add(0x18, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "CLC"));
	        OpCodeNames.Add(0x19, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteY,    "ORA"));
	        OpCodeNames.Add(0x1D, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "ORA"));
	        OpCodeNames.Add(0x1E, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "ASL"));
            OpCodeNames.Add(0x20, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "JSR"));
	        OpCodeNames.Add(0x21, new KeyValuePair<AddressingType, string>(AddressingType.IndirectX,    "AND"));
	        OpCodeNames.Add(0x24, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "BIT"));
	        OpCodeNames.Add(0x25, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "AND"));
	        OpCodeNames.Add(0x26, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "ROL"));
	        OpCodeNames.Add(0x28, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "PLP"));
	        OpCodeNames.Add(0x29, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "AND"));
	        OpCodeNames.Add(0x2A, new KeyValuePair<AddressingType, string>(AddressingType.Accumulator,  "ROL"));
	        OpCodeNames.Add(0x2C, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "BIT"));
	        OpCodeNames.Add(0x2D, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "AND"));
	        OpCodeNames.Add(0x2E, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "ROL"));
	        OpCodeNames.Add(0x30, new KeyValuePair<AddressingType, string>(AddressingType.Relative,     "BMI"));
	        OpCodeNames.Add(0x31, new KeyValuePair<AddressingType, string>(AddressingType.IndirectY,    "AND"));
	        OpCodeNames.Add(0x35, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "AND"));
	        OpCodeNames.Add(0x36, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "ROL"));
	        OpCodeNames.Add(0x38, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "SEC"));
	        OpCodeNames.Add(0x39, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteY,    "AND"));
	        OpCodeNames.Add(0x3D, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "AND"));
	        OpCodeNames.Add(0x3E, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "ROL"));
            OpCodeNames.Add(0x40, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "RTI"));
	        OpCodeNames.Add(0x41, new KeyValuePair<AddressingType, string>(AddressingType.IndirectX,    "EOR"));
	        OpCodeNames.Add(0x45, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "EOR"));
	        OpCodeNames.Add(0x46, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "LSR"));
	        OpCodeNames.Add(0x48, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "PHA"));
	        OpCodeNames.Add(0x49, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "EOR"));
	        OpCodeNames.Add(0x4A, new KeyValuePair<AddressingType, string>(AddressingType.Accumulator,  "LSR"));
	        OpCodeNames.Add(0x4C, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "JMP"));
	        OpCodeNames.Add(0x4D, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "EOR"));
	        OpCodeNames.Add(0x4E, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "LSR"));
	        OpCodeNames.Add(0x50, new KeyValuePair<AddressingType, string>(AddressingType.Relative,     "BVC"));
	        OpCodeNames.Add(0x51, new KeyValuePair<AddressingType, string>(AddressingType.IndirectY,    "EOR"));
	        OpCodeNames.Add(0x55, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "EOR"));
	        OpCodeNames.Add(0x56, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "LSR"));
	        OpCodeNames.Add(0x58, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "CLI"));
	        OpCodeNames.Add(0x59, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteY,    "EOR"));
	        OpCodeNames.Add(0x5D, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "EOR"));
	        OpCodeNames.Add(0x5E, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "LSR"));
	        OpCodeNames.Add(0x60, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "RTS"));
	        OpCodeNames.Add(0x61, new KeyValuePair<AddressingType, string>(AddressingType.IndirectX,    "ADC"));
	        OpCodeNames.Add(0x65, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "ADC"));
            OpCodeNames.Add(0x66, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "ROR"));
	        OpCodeNames.Add(0x68, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "PLA"));
	        OpCodeNames.Add(0x69, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "ADC"));
	        OpCodeNames.Add(0x6A, new KeyValuePair<AddressingType, string>(AddressingType.Accumulator,  "ROR"));
	        OpCodeNames.Add(0x6C, new KeyValuePair<AddressingType, string>(AddressingType.Indirect,     "JMP"));
	        OpCodeNames.Add(0x6D, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "ADC"));
            OpCodeNames.Add(0x6E, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "ROR"));
	        OpCodeNames.Add(0x70, new KeyValuePair<AddressingType, string>(AddressingType.Relative,     "BVS"));
	        OpCodeNames.Add(0x71, new KeyValuePair<AddressingType, string>(AddressingType.IndirectY,    "ADC"));
	        OpCodeNames.Add(0x75, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "ADC"));
	        OpCodeNames.Add(0x76, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "ROR"));
	        OpCodeNames.Add(0x78, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "SEI"));
	        OpCodeNames.Add(0x79, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteY,    "ADC"));
	        OpCodeNames.Add(0x7D, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "ADC"));
            OpCodeNames.Add(0x7E, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "ROR"));
	        OpCodeNames.Add(0x81, new KeyValuePair<AddressingType, string>(AddressingType.IndirectX,    "STA"));
	        OpCodeNames.Add(0x84, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "STY"));
	        OpCodeNames.Add(0x85, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "STA"));
	        OpCodeNames.Add(0x86, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "STX"));
	        OpCodeNames.Add(0x88, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "DEY"));
	        OpCodeNames.Add(0x8A, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "TXA"));
	        OpCodeNames.Add(0x8C, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "STY"));
	        OpCodeNames.Add(0x8D, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "STA"));
	        OpCodeNames.Add(0x8E, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "STX"));
            OpCodeNames.Add(0x90, new KeyValuePair<AddressingType, string>(AddressingType.Relative,     "BCC"));
	        OpCodeNames.Add(0x91, new KeyValuePair<AddressingType, string>(AddressingType.IndirectY,    "STA"));
	        OpCodeNames.Add(0x94, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "STY"));
	        OpCodeNames.Add(0x95, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "STA"));
	        OpCodeNames.Add(0x96, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageY,    "STX"));
	        OpCodeNames.Add(0x98, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "TYA"));
	        OpCodeNames.Add(0x99, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteY,    "STA"));
	        OpCodeNames.Add(0x9A, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "TXS"));
	        OpCodeNames.Add(0x9D, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "STA"));
	        OpCodeNames.Add(0xA0, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "LDY"));
	        OpCodeNames.Add(0xA1, new KeyValuePair<AddressingType, string>(AddressingType.IndirectX,    "LDA"));
	        OpCodeNames.Add(0xA2, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "LDX"));
	        OpCodeNames.Add(0xA4, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "LDY"));
	        OpCodeNames.Add(0xA5, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "LDA"));
	        OpCodeNames.Add(0xA6, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "LDX"));
	        OpCodeNames.Add(0xA8, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "TAY"));
	        OpCodeNames.Add(0xA9, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "LDA"));
	        OpCodeNames.Add(0xAA, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "TAX"));
	        OpCodeNames.Add(0xAC, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "LDY"));
	        OpCodeNames.Add(0xAD, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "LDA"));
	        OpCodeNames.Add(0xAE, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "LDX"));
	        OpCodeNames.Add(0xB0, new KeyValuePair<AddressingType, string>(AddressingType.Relative,     "BCS"));
	        OpCodeNames.Add(0xB1, new KeyValuePair<AddressingType, string>(AddressingType.IndirectY,    "LDA"));
	        OpCodeNames.Add(0xB4, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "LDY"));
	        OpCodeNames.Add(0xB5, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "LDA"));
	        OpCodeNames.Add(0xB6, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageY,    "LDX"));
	        OpCodeNames.Add(0xB8, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "CLV"));
	        OpCodeNames.Add(0xB9, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteY,    "LDA"));
	        OpCodeNames.Add(0xBA, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "TSX"));
	        OpCodeNames.Add(0xBC, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "LDY"));
	        OpCodeNames.Add(0xBD, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "LDA"));
	        OpCodeNames.Add(0xBE, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteY,    "LDX"));
	        OpCodeNames.Add(0xC0, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "CPY"));
	        OpCodeNames.Add(0xC1, new KeyValuePair<AddressingType, string>(AddressingType.IndirectX,    "CMP"));
	        OpCodeNames.Add(0xC4, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "CPY"));
	        OpCodeNames.Add(0xC5, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "CMP"));
	        OpCodeNames.Add(0xC6, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "DEC"));
	        OpCodeNames.Add(0xC8, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "INY"));
	        OpCodeNames.Add(0xC9, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "CMP"));
	        OpCodeNames.Add(0xCA, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "DEX"));
	        OpCodeNames.Add(0xCC, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "CPY"));
	        OpCodeNames.Add(0xCD, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "CMP"));
	        OpCodeNames.Add(0xCE, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "DEC"));
	        OpCodeNames.Add(0xD0, new KeyValuePair<AddressingType, string>(AddressingType.Relative,     "BNE"));
	        OpCodeNames.Add(0xD1, new KeyValuePair<AddressingType, string>(AddressingType.IndirectY,    "CMP"));
	        OpCodeNames.Add(0xD5, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "CMP"));
	        OpCodeNames.Add(0xD6, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "DEC"));
	        OpCodeNames.Add(0xD8, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "CLD"));
	        OpCodeNames.Add(0xD9, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteY,    "CMP"));
	        OpCodeNames.Add(0xDD, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "CMP"));
	        OpCodeNames.Add(0xDE, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "DEC"));
	        OpCodeNames.Add(0xE0, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "CPX"));
	        OpCodeNames.Add(0xE1, new KeyValuePair<AddressingType, string>(AddressingType.IndirectX,    "SBC"));
	        OpCodeNames.Add(0xE4, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "CPX"));
	        OpCodeNames.Add(0xE5, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "SBC"));
	        OpCodeNames.Add(0xE6, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPage,     "INC"));
	        OpCodeNames.Add(0xE8, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "INX"));
	        OpCodeNames.Add(0xE9, new KeyValuePair<AddressingType, string>(AddressingType.Immediate,    "SBC"));
	        OpCodeNames.Add(0xEA, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "NOP"));
	        OpCodeNames.Add(0xEC, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "CPX"));
	        OpCodeNames.Add(0xED, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "SBC"));
	        OpCodeNames.Add(0xEE, new KeyValuePair<AddressingType, string>(AddressingType.Absolute,     "INC"));
	        OpCodeNames.Add(0xF0, new KeyValuePair<AddressingType, string>(AddressingType.Relative,     "BEQ"));
	        OpCodeNames.Add(0xF1, new KeyValuePair<AddressingType, string>(AddressingType.IndirectY,    "SBC"));
	        OpCodeNames.Add(0xF5, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "SBC"));
	        OpCodeNames.Add(0xF6, new KeyValuePair<AddressingType, string>(AddressingType.ZeroPageX,    "INC"));
	        OpCodeNames.Add(0xF8, new KeyValuePair<AddressingType, string>(AddressingType.Implied,      "SED"));
	        OpCodeNames.Add(0xF9, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteY,    "SBC"));
	        OpCodeNames.Add(0xFD, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "SBC"));
	        OpCodeNames.Add(0xFE, new KeyValuePair<AddressingType, string>(AddressingType.AbsoluteX,    "INC"));
        }
    }
}
