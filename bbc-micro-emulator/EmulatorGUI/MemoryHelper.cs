﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emulator;

namespace EmulatorGUI
{
    class MemoryHelper
    {
        static public int[] getMemory(int start, int length, ref Interop m)
        {
            ValueType[] arr;
            arr = m.getMemory(start, length);
            int[] mem = new int[length];
            for (int i = 0; i < length; i++)
            {
                mem[i] = (int)arr[i];
            }
            return mem;
        }

        static public int[] getVideoMem(ref Interop m)
        {
            return getMemory(Constants.MODE7_START_ADDR, Constants.MODE7_LENGTH, ref m);
        }
    }
}
