﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emulator;
using System.Threading;

namespace EmulatorGUI
{
    public partial class Disassembler : Form
    {
        private EmulatorManager m;
        private int statusReg;
        private int addressToWatch;
        private int oldAddressValue;
        private bool watchAddress;
        private bool inHex;
        private bool wasRunning;

        private int x, y, a, pc, s;

        public Disassembler()
        {
            InitializeComponent();
        }

        public Disassembler(ref EmulatorManager m)
        {
            inHex = false;
            this.m = m;
            watchAddress = false;
            
            InitializeComponent();
        }

        private void Disassembler_Load(object sender, EventArgs e)
        {
            setup();
        }

        public void ShowEx(bool r)
        {
            wasRunning = r;
            this.Show();
        }

        private void getTotals()
        {
            ValueType[] arr;
            arr = m.m.getTotals();
            int[] reg = new int[2];
            for(int i = 0; i < 2; i++)
            {
                reg[i] = (int)arr[i];
            }
            lblCycles.Text = reg[0].ToString();
            lblExecuted.Text = reg[1].ToString();
        }

        private void getRegisters()
        {
            ValueType[] arr;
            arr = m.m.getRegisters();
            int[] reg = new int[6];
            for (int i = 0; i < 6; i++)
            {
                reg[i] = (int)arr[i];
            }

            a = reg[0];
            x = reg[1];
            y = reg[2];
            pc = reg[3];
            s = reg[4];
            statusReg = reg[5];
        }

        private void setStatusFlags()
        {
            N.Checked = (statusReg & 128) > 0;
            V.Checked = (statusReg & 64) > 0;
            //5th bit isn't used
            B.Checked = (statusReg & 16) > 0;
            D.Checked = (statusReg & 8) > 0;
            I.Checked = (statusReg & 4) > 0;
            Z.Checked = (statusReg & 2) > 0;
            C.Checked = (statusReg & 1) > 0;
        }

        private void getArgumentSyntax(out string args, int x, int[] arr)
        {
            AddressingType at = OpCodeHelper.OpCodeNames[arr[x]].Key;
            x++;
            args = "none";
            
            if(at == AddressingType.Accumulator)
            {
                args = "A";
            }
            else if(at == AddressingType.Implied)
            {
                args = "";
            }
            if((x + 1) < arr.Length)
            {
                if(at == AddressingType.Absolute)
                {
                    args = "&" + arr[x + 1].ToString("X2") + arr[x].ToString("X2");
                }
                else if(at == AddressingType.AbsoluteY)
                {
                    args = "&" + arr[x + 1].ToString("X2") + arr[x].ToString("X2") + ", Y";
                }
                else if(at == AddressingType.AbsoluteX)
                {
                    args = "&" + arr[x + 1].ToString("X2") + arr[x].ToString("X2") + ", X";
                }
            }
            if(x < arr.Length)
            {
                if(at == AddressingType.Immediate)
                {
                    args = "#&" + arr[x].ToString("X2");
                }
                else if(at == AddressingType.Indirect)
                {
                    args = "(&" + arr[x].ToString("X2") + ")";
                }
                else if(at == AddressingType.IndirectX)
                {
                    args = "(&" + arr[x].ToString("X2") + ", X)";
                }
                else if(at == AddressingType.IndirectY)
                {
                    args = "(&" + arr[x].ToString("X2") + "), Y";
                }
                else if(at == AddressingType.Relative)
                {
                    args = "&" + arr[x].ToString("X2");
                }
                else if(at == AddressingType.ZeroPage)
                {
                    args = "&" + arr[x].ToString("X2");
                }
                else if(at == AddressingType.ZeroPageX)
                {
                    args = "&" + arr[x].ToString("X2") + ", X";
                }
                else if(at == AddressingType.ZeroPageY)
                {
                    args = "&" + arr[x].ToString("X2") + ", Y";
                }
            }
        }

        private void createText(int[] arr)
        {
            StringBuilder sb = new StringBuilder();
            string args = "";
            string instruction;

            for (int i = 0; i < arr.Length; i++)
            {
                if (OpCodeHelper.OpCodeNames.ContainsKey(arr[i]))
                {
                    instruction = OpCodeHelper.OpCodeNames[arr[i]].Value;
                    getArgumentSyntax(out args, i, arr);
                    i += m.m.getBytesToFollow(arr[i]);
                   
                    if(args != "none")
                    {
                        sb.Append(instruction + " " + args);
                        sb.Append("\r\n");
                    }
                }
                else
                {
                    sb.Append(arr[i].ToString("X") + "\r\n");
                }
            }
            txtMemory.Text = sb.ToString();
        }

        private void btnDisassemble_Click(object sender, EventArgs e)
        {
            int start = 0;

            if (chkHex.Checked)
            {
                if(!int.TryParse(txtMem.Text, System.Globalization.NumberStyles.HexNumber, Application.CurrentCulture, out start))
                {
                    MessageBox.Show("Ensure correct number base is selected","Error parsing address");
                }
            }
            else
            {
                if(!int.TryParse(txtMem.Text, out start))
                {
                    MessageBox.Show("Ensure correct number base is selected", "Error parsing address");
                }
            }
            createText(MemoryHelper.getMemory(start, 100, ref m.m));
        }

        private void chkHex_CheckedChanged(object sender, EventArgs e)
        {
            chkDec.Checked = !chkHex.Checked;
            chkBoxesChanged(chkHex.Checked);
        }

        private void chkDec_CheckedChanged(object sender, EventArgs e)
        {
            chkHex.Checked = !chkDec.Checked;
            chkBoxesChanged(chkHex.Checked);
        }

        private void chkBoxesChanged(bool set)
        {
            inHex = set;
            setText();
        }

        private void setText()
        {
            if(inHex)
            {
                lblA.Text = a.ToString("X");
                lblPC.Text = pc.ToString("X");
                lblX.Text = x.ToString("X");
                lblY.Text = y.ToString("X");
                lblS.Text = s.ToString("X");
                txtMem.Text = pc.ToString("X4");
            }
            else
            {
                lblA.Text = a.ToString();
                lblPC.Text = pc.ToString();
                lblX.Text = x.ToString();
                lblY.Text = y.ToString();
                lblS.Text = s.ToString();
                txtMem.Text = pc.ToString();
            }
        }

        private void Disassembler_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(wasRunning)
            {
                m.startCPU();
            }
            e.Cancel = true;
            this.Hide();
        }

        private void Disassembler_VisibleChanged(object sender, EventArgs e)
        {
            if(this.Visible == true)
            {
                setup();
            }
        }

        private void setup()
        {
            getTotals();
            getRegisters();

            if(chkUsePC.Checked)
            {
                createText(MemoryHelper.getMemory(pc, 100, ref m.m));
            }
            else
            {
                if(!inHex)
                {
                    createText(MemoryHelper.getMemory(int.Parse(txtMem.Text), 100, ref m.m));
                }
                else
                {
                    createText(MemoryHelper.getMemory(int.Parse(txtMem.Text, System.Globalization.NumberStyles.HexNumber), 100, ref m.m));
                }
            }
            
            txtMemory.SelectionLength = 0;
            txtMemory.Select(0, 0);
            setStatusFlags();

            if(watchAddress)
            {
                int newVal = (int)m.m.getMemory(addressToWatch, 1)[0];
                if(newVal != oldAddressValue)
                {
                    MessageBox.Show("Value has changed at: " + txtWatch.Text.ToUpper());
                }
                oldAddressValue = newVal;
            }

            setText();
        }

        private void btnStep_Click(object sender, EventArgs e)
        {
            m.singleStep();
            setup();
        }

        private void btnStep10_Click(object sender, EventArgs e)
        {
            step(10);
        }

        private void btnStep100_Click(object sender, EventArgs e)
        {
            step(100);
        }

        private void btnStepCustom_Click(object sender, EventArgs e)
        {
            step((int)nmcStep.Value);
        }

        private void step(int iLoop)
        {
            for(int i = 0; i < iLoop; i++)
            {
                m.singleStep();
            }
            setup();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            int breakpoint = -1, pc;
            ValueType[] arr;

            try
            {
                breakpoint = int.Parse(txtBreakpoint.Text, System.Globalization.NumberStyles.HexNumber);
            }
            catch
            {
                MessageBox.Show("Invalid hex number");
            }

            pc = int.Parse(lblPC.Text);
            arr = m.m.getRegisters();

            while(pc != breakpoint)
            {
                m.singleStep();
                arr = m.m.getRegisters();
                pc = (int)arr[3];
            }

            setup();
        }

        private void chkWatch_CheckedChanged(object sender, EventArgs e)
        {
            watchAddress = chkWatch.Checked;
            try
            {
                addressToWatch = int.Parse(txtWatch.Text, System.Globalization.NumberStyles.HexNumber);
                oldAddressValue = (int)m.m.getMemory(addressToWatch, 1)[0];
            }
            catch
            {
                MessageBox.Show("Invalid hex number");
            }
        }

        private void chkUsePC_CheckedChanged(object sender, EventArgs e)
        {
            btnDisassemble.Enabled = txtMem.Enabled = !chkUsePC.Checked;

            if(chkUsePC.Checked)
            {
                setup();
            }
        }
    }
}
