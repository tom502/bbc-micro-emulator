﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emulator;

namespace EmulatorGUI
{
    public partial class MemoryViewer : Form
    {
        EmulatorManager m;
        int[] _mem;
        char[] chars = new char[256];
        StringBuilder sb;
        Find f;

        public MemoryViewer()
        {
            InitializeComponent();
        }

        public MemoryViewer(ref EmulatorManager m)
        {
            this.m = m;
            sb = new StringBuilder();
            f = new Find();
            InitializeComponent();

            
        }

        private void MemoryViewer_Load(object sender, EventArgs e)
        {
            for(int i = 0; i < 256; i++)
            {
                if(i < 0x20 || i > 127)
                {
                    chars[i] = '.';
                }
                else
                {
                    chars[i] = (char)i;
                }
            }
            displayMemory();
        }

        void displayMemory()
        {
            _mem = MemoryHelper.getMemory(0, 65536, ref m.m);
            buildString();
            txtMemory.Text = sb.ToString();
        }

        private void buildString()
        {
            string lineChars = " |";

            sb.Append(0.ToString("X4") + " ");
                
            for (int i = 0; i < _mem.Length; i++)
            {
                lineChars += chars[_mem[i]];
                sb.Append(_mem[i].ToString("X2"));
                sb.Append("  ");
                
                if ((i+1) % 4 == 0)
                {
                    sb.Append("     ");
                }

                if ((i+1) % 16 == 0)
                {
                    sb.Append(lineChars + "|");
                    sb.Append(" \r\n");
                    if(i < 0xFFFF)
                    {
                        sb.Append((i + 1).ToString("X4") + " ");
                    }
                    lineChars = " |";
                }
            }
        }

        private void MemoryViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void MemoryViewer_VisibleChanged(object sender, EventArgs e)
        {
            //if(this.Visible)
            //{
            //    displayMemory();
            //}
        }

        private void txtMemory_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Control && e.KeyCode == Keys.F)
            {
                if(f.ShowDialog() == DialogResult.OK)
                {
                    int lineLength = 110;
                    string address = f.txtAddress.Text.ToUpper();
                    while(address.Length < 4)
                    {
                        address = address.Insert(0, "0");
                    }

                    //If it's the first value of the line - the whole line will be highlighted
                    if(address.EndsWith("0"))
                    {
                        txtMemory.Select(txtMemory.Text.IndexOf(address), lineLength);
                    }
                    else
                    {
                        int idx = 7;
                        int mod = int.Parse(address.Substring(0,3), NumberStyles.HexNumber);
                        int col = int.Parse(address[3].ToString(), NumberStyles.HexNumber);

                        //Work out the index of the value to highlight
                        //Account for the columns 2 characters wide, then the 5 character columns
                        //then the line number
                        idx += ((col * 2 - 1) * 2) + ((col / 4) * 5) + ((lineLength + 1) * mod);
                        
                        txtMemory.Select(idx, 2);
                    }

                    txtMemory.ScrollToCaret();
                }
            }
        }
    }
}
