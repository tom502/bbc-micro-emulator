﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace EmulatorGUI.Keyboard
{

    public class BeebKey
    {
        //The position in the table in the Micro
        int row, col;
        //Whether this references the "shifted" key or not
        bool shift;
        //The actual key pressed
        Keys key;

        public BeebKey(int row, int col, Keys key, bool shift)
        {
            this.row = row;
            this.col = col;
            this.key = key;
        }

        public bool Shift
        {
            get
            {
                return shift;
            }
            set
            {
                shift = value;
            }
        }

        public int Row
        {
            get
            {
                return row;
            }
            set
            {
                row = value;
            }
        }

        public int Col
        {
            get
            {
                return col;
            }
            set
            {
                col = value;
            }
        }

        public Keys Key
        {
            get
            {
                return key;
            }
            set
            {
                key = value;
            }
        }
    }

    public class AcceptableKeys : IEnumerable
    {

        //List of all keys that we will translate
        private BeebKey[] keys = new BeebKey[255];
        int r = -1, c = -1;

        public AcceptableKeys()
        {
            //Initiate with "dumb" values
            for(int i = 0; i < 255; i++)
            {
                keys[i] = new BeebKey(-1, -1, Keys.F12, false);
            }
            /* Key      Row     Col     XNA
             * A         4       1      65
             * B         6       4      66
             * C         5       2      67
             * D         3       2      68
             * E         2       2      69
             * F         4       3      70
             * G         5       3      71
             * H         5       4      72
             * I         2       5      73
             * J         4       5      74
             * K         4       6      75
             * L         5       6      76
             * M         6       5      77
             * N         5       5      78
             * O         3       6      79
             * P         3       7      80
             * Q         1       0      81
             * R         3       3      82
             * S         5       1      83
             * T         2       3      84
             * U         3       5      85
             * V         6       3      86
             * W         2       1      87
             * X         4       2      88
             * Y         4       4      89
             * Z         6       1      90
             */

            //Add Alpha Keys
            for(int i = 0, j = 65; i < 26; i++, j++)
            {
                #region Row 4 Keys
                if(j == 65 || j == 70 || j == 74 || j == 75 || j == 88 || j == 89)
                {
                    r = 4;
                    if(j == 65)
                    {
                        c = 1;
                    }
                    else if(j == 70)
                    {
                        c = 3;
                    }
                    else if(j == 74)
                    {
                        c = 5;
                    }
                    else if(j == 75)
                    {
                        c = 6;
                    }
                    else if(j == 88)
                    {
                        c = 2;
                    }
                    else if(j == 89)
                    {
                        c = 4;
                    }
                }
                #endregion
                #region Row 6 Keys
                else if(j == 66 || j == 77 || j == 86 || j == 90)
                {
                    r = 6;
                    if(j == 66)
                    {
                        c = 4;
                    }
                    else if(j == 77)
                    {
                        c = 5;
                    }
                    else if(j == 86)
                    {
                        c = 3;
                    }
                    else if(j == 90)
                    {
                        c = 1;
                    }
                }
                #endregion
                #region Row 5 Keys
                else if(j == 67 || j == 71 || j == 72 || j == 76 || j == 78 || j == 83)
                {
                    r = 5;
                    if(j == 67)
                    {
                        c = 2;
                    }
                    else if(j == 71)
                    {
                        c = 3;
                    }
                    else if(j == 72)
                    {
                        c = 4;
                    }
                    else if(j == 76)
                    {
                        c = 6;
                    }
                    else if(j == 78)
                    {
                        c = 5;
                    }
                    else if(j == 83)
                    {
                        c = 1;
                    }
                }
                #endregion
                #region Row 3 Keys
                else if(j == 68 || j == 79 || j == 80 || j == 82 || j == 85)
                {
                    r = 3;
                    if(j == 68)
                    {
                        c = 2;
                    }
                    else if(j == 79)
                    {
                        c = 6;
                    }
                    else if(j == 80)
                    {
                        c = 7;
                    }
                    else if(j == 82)
                    {
                        c = 3;
                    }
                    else if(j == 85)
                    {
                        c = 5;
                    }
                }
                #endregion
                #region Row 2 Keys
                else if(j == 69 || j == 73 || j == 84 || j == 87)
                {
                    r = 2;
                    if(j == 69)
                    {
                        c = 2;
                    }
                    else if(j == 73)
                    {
                        c = 5;
                    }
                    else if(j == 84)
                    {
                        c = 3;
                    }
                    else if(j == 87)
                    {
                        c = 1;
                    }
                }
                #endregion
                #region Row 1 Key
                else if(j == 81)
                {
                    r = 1;
                    c = 0;
                }
                #endregion
                keys[j] = new BeebKey(r, c, (Keys)j, false);
                //keys[i + 1] = new BeebKey(0, 0, (Keys)j, true);
            }

            /* Key      Row     Col     XNA
             * 0        2       7       48
             * 1        3       0       49
             * 2        3       1       50
             * 3        1       1       51
             * 4        1       2       52
             * 5        1       3       53
             * 6        3       4       54
             * 7        2       4       55
             * 8        1       5       56
             * 9        2       6       57
             */

            r = c = -1;

            //Add Numerical Keys
            for(int i = 0, j = 48; i < 10; i++, j++)
            {
                #region Row 2 Keys
                if(j == 48 || j == 55 || j == 57)
                {
                    r = 2;
                    if(j == 48)
                    {
                        c = 7;
                    }
                    else if(j == 55)
                    {
                        c = 4;
                    }
                    else if(j == 57)
                    {
                        c = 6;
                    }
                }
                #endregion
                #region Row 3 Keys
                else if(j == 49 || j == 50 || j == 54)
                {
                    r = 3;
                    if(j == 49)
                    {
                        c = 0;
                    }
                    else if(j == 50)
                    {
                        c = 1;
                    }
                    else if(j == 54)
                    {
                        c = 4;
                    }
                }
                #endregion
                #region Row 1 Keys
                else if(j == 51 || j == 52 || j == 53 || j == 56)
                {
                    r = 1;
                    c = j - 50;
                    if(j == 56)
                    {
                        c--;
                    }
                }
                #endregion
                keys[j] = new BeebKey(r, c, (Keys)j, false);
                //keys[i + 1] = new BeebKey(0, 0, (Keys)j, true);
            }

            /* Key      Row     Col       XNA
             * F0        2       0      F1 - 112
             * F1        7       1      F2 - 113
             * F2        7       2      F3 - 114
             * F3        7       3      F4 - 115
             * F4        1       4      F5 - 116
             * F5        7       4      F6 - 117
             * F6        7       5      F7 - 118
             * F7        1       6      F8 - 119
             * F8        7       6      F9 - 120
             * F9        7       7      F10 - 121 
             */

            r = c = -1;

            //Add function keys F1-F10 (f0 - f9 on the Micro)
            for(int i = 0, j = 112; i < 10; i++, j++)
            {
                #region Set r and c
                if(j == 112)
                {
                    r = 2;
                    c = 0;
                }
                else if(j == 113 || j == 114 || j == 115)
                {
                    r = 7;
                    c = j - 112;
                }
                else if(j == 116)
                {
                    r = 1;
                    c = 4;
                }
                else if(j == 117 || j == 118)
                {
                    r = 7;
                    c = j - 113;
                }
                else if(j == 119)
                {
                    r = 1;
                    c = 6;
                }
                else if(j == 120 || j == 121)
                {
                    r = 7;
                    c = j - 114;
                }
                #endregion
                keys[j] = new BeebKey(r, c, (Keys)j, false);
            }

            keys[92] = new BeebKey(0, 0, Keys.LeftShift, false);
            keys[93] = new BeebKey(0, 0, Keys.RightShift, false);
            keys[94] = new BeebKey(0, 1, Keys.LeftControl, false);
            keys[95] = new BeebKey(6, 2, Keys.Space, false);
            keys[96] = new BeebKey(6, 0, Keys.Tab, false);
            keys[97] = new BeebKey(7, 0, Keys.Escape, false);
            keys[98] = new BeebKey(5, 9, Keys.Delete, false);
            //Use Right control as Copy
            keys[99] = new BeebKey(6, 9, Keys.RightControl, false);
            //Use Left Alt as Shift Lock
            keys[100] = new BeebKey(5, 0, Keys.LeftAlt, false);
            keys[101] = new BeebKey(4, 0, Keys.CapsLock, false);
            keys[102] = new BeebKey(4, 9, Keys.Enter, false);
            keys[103] = new BeebKey(3, 9, Keys.Up, false);
            keys[104] = new BeebKey(2, 9, Keys.Down, false);
            keys[105] = new BeebKey(1, 9, Keys.Left, false);
            keys[106] = new BeebKey(7, 9, Keys.Right, false);
            //Pause is break key
            //keys[107] = new BeebKey(0, 0, Keys.Pause, false);


            //Symbol keys get complicated as XNA doesn't even pick up the correct "name" for the key
            /*keys[62] = Keys.OemComma;
            keys[63] = Keys.OemPeriod;
            keys[64] = Keys.OemQuestion;
            keys[65] = Keys.OemSemicolon;
            //This tilde is actuall the @ ' key
            keys[66] = Keys.OemTilde;
            //This quotes is actuall the ~ # key
            keys[67] = Keys.OemQuotes;
            keys[68] = Keys.OemPlus;
            keys[69] = Keys.OemPipe;
            keys[70] = Keys.OemBackslash;
            keys[71] = Keys.OemCloseBrackets;
            keys[72] = Keys.OemOpenBrackets;*/
        }

        public IEnumerator GetEnumerator()
        {
            for(int i = 0; i < keys.Length; i++)
            {
                yield return keys[i].Key;
            }
        }

        public BeebKey this[Keys k]
        {
            get { return keys[(int)k]; }
        }
    }

    class KeyboardTranslation
    {
        public AcceptableKeys acceptableKeys;

        public KeyboardTranslation()
        {
            acceptableKeys = new AcceptableKeys();

        }

        public int[] translateKey(Keys k)
        {
            int[] ret = new int[2];

            ret[0] = acceptableKeys[k].Row;
            ret[1] = acceptableKeys[k].Col;

            return ret;
        }

        public bool isAcceptableKey(Keys k)
        {
            bool ret = false;

            foreach(Keys n in acceptableKeys)
            {
                if(k == n)
                {
                    ret = true;
                    break;
                }
            }

            return ret;
        }
    }
}
