using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using xKeys = Microsoft.Xna.Framework.Input.Keys;
using System;
using System.Windows.Forms;
using System.IO;

namespace EmulatorGUI
{
    /*
     * Example control inherits from GraphicsDeviceControl, which allows it to
     * render using a GraphicsDevice. This control shows how to use ContentManager
     * inside a WinForms application. It loads a SpriteFont object through the
     * ContentManager, then uses a SpriteBatch to draw text. The control is not
     * animated, so it only redraws itself in response to WinForms paint messages.
    */
    class VDUControl : GraphicsDeviceControl
    {
        ContentManager content;
        SpriteBatch spriteBatch;
        SpriteFont font;

        char[] videoASCII;
        string toDraw;

        /// <summary>
        /// Initializes the control, creating the ContentManager
        /// and using it to load a SpriteFont.
        /// </summary>
        protected override void Initialize()
        {
            content = new ContentManager(this.Services, Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Content\\bin"));
            content.Unload();

            font = content.Load<SpriteFont>("font");
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            videoASCII = new char[Constants.MODE7_LENGTH];
        }
        
        /// <summary>
        /// Disposes the control, unloading the ContentManager.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                content.Unload();
            }

            base.Dispose(disposing);
        }


        /// <summary>
        /// Draws the control, using SpriteBatch and SpriteFont.
        /// </summary>
        protected override void Draw()
        {
            if(this.Parent.ContainsFocus)
            {
                //UpdateInput();
                GraphicsDevice.Clear(Color.Black);
                Texture2D blank = new Texture2D(GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
                //blank.SetData(new[] { Color.White });
                spriteBatch.Begin();
                toDraw = new string(videoASCII);
                insertNewLines();
                //Seems to break the device when trying to draw "\0"
                if(!toDraw.Contains("\0"))
                    spriteBatch.DrawString(font, toDraw, new Vector2(0.0f, 0.0f), Color.White);
                
                spriteBatch.End();
            }
        }

        private void insertNewLines()
        {
            //Characters per line
            int x = 40;
            //How many times to loop through
            int s = toDraw.Length / x;
            for(int i = 0; i < s; i++)
            {
                toDraw = toDraw.Insert((i * x) + i, "\n");
            }
        }

        //If I need the drawline function back
        //http://www.xnawiki.com/index.php?title=Drawing_2D_lines_without_using_primitives
        
        public void updateVideoASCII(char[] c)
        {
            videoASCII = c;
        }
    }
}
