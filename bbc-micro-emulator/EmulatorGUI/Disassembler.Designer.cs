﻿namespace EmulatorGUI
{
    partial class Disassembler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblA = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.lblX = new System.Windows.Forms.Label();
            this.txtMemory = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblS = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblPC = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.txtMem = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkDec = new System.Windows.Forms.CheckBox();
            this.chkHex = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkUsePC = new System.Windows.Forms.CheckBox();
            this.btnDisassemble = new System.Windows.Forms.Button();
            this.btnStep = new System.Windows.Forms.Button();
            this.btnStep10 = new System.Windows.Forms.Button();
            this.btnStep100 = new System.Windows.Forms.Button();
            this.btnStepCustom = new System.Windows.Forms.Button();
            this.nmcStep = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblExecuted = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lblCycles = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.btnGo = new System.Windows.Forms.Button();
            this.txtBreakpoint = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.N = new System.Windows.Forms.RadioButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.Z = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.B = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.D = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.I = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.V = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.C = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.boxWatch = new System.Windows.Forms.GroupBox();
            this.chkWatch = new System.Windows.Forms.CheckBox();
            this.txtWatch = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmcStep)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.boxWatch.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Location = new System.Drawing.Point(51, 22);
            this.lblA.Name = "lblA";
            this.lblA.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblA.Size = new System.Drawing.Size(13, 13);
            this.lblA.TabIndex = 0;
            this.lblA.Text = "0";
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(51, 64);
            this.lblY.Name = "lblY";
            this.lblY.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblY.Size = new System.Drawing.Size(13, 13);
            this.lblY.TabIndex = 2;
            this.lblY.Text = "0";
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(51, 43);
            this.lblX.Name = "lblX";
            this.lblX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblX.Size = new System.Drawing.Size(13, 13);
            this.lblX.TabIndex = 3;
            this.lblX.Text = "0";
            // 
            // txtMemory
            // 
            this.txtMemory.BackColor = System.Drawing.Color.White;
            this.txtMemory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMemory.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMemory.Location = new System.Drawing.Point(12, 13);
            this.txtMemory.Multiline = true;
            this.txtMemory.Name = "txtMemory";
            this.txtMemory.ReadOnly = true;
            this.txtMemory.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMemory.Size = new System.Drawing.Size(163, 349);
            this.txtMemory.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "X:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Y:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Acc:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.lblS);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lblPC);
            this.groupBox1.Controls.Add(this.lblX);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblA);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblY);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.shapeContainer1);
            this.groupBox1.Location = new System.Drawing.Point(12, 367);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(92, 131);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registers";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "S:";
            // 
            // lblS
            // 
            this.lblS.AutoSize = true;
            this.lblS.Location = new System.Drawing.Point(51, 105);
            this.lblS.Name = "lblS";
            this.lblS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblS.Size = new System.Drawing.Size(13, 13);
            this.lblS.TabIndex = 11;
            this.lblS.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "PC:";
            // 
            // lblPC
            // 
            this.lblPC.AutoSize = true;
            this.lblPC.Location = new System.Drawing.Point(51, 85);
            this.lblPC.Name = "lblPC";
            this.lblPC.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblPC.Size = new System.Drawing.Size(13, 13);
            this.lblPC.TabIndex = 8;
            this.lblPC.Text = "0";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape4,
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(86, 112);
            this.shapeContainer1.TabIndex = 10;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 10;
            this.lineShape4.X2 = 80;
            this.lineShape4.Y1 = 84;
            this.lineShape4.Y2 = 84;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 11;
            this.lineShape3.X2 = 81;
            this.lineShape3.Y1 = 63;
            this.lineShape3.Y2 = 63;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 10;
            this.lineShape2.X2 = 80;
            this.lineShape2.Y1 = 44;
            this.lineShape2.Y2 = 44;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 10;
            this.lineShape1.X2 = 80;
            this.lineShape1.Y1 = 23;
            this.lineShape1.Y2 = 23;
            // 
            // txtMem
            // 
            this.txtMem.Enabled = false;
            this.txtMem.Location = new System.Drawing.Point(9, 32);
            this.txtMem.Name = "txtMem";
            this.txtMem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtMem.Size = new System.Drawing.Size(83, 20);
            this.txtMem.TabIndex = 9;
            this.txtMem.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Location:";
            // 
            // chkDec
            // 
            this.chkDec.AutoSize = true;
            this.chkDec.Checked = true;
            this.chkDec.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDec.Location = new System.Drawing.Point(8, 59);
            this.chkDec.Name = "chkDec";
            this.chkDec.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkDec.Size = new System.Drawing.Size(85, 17);
            this.chkDec.TabIndex = 11;
            this.chkDec.Text = "       Decimal";
            this.chkDec.UseVisualStyleBackColor = true;
            this.chkDec.CheckedChanged += new System.EventHandler(this.chkDec_CheckedChanged);
            // 
            // chkHex
            // 
            this.chkHex.AutoSize = true;
            this.chkHex.Location = new System.Drawing.Point(9, 78);
            this.chkHex.Name = "chkHex";
            this.chkHex.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkHex.Size = new System.Drawing.Size(84, 17);
            this.chkHex.TabIndex = 12;
            this.chkHex.Text = "             Hex";
            this.chkHex.UseVisualStyleBackColor = true;
            this.chkHex.CheckedChanged += new System.EventHandler(this.chkHex_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkUsePC);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.chkHex);
            this.groupBox2.Controls.Add(this.txtMem);
            this.groupBox2.Controls.Add(this.chkDec);
            this.groupBox2.Controls.Add(this.btnDisassemble);
            this.groupBox2.Location = new System.Drawing.Point(181, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(104, 174);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Settings";
            // 
            // chkUsePC
            // 
            this.chkUsePC.Checked = true;
            this.chkUsePC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUsePC.Location = new System.Drawing.Point(9, 98);
            this.chkUsePC.Name = "chkUsePC";
            this.chkUsePC.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkUsePC.Size = new System.Drawing.Size(84, 21);
            this.chkUsePC.TabIndex = 15;
            this.chkUsePC.Text = "       Use PC";
            this.chkUsePC.UseVisualStyleBackColor = true;
            this.chkUsePC.CheckedChanged += new System.EventHandler(this.chkUsePC_CheckedChanged);
            // 
            // btnDisassemble
            // 
            this.btnDisassemble.Enabled = false;
            this.btnDisassemble.Location = new System.Drawing.Point(7, 141);
            this.btnDisassemble.Name = "btnDisassemble";
            this.btnDisassemble.Size = new System.Drawing.Size(91, 23);
            this.btnDisassemble.TabIndex = 14;
            this.btnDisassemble.Text = "Disassemble";
            this.btnDisassemble.UseVisualStyleBackColor = true;
            this.btnDisassemble.Click += new System.EventHandler(this.btnDisassemble_Click);
            // 
            // btnStep
            // 
            this.btnStep.Location = new System.Drawing.Point(7, 58);
            this.btnStep.Name = "btnStep";
            this.btnStep.Size = new System.Drawing.Size(91, 23);
            this.btnStep.TabIndex = 15;
            this.btnStep.Text = "Single Step";
            this.btnStep.UseVisualStyleBackColor = true;
            this.btnStep.Click += new System.EventHandler(this.btnStep_Click);
            // 
            // btnStep10
            // 
            this.btnStep10.Location = new System.Drawing.Point(6, 86);
            this.btnStep10.Name = "btnStep10";
            this.btnStep10.Size = new System.Drawing.Size(91, 23);
            this.btnStep10.TabIndex = 16;
            this.btnStep10.Text = "Step 10";
            this.btnStep10.UseVisualStyleBackColor = true;
            this.btnStep10.Click += new System.EventHandler(this.btnStep10_Click);
            // 
            // btnStep100
            // 
            this.btnStep100.Location = new System.Drawing.Point(6, 115);
            this.btnStep100.Name = "btnStep100";
            this.btnStep100.Size = new System.Drawing.Size(91, 23);
            this.btnStep100.TabIndex = 17;
            this.btnStep100.Text = "Step 100";
            this.btnStep100.UseVisualStyleBackColor = true;
            this.btnStep100.Click += new System.EventHandler(this.btnStep100_Click);
            // 
            // btnStepCustom
            // 
            this.btnStepCustom.Location = new System.Drawing.Point(6, 144);
            this.btnStepCustom.Name = "btnStepCustom";
            this.btnStepCustom.Size = new System.Drawing.Size(91, 23);
            this.btnStepCustom.TabIndex = 18;
            this.btnStepCustom.Text = "Step";
            this.btnStepCustom.UseVisualStyleBackColor = true;
            this.btnStepCustom.Click += new System.EventHandler(this.btnStepCustom_Click);
            // 
            // nmcStep
            // 
            this.nmcStep.Location = new System.Drawing.Point(100, 147);
            this.nmcStep.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.nmcStep.Name = "nmcStep";
            this.nmcStep.Size = new System.Drawing.Size(66, 20);
            this.nmcStep.TabIndex = 19;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblExecuted);
            this.groupBox3.Controls.Add(this.lbl5);
            this.groupBox3.Controls.Add(this.lblCycles);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.shapeContainer2);
            this.groupBox3.Location = new System.Drawing.Point(292, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(129, 68);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Totals";
            // 
            // lblExecuted
            // 
            this.lblExecuted.AutoSize = true;
            this.lblExecuted.Location = new System.Drawing.Point(59, 43);
            this.lblExecuted.Name = "lblExecuted";
            this.lblExecuted.Size = new System.Drawing.Size(13, 13);
            this.lblExecuted.TabIndex = 3;
            this.lblExecuted.Text = "0";
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Location = new System.Drawing.Point(7, 43);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(54, 13);
            this.lbl5.TabIndex = 7;
            this.lbl5.Text = "Ins. Exec:";
            // 
            // lblCycles
            // 
            this.lblCycles.AutoSize = true;
            this.lblCycles.Location = new System.Drawing.Point(59, 22);
            this.lblCycles.Name = "lblCycles";
            this.lblCycles.Size = new System.Drawing.Size(13, 13);
            this.lblCycles.TabIndex = 0;
            this.lblCycles.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "Cycles:";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape8});
            this.shapeContainer2.Size = new System.Drawing.Size(123, 49);
            this.shapeContainer2.TabIndex = 10;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape8
            // 
            this.lineShape8.Name = "lineShape1";
            this.lineShape8.X1 = 5;
            this.lineShape8.X2 = 119;
            this.lineShape8.Y1 = 23;
            this.lineShape8.Y2 = 23;
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(6, 29);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(98, 23);
            this.btnGo.TabIndex = 20;
            this.btnGo.Text = "Set Breakpoint";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // txtBreakpoint
            // 
            this.txtBreakpoint.Location = new System.Drawing.Point(110, 32);
            this.txtBreakpoint.Name = "txtBreakpoint";
            this.txtBreakpoint.Size = new System.Drawing.Size(44, 20);
            this.txtBreakpoint.TabIndex = 22;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnStep10);
            this.groupBox4.Controls.Add(this.btnStep);
            this.groupBox4.Controls.Add(this.txtBreakpoint);
            this.groupBox4.Controls.Add(this.btnStep100);
            this.groupBox4.Controls.Add(this.btnStepCustom);
            this.groupBox4.Controls.Add(this.btnGo);
            this.groupBox4.Controls.Add(this.nmcStep);
            this.groupBox4.Location = new System.Drawing.Point(181, 196);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(198, 180);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Debugging";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.panel1);
            this.groupBox5.Controls.Add(this.panel7);
            this.groupBox5.Controls.Add(this.panel6);
            this.groupBox5.Controls.Add(this.panel5);
            this.groupBox5.Controls.Add(this.panel3);
            this.groupBox5.Controls.Add(this.panel2);
            this.groupBox5.Controls.Add(this.panel4);
            this.groupBox5.Location = new System.Drawing.Point(110, 382);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(284, 46);
            this.groupBox5.TabIndex = 25;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Status Register";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.N);
            this.panel1.Location = new System.Drawing.Point(1, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(42, 27);
            this.panel1.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "N:";
            // 
            // N
            // 
            this.N.AutoSize = true;
            this.N.Enabled = false;
            this.N.Location = new System.Drawing.Point(24, 6);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(14, 13);
            this.N.TabIndex = 7;
            this.N.TabStop = true;
            this.N.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.Z);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Location = new System.Drawing.Point(205, 14);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(42, 27);
            this.panel7.TabIndex = 20;
            // 
            // Z
            // 
            this.Z.AutoSize = true;
            this.Z.Enabled = false;
            this.Z.Location = new System.Drawing.Point(21, 6);
            this.Z.Name = "Z";
            this.Z.Size = new System.Drawing.Size(14, 13);
            this.Z.TabIndex = 12;
            this.Z.TabStop = true;
            this.Z.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 6);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Z:";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.B);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Location = new System.Drawing.Point(80, 14);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(42, 27);
            this.panel6.TabIndex = 19;
            // 
            // B
            // 
            this.B.AutoSize = true;
            this.B.Enabled = false;
            this.B.Location = new System.Drawing.Point(22, 6);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(14, 13);
            this.B.TabIndex = 9;
            this.B.TabStop = true;
            this.B.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "B:";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.D);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Location = new System.Drawing.Point(122, 14);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(42, 27);
            this.panel5.TabIndex = 18;
            // 
            // D
            // 
            this.D.AutoSize = true;
            this.D.Enabled = false;
            this.D.Location = new System.Drawing.Point(23, 6);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(14, 13);
            this.D.TabIndex = 10;
            this.D.TabStop = true;
            this.D.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "D:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.I);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Location = new System.Drawing.Point(165, 14);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(42, 27);
            this.panel3.TabIndex = 16;
            // 
            // I
            // 
            this.I.AutoSize = true;
            this.I.Enabled = false;
            this.I.Location = new System.Drawing.Point(19, 6);
            this.I.Name = "I";
            this.I.Size = new System.Drawing.Size(14, 13);
            this.I.TabIndex = 11;
            this.I.TabStop = true;
            this.I.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "I:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.V);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(43, 14);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(37, 27);
            this.panel2.TabIndex = 15;
            // 
            // V
            // 
            this.V.AutoSize = true;
            this.V.Enabled = false;
            this.V.Location = new System.Drawing.Point(22, 6);
            this.V.Name = "V";
            this.V.Size = new System.Drawing.Size(14, 13);
            this.V.TabIndex = 8;
            this.V.TabStop = true;
            this.V.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "V:";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.C);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Location = new System.Drawing.Point(246, 14);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(36, 27);
            this.panel4.TabIndex = 17;
            // 
            // C
            // 
            this.C.AutoSize = true;
            this.C.Enabled = false;
            this.C.Location = new System.Drawing.Point(19, 6);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(14, 13);
            this.C.TabIndex = 13;
            this.C.TabStop = true;
            this.C.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(2, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "C:";
            // 
            // boxWatch
            // 
            this.boxWatch.Controls.Add(this.chkWatch);
            this.boxWatch.Controls.Add(this.txtWatch);
            this.boxWatch.Location = new System.Drawing.Point(294, 91);
            this.boxWatch.Name = "boxWatch";
            this.boxWatch.Size = new System.Drawing.Size(100, 99);
            this.boxWatch.TabIndex = 26;
            this.boxWatch.TabStop = false;
            this.boxWatch.Text = "Watch";
            // 
            // chkWatch
            // 
            this.chkWatch.AutoSize = true;
            this.chkWatch.Location = new System.Drawing.Point(6, 47);
            this.chkWatch.Name = "chkWatch";
            this.chkWatch.Size = new System.Drawing.Size(77, 30);
            this.chkWatch.TabIndex = 1;
            this.chkWatch.Text = "Watch this\r\naddress";
            this.chkWatch.UseVisualStyleBackColor = true;
            this.chkWatch.CheckedChanged += new System.EventHandler(this.chkWatch_CheckedChanged);
            // 
            // txtWatch
            // 
            this.txtWatch.Location = new System.Drawing.Point(8, 21);
            this.txtWatch.Name = "txtWatch";
            this.txtWatch.Size = new System.Drawing.Size(86, 20);
            this.txtWatch.TabIndex = 0;
            // 
            // Disassembler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 510);
            this.Controls.Add(this.boxWatch);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtMemory);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Disassembler";
            this.Text = "Disassembler";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Disassembler_FormClosing);
            this.Load += new System.EventHandler(this.Disassembler_Load);
            this.VisibleChanged += new System.EventHandler(this.Disassembler_VisibleChanged);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmcStep)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.boxWatch.ResumeLayout(false);
            this.boxWatch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.TextBox txtMemory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtMem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkDec;
        private System.Windows.Forms.CheckBox chkHex;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDisassemble;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblPC;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Button btnStep;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblS;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private System.Windows.Forms.Button btnStep10;
        private System.Windows.Forms.Button btnStep100;
        private System.Windows.Forms.Button btnStepCustom;
        private System.Windows.Forms.NumericUpDown nmcStep;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblExecuted;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lblCycles;
        private System.Windows.Forms.Label label17;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape8;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.TextBox txtBreakpoint;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton C;
        private System.Windows.Forms.RadioButton Z;
        private System.Windows.Forms.RadioButton I;
        private System.Windows.Forms.RadioButton D;
        private System.Windows.Forms.RadioButton B;
        private System.Windows.Forms.RadioButton V;
        private System.Windows.Forms.RadioButton N;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox boxWatch;
        private System.Windows.Forms.CheckBox chkWatch;
        private System.Windows.Forms.TextBox txtWatch;
        private System.Windows.Forms.CheckBox chkUsePC;
    }
}