﻿namespace EmulatorGUI
{
    partial class BBC_Emulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnMemDump = new System.Windows.Forms.Button();
            this.btnDissassemble = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.SpeedCheck = new System.Windows.Forms.Timer(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblSpeed = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debuggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryDumpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emulatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadBASICToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.throttle2MzToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlBase = new System.Windows.Forms.Panel();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.vduControl = new EmulatorGUI.VDUControl();
            this.invTimer = new System.Windows.Forms.Timer(this.components);
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.pnlBase.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMemDump
            // 
            this.btnMemDump.Location = new System.Drawing.Point(404, 6);
            this.btnMemDump.Name = "btnMemDump";
            this.btnMemDump.Size = new System.Drawing.Size(93, 23);
            this.btnMemDump.TabIndex = 0;
            this.btnMemDump.Text = "Memory Dump";
            this.btnMemDump.UseVisualStyleBackColor = true;
            this.btnMemDump.Click += new System.EventHandler(this.btnMemDump_Click);
            // 
            // btnDissassemble
            // 
            this.btnDissassemble.Location = new System.Drawing.Point(314, 6);
            this.btnDissassemble.Name = "btnDissassemble";
            this.btnDissassemble.Size = new System.Drawing.Size(84, 23);
            this.btnDissassemble.TabIndex = 1;
            this.btnDissassemble.Text = "Dissassemble";
            this.btnDissassemble.UseVisualStyleBackColor = true;
            this.btnDissassemble.Click += new System.EventHandler(this.btnDissassemble_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(233, 6);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 2;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // SpeedCheck
            // 
            this.SpeedCheck.Interval = 1000;
            this.SpeedCheck.Tick += new System.EventHandler(this.SpeedCheck_Tick);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblSpeed,
            this.toolStripStatusLabel1});
            this.statusStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.statusStrip.Location = new System.Drawing.Point(0, 557);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(500, 24);
            this.statusStrip.TabIndex = 5;
            this.statusStrip.Text = "statusStrip1";
            // 
            // lblSpeed
            // 
            this.lblSpeed.BorderStyle = System.Windows.Forms.Border3DStyle.Bump;
            this.lblSpeed.Margin = new System.Windows.Forms.Padding(0, 3, -4, 2);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(28, 19);
            this.lblSpeed.Text = "0.00";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.toolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.toolStripStatusLabel1.Margin = new System.Windows.Forms.Padding(-2, 3, 0, 2);
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(36, 19);
            this.toolStripStatusLabel1.Text = "MHz";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.emulatorToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(500, 24);
            this.menuStrip.TabIndex = 6;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.resetToolStripMenuItem.Text = "&Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.debuggerToolStripMenuItem,
            this.memoryDumpToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // debuggerToolStripMenuItem
            // 
            this.debuggerToolStripMenuItem.Name = "debuggerToolStripMenuItem";
            this.debuggerToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.debuggerToolStripMenuItem.Text = "&Debugger";
            this.debuggerToolStripMenuItem.Click += new System.EventHandler(this.debuggerToolStripMenuItem_Click);
            // 
            // memoryDumpToolStripMenuItem
            // 
            this.memoryDumpToolStripMenuItem.Name = "memoryDumpToolStripMenuItem";
            this.memoryDumpToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.memoryDumpToolStripMenuItem.Text = "&Memory Dump";
            this.memoryDumpToolStripMenuItem.Click += new System.EventHandler(this.memoryDumpToolStripMenuItem_Click);
            // 
            // emulatorToolStripMenuItem
            // 
            this.emulatorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadBASICToolStripMenuItem,
            this.throttle2MzToolStripMenuItem});
            this.emulatorToolStripMenuItem.Name = "emulatorToolStripMenuItem";
            this.emulatorToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.emulatorToolStripMenuItem.Text = "&Emulator";
            // 
            // loadBASICToolStripMenuItem
            // 
            this.loadBASICToolStripMenuItem.Checked = true;
            this.loadBASICToolStripMenuItem.CheckOnClick = true;
            this.loadBASICToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.loadBASICToolStripMenuItem.Enabled = false;
            this.loadBASICToolStripMenuItem.Name = "loadBASICToolStripMenuItem";
            this.loadBASICToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.loadBASICToolStripMenuItem.Text = "Load BASIC";
            this.loadBASICToolStripMenuItem.Click += new System.EventHandler(this.loadBASICToolStripMenuItem_Click);
            // 
            // throttle2MzToolStripMenuItem
            // 
            this.throttle2MzToolStripMenuItem.CheckOnClick = true;
            this.throttle2MzToolStripMenuItem.Name = "throttle2MzToolStripMenuItem";
            this.throttle2MzToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.throttle2MzToolStripMenuItem.Text = "Throttle (~2Mz)";
            this.throttle2MzToolStripMenuItem.Click += new System.EventHandler(this.throttle2MzToolStripMenuItem_Click);
            // 
            // pnlBase
            // 
            this.pnlBase.Controls.Add(this.btnStartStop);
            this.pnlBase.Controls.Add(this.btnReset);
            this.pnlBase.Controls.Add(this.btnDissassemble);
            this.pnlBase.Controls.Add(this.btnMemDump);
            this.pnlBase.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBase.Location = new System.Drawing.Point(0, 524);
            this.pnlBase.Name = "pnlBase";
            this.pnlBase.Size = new System.Drawing.Size(500, 33);
            this.pnlBase.TabIndex = 7;
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(81, 6);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(75, 23);
            this.btnStartStop.TabIndex = 4;
            this.btnStartStop.Text = "Start/Stop";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // vduControl
            // 
            this.vduControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vduControl.Location = new System.Drawing.Point(0, 24);
            this.vduControl.Name = "vduControl";
            this.vduControl.Size = new System.Drawing.Size(500, 500);
            this.vduControl.TabIndex = 0;
            this.vduControl.Text = "vduControl1";
            // 
            // invTimer
            // 
            this.invTimer.Tick += new System.EventHandler(this.invTimer_Tick);
            // 
            // BBC_Emulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 581);
            this.Controls.Add(this.vduControl);
            this.Controls.Add(this.pnlBase);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "BBC_Emulator";
            this.Text = "BBC_Emulator";
            this.Load += new System.EventHandler(this.BBC_Emulator_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.pnlBase.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMemDump;
        private System.Windows.Forms.Button btnDissassemble;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Timer SpeedCheck;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblSpeed;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debuggerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoryDumpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emulatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadBASICToolStripMenuItem;
        private System.Windows.Forms.Panel pnlBase;
        private VDUControl vduControl;
        private System.Windows.Forms.Timer invTimer;
        private System.Windows.Forms.ToolStripMenuItem throttle2MzToolStripMenuItem;
        private System.Windows.Forms.Button btnStartStop;
    }
}