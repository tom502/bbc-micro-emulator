#include "Assembler.h"


Assembler::Assembler()
{
	assembledIdx = 0;
	createMap();
}


Assembler::~Assembler()
{
}

void Assembler::assembleThis(std::string source)
{
	int idx = std::count(source.begin(), source.end(), ';');

	std::string * lines = new std::string[idx];
	idx = 0;
    std::stringstream stream(source);
    while( getline(stream, lines[idx], ';') )
	{
		idx++;
	}

}

void Assembler::parseLine(std::string line)
{
	size_t found;

	std::map<std::string, KeyValuePair>::iterator iter;
    
    for (iter = opCodeNames.begin(); iter != opCodeNames.end(); ++iter)
	{
		found = line.find(iter->first);
		if(found != std::string::npos)
		{
			//This opcode exists in this line
			KeyValuePair opCode = iter->second;
			assembledArray[assembledIdx] = opCode.second;
			assembledIdx++;
			line.replace(0, 4, "");
			if(instructionBytes[opCode.second] > 1)
			{
				//There are arguments to process
			}
		}
    }
}

void Assembler::init()
{
	//Number of bytes for each instruction
	#pragma region bytes
	// ADC
	instructionBytes[0x69] = 2;
	instructionBytes[0x65] = 2;
	instructionBytes[0x75] = 2;
	instructionBytes[0x6D] = 3;
	instructionBytes[0x7D] = 3;
	instructionBytes[0x79] = 3;
	instructionBytes[0x61] = 2;
	instructionBytes[0x71] = 2;
	// AND
	instructionBytes[0x29] = 2;
	instructionBytes[0x25] = 2;
	instructionBytes[0x35] = 2;
	instructionBytes[0x2D] = 3;
	instructionBytes[0x3D] = 3;
	instructionBytes[0x39] = 3;
	instructionBytes[0x21] = 2;
	instructionBytes[0x31] = 2;
	// ASL
	instructionBytes[0x0A] = 1;
	instructionBytes[0x06] = 2;
	instructionBytes[0x16] = 2;
	instructionBytes[0x0E] = 3;
	instructionBytes[0x1E] = 3;
	// BCC
	instructionBytes[0x90] = 2;
	// BCS
	instructionBytes[0xB0] = 2;
	// BEQ
	instructionBytes[0xF0] = 2;
	// BIT
	instructionBytes[0x24] = 2;
	instructionBytes[0x2C] = 3;
	// BMI
	instructionBytes[0x30] = 2;
	// BNE
	instructionBytes[0xD0] = 2;
	// BPL
	instructionBytes[0x10] = 2;
	// BRK
	instructionBytes[0x00] = 1;
	// BVC
	instructionBytes[0x50] = 2;
	// BVS
	instructionBytes[0x70] = 2;
	// CLC
	instructionBytes[0x18] = 1;
	// CLD
	instructionBytes[0xD8] = 1;
	// CLI
	instructionBytes[0x58] = 1;
	// CLV
	instructionBytes[0xB8] = 1;
	// CMP
	instructionBytes[0xC9] = 2;
	instructionBytes[0xC5] = 2;
	instructionBytes[0xD5] = 2;
	instructionBytes[0xCD] = 3;
	instructionBytes[0xDD] = 3;
	instructionBytes[0xD9] = 3;
	instructionBytes[0xC1] = 2;
	instructionBytes[0xD1] = 2;
	// CPX
	instructionBytes[0xE0] = 2;
	instructionBytes[0xE4] = 2;
	instructionBytes[0xEC] = 3;
	// CPY
	instructionBytes[0xC0] = 2;
	instructionBytes[0xC4] = 2;
	instructionBytes[0xCC] = 3;
	// DEC
	instructionBytes[0xC6] = 2;
	instructionBytes[0xD6] = 2;
	instructionBytes[0xCE] = 3;
	instructionBytes[0xDE] = 3;
	// DEX
	instructionBytes[0xCA] = 1;
	// DEY
	instructionBytes[0x88] = 1;
	// EOR
	instructionBytes[0x49] = 2;
	instructionBytes[0x45] = 2;
	instructionBytes[0x55] = 2;
	instructionBytes[0x4D] = 3;
	instructionBytes[0x5D] = 3;
	instructionBytes[0x59] = 3;
	instructionBytes[0x41] = 2;
	instructionBytes[0x11] = 2;
	// INC
	instructionBytes[0xE6] = 2;
	instructionBytes[0xF6] = 2;
	instructionBytes[0xEE] = 3;
	instructionBytes[0xFE] = 3;
	// INX
	instructionBytes[0xC8] = 1;
	// INY
	instructionBytes[0xE8] = 1;
	// JMP
	instructionBytes[0x4C] = 3;
	instructionBytes[0x6C] = 3;
	// JSR
	instructionBytes[0x20] = 3;
	// LDA
	instructionBytes[0xA9] = 2;
	instructionBytes[0xA5] = 2;
	instructionBytes[0xB5] = 2;
	instructionBytes[0xAD] = 3;
	instructionBytes[0xBD] = 3;
	instructionBytes[0xB9] = 3;
	instructionBytes[0xA1] = 2;
	instructionBytes[0xB1] = 2;
	// LDX
	instructionBytes[0xAE] = 3;
	instructionBytes[0xA6] = 2;
	instructionBytes[0xA2] = 2;
	instructionBytes[0xBE] = 3;
	instructionBytes[0xB6] = 2;
	// LDY
	instructionBytes[0xAC] = 3;
	instructionBytes[0xA4] = 2;
	instructionBytes[0xA0] = 2;
	instructionBytes[0xBC] = 3;
	instructionBytes[0xB4] = 4;
	// LSR
	instructionBytes[0x4A] = 1;
	instructionBytes[0x46] = 2;
	instructionBytes[0x56] = 2;
	instructionBytes[0x4E] = 3;
	instructionBytes[0x5E] = 3;
	// NOP
	instructionBytes[0xEA] = 1;
	// ORA
	instructionBytes[0x09] = 2;
	instructionBytes[0x05] = 2;
	instructionBytes[0x15] = 2;
	instructionBytes[0x0D] = 3;
	instructionBytes[0x1D] = 3;
	instructionBytes[0x19] = 3;
	instructionBytes[0x01] = 2;
	instructionBytes[0x11] = 2;
	// PHA
	instructionBytes[0x48] = 1;
	// PHP
	instructionBytes[0x08] = 1;
	// PLA
	instructionBytes[0x68] = 1;
	// PLP
	instructionBytes[0x28] = 1;
	// ROL
	instructionBytes[0x2A] = 1;
	instructionBytes[0x26] = 2;
	instructionBytes[0x36] = 2;
	instructionBytes[0x2E] = 3;
	instructionBytes[0x3E] = 3;
	// ROR
	instructionBytes[0x6A] = 1;
	instructionBytes[0x66] = 2;
	instructionBytes[0x76] = 2;
	instructionBytes[0x6E] = 3;
	instructionBytes[0x7E] = 3;
	// RTI
	instructionBytes[0x40] = 1;
	// RTS
	instructionBytes[0x60] = 1;
	// SBC
	instructionBytes[0xE9] = 2;
	instructionBytes[0xE5] = 2;
	instructionBytes[0xF5] = 2;
	instructionBytes[0xED] = 3;
	instructionBytes[0xFD] = 3;
	instructionBytes[0xF9] = 3;
	instructionBytes[0xE1] = 2;
	instructionBytes[0xF1] = 2;
	// SEC
	instructionBytes[0x38] = 1;
	// SED
	instructionBytes[0xF8] = 1;
	// SEI
	instructionBytes[0x78] = 1;
	// STA
	instructionBytes[0x85] = 2;
	instructionBytes[0x95] = 2;
	instructionBytes[0x8D] = 3;
	instructionBytes[0x9D] = 3;
	instructionBytes[0x99] = 3;
	instructionBytes[0x81] = 2;
	instructionBytes[0x91] = 2;
	// STX
	instructionBytes[0x86] = 2;
	instructionBytes[0x96] = 2;
	instructionBytes[0x8E] = 3;
	// STY
	instructionBytes[0x84] = 2;
	instructionBytes[0x94] = 2;
	instructionBytes[0x8C] = 3;
	// TAX
	instructionBytes[0xAA] = 1;
	// TAY
	instructionBytes[0xA8] = 1;
	// TSX
	instructionBytes[0xBA] = 1;
	// TXA
	instructionBytes[0x8A] = 1;
	// TXS
	instructionBytes[0x9A] = 1;
	// TYA
	instructionBytes[0x98] = 1;
	#pragma endregion
	
	// Set function pointers
	/*#pragma region opCodes
	for(int i = 0; i < 0x100; i++)
	{
		opCodes[i] = &MPU::Illegal;
	}
	// Initialise op code list
	opCodes[0x00] = &MPU::BRK;
	opCodes[0x01] = &MPU::ORA;
	opCodes[0x05] = &MPU::ORA;
	opCodes[0x06] = &MPU::ASL;
	opCodes[0x08] = &MPU::PHP;
	opCodes[0x09] = &MPU::ORA;
	opCodes[0x0A] = &MPU::ASL;
	opCodes[0x0D] = &MPU::ORA;
	opCodes[0x0E] = &MPU::ASL;

	opCodes[0x10] = &MPU::BPL;
	opCodes[0x11] = &MPU::ORA;
	opCodes[0x15] = &MPU::ORA;
	opCodes[0x16] = &MPU::ASL;
	opCodes[0x18] = &MPU::CLC;
	opCodes[0x19] = &MPU::ORA;
	opCodes[0x1D] = &MPU::ORA;
	opCodes[0x1E] = &MPU::ASL;

	opCodes[0x20] = &MPU::JSR;
	opCodes[0x21] = &MPU::AND;
	opCodes[0x24] = &MPU::BIT;
	opCodes[0x25] = &MPU::AND;
	opCodes[0x26] = &MPU::ROL;
	opCodes[0x28] = &MPU::PLP;
	opCodes[0x29] = &MPU::AND;
	opCodes[0x2A] = &MPU::ROL;
	opCodes[0x2C] = &MPU::BIT;
	opCodes[0x2D] = &MPU::AND;
	opCodes[0x2E] = &MPU::ROL;

	opCodes[0x30] = &MPU::BMI;
	opCodes[0x31] = &MPU::AND;
	opCodes[0x35] = &MPU::AND;
	opCodes[0x36] = &MPU::ROL;
	opCodes[0x38] = &MPU::SEC;
	opCodes[0x39] = &MPU::AND;
	opCodes[0x3D] = &MPU::AND;
	opCodes[0x3E] = &MPU::ROL;

	opCodes[0x40] = &MPU::RTI;
	opCodes[0x41] = &MPU::EOR;
	opCodes[0x45] = &MPU::EOR;
	opCodes[0x46] = &MPU::LSR;
	opCodes[0x48] = &MPU::PHA;
	opCodes[0x49] = &MPU::EOR;
	opCodes[0x4A] = &MPU::LSR;
	opCodes[0x4C] = &MPU::JMP;
	opCodes[0x4D] = &MPU::EOR;
	opCodes[0x4E] = &MPU::LSR;

	opCodes[0x50] = &MPU::BVC;
	opCodes[0x51] = &MPU::EOR;
	opCodes[0x55] = &MPU::EOR;
	opCodes[0x56] = &MPU::LSR;
	opCodes[0x58] = &MPU::CLI;
	opCodes[0x59] = &MPU::EOR;
	opCodes[0x5D] = &MPU::EOR;
	opCodes[0x5E] = &MPU::LSR;

	opCodes[0x60] = &MPU::RTS;
	opCodes[0x61] = &MPU::ADC;
	opCodes[0x65] = &MPU::ADC;
	opCodes[0x66] = &MPU::ROR;
	opCodes[0x68] = &MPU::PLA;
	opCodes[0x69] = &MPU::ADC;
	opCodes[0x6A] = &MPU::ROR;
	opCodes[0x6C] = &MPU::JMP;
	opCodes[0x6D] = &MPU::ADC;
	opCodes[0x6E] = &MPU::ROR;

	opCodes[0x70] = &MPU::BVS;
	opCodes[0x71] = &MPU::ADC;
	opCodes[0x75] = &MPU::ADC;
	opCodes[0x76] = &MPU::ROR;
	opCodes[0x78] = &MPU::SEI;
	opCodes[0x79] = &MPU::ADC;
	opCodes[0x7D] = &MPU::ADC;
	opCodes[0x7E] = &MPU::ROR;

	opCodes[0x81] = &MPU::STA;
	opCodes[0x84] = &MPU::STY;
	opCodes[0x85] = &MPU::STA;
	opCodes[0x86] = &MPU::STX;
	opCodes[0x88] = &MPU::DEY;
	opCodes[0x8A] = &MPU::TXA;
	opCodes[0x8C] = &MPU::STY;
	opCodes[0x8D] = &MPU::STA;
	opCodes[0x8E] = &MPU::STX;

	opCodes[0x90] = &MPU::BCC;
	opCodes[0x91] = &MPU::STA;
	opCodes[0x94] = &MPU::STY;
	opCodes[0x95] = &MPU::STA;
	opCodes[0x96] = &MPU::STX;
	opCodes[0x98] = &MPU::TYA;
	opCodes[0x99] = &MPU::STA;
	opCodes[0x9A] = &MPU::TXS;
	opCodes[0x9D] = &MPU::STA;

	opCodes[0xA0] = &MPU::LDY;
	opCodes[0xA1] = &MPU::LDA;
	opCodes[0xA2] = &MPU::LDX;
	opCodes[0xA4] = &MPU::LDY;
	opCodes[0xA5] = &MPU::LDA;
	opCodes[0xA6] = &MPU::LDX;
	opCodes[0xA8] = &MPU::TAY;
	opCodes[0xA9] = &MPU::LDA;
	opCodes[0xAA] = &MPU::TAX;
	opCodes[0xAC] = &MPU::LDY;
	opCodes[0xAD] = &MPU::LDA;
	opCodes[0xAE] = &MPU::LDX;

	opCodes[0xB0] = &MPU::BCS;
	opCodes[0xB1] = &MPU::LDA;
	opCodes[0xB4] = &MPU::LDY;
	opCodes[0xB5] = &MPU::LDA;
	opCodes[0xB6] = &MPU::LDX;
	opCodes[0xB8] = &MPU::CLV;
	opCodes[0xB9] = &MPU::LDA;
	opCodes[0xBA] = &MPU::TSX;
	opCodes[0xBC] = &MPU::LDY;
	opCodes[0xBD] = &MPU::LDA;
	opCodes[0xBE] = &MPU::LDX;

	opCodes[0xC0] = &MPU::CPY;
	opCodes[0xC1] = &MPU::CMP;
	opCodes[0xC4] = &MPU::CPY;
	opCodes[0xC5] = &MPU::CMP;
	opCodes[0xC6] = &MPU::DEC;
	opCodes[0xC8] = &MPU::INY;
	opCodes[0xC9] = &MPU::CMP;
	opCodes[0xCA] = &MPU::DEX;
	opCodes[0xCC] = &MPU::CPY;
	opCodes[0xCD] = &MPU::CMP;
	opCodes[0xCE] = &MPU::DEC;

	opCodes[0xD0] = &MPU::BNE;
	opCodes[0xD1] = &MPU::CMP;
	opCodes[0xD5] = &MPU::CMP;
	opCodes[0xD6] = &MPU::DEC;
	opCodes[0xD8] = &MPU::CLD;
	opCodes[0xD9] = &MPU::CMP;
	opCodes[0xDD] = &MPU::CMP;
	opCodes[0xDE] = &MPU::DEC;

	opCodes[0xE0] = &MPU::CPX;
	opCodes[0xE1] = &MPU::SBC;
	opCodes[0xE4] = &MPU::CPX;
	opCodes[0xE5] = &MPU::SBC;
	opCodes[0xE6] = &MPU::INC;
	opCodes[0xE8] = &MPU::INX;
	opCodes[0xE9] = &MPU::SBC;
	opCodes[0xEA] = &MPU::NOP;
	opCodes[0xEC] = &MPU::CPX;
	opCodes[0xED] = &MPU::SBC;
	opCodes[0xEE] = &MPU::INC;

	opCodes[0xF0] = &MPU::BEQ;
	opCodes[0xF1] = &MPU::SBC;
	opCodes[0xF5] = &MPU::SBC;
	opCodes[0xF6] = &MPU::INC;
	opCodes[0xF8] = &MPU::SED;
	opCodes[0xF9] = &MPU::SBC;
	opCodes[0xFD] = &MPU::SBC;
	opCodes[0xFE] = &MPU::INC;
	#pragma endregion
	// Set addressing types
	#pragma region Addressing types
	_ADC[0x61] = &MPU::IndirectX;
	_ADC[0x65] = &MPU::ZeroPage;
	_ADC[0x69] = &MPU::Immediate;
	_ADC[0x6D] = &MPU::Absolute;
	_ADC[0x71] = &MPU::IndirectY;
	_ADC[0x75] = &MPU::ZeroPageX;
	_ADC[0x79] = &MPU::AbsoluteY;
	_ADC[0x7D] = &MPU::AbsoluteX;

	_AND[0x21] = &MPU::IndirectX;
	_AND[0x25] = &MPU::ZeroPage;
	_AND[0x29] = &MPU::Immediate;
	_AND[0x2D] = &MPU::Absolute;
	_AND[0x31] = &MPU::IndirectY;
	_AND[0x35] = &MPU::ZeroPageX;
	_AND[0x39] = &MPU::AbsoluteY;
	_AND[0x3D] = &MPU::AbsoluteX;

	_ASL[0x06] = &MPU::ZeroPage_Source;
	_ASL[0x0A] = &MPU::Accumulator_Source;
	_ASL[0x0E] = &MPU::Absolute_Source;
	_ASL[0x16] = &MPU::ZeroPageX_Source;
	_ASL[0x1E] = &MPU::AbsoluteX_Source;

	_BIT[0x24] = &MPU::ZeroPage;
	_BIT[0x2C] = &MPU::Absolute;

	_CMP[0xCD] = &MPU::Absolute;
	_CMP[0xC5] = &MPU::ZeroPage;
	_CMP[0xC9] = &MPU::Immediate;
	_CMP[0xDD] = &MPU::AbsoluteX;
	_CMP[0xD9] = &MPU::AbsoluteY;
	_CMP[0xC1] = &MPU::IndirectX;
	_CMP[0xD1] = &MPU::IndirectY;
	_CMP[0xD5] = &MPU::ZeroPageX;

	_CPX[0xEC] = &MPU::Absolute;
	_CPX[0xE4] = &MPU::ZeroPage;
	_CPX[0xE0] = &MPU::Immediate;

	_CPY[0xCC] = &MPU::Absolute;
	_CPY[0xC4] = &MPU::ZeroPage;
	_CPY[0xC0] = &MPU::Immediate;

	_DEC[0xCE] = &MPU::Absolute_Source;
	_DEC[0xC6] = &MPU::ZeroPage_Source;
	_DEC[0xDE] = &MPU::AbsoluteX_Source;
	_DEC[0xD6] = &MPU::ZeroPageX_Source;

	_EOR[0x4D] = &MPU::Absolute;
	_EOR[0x45] = &MPU::ZeroPage;
	_EOR[0x49] = &MPU::Immediate;
	_EOR[0x5D] = &MPU::AbsoluteX;
	_EOR[0x59] = &MPU::AbsoluteY;
	_EOR[0x41] = &MPU::IndirectX;
	_EOR[0x51] = &MPU::IndirectY;
	_EOR[0x55] = &MPU::ZeroPageX;

	_INC[0xEE] = &MPU::Absolute_Source;
	_INC[0xE6] = &MPU::ZeroPage_Source;
	_INC[0xFE] = &MPU::AbsoluteX_Source;
	_INC[0xF6] = &MPU::ZeroPageX_Source;

	_JMP[0x4C] = &MPU::AbsoluteJumps;
	_JMP[0x6C] = &MPU::Indirect;

	_LDA[0xAD] = &MPU::Absolute;
	_LDA[0xA5] = &MPU::ZeroPage;
	_LDA[0xA9] = &MPU::Immediate;
	_LDA[0xBD] = &MPU::AbsoluteX;
	_LDA[0xB9] = &MPU::AbsoluteY;
	_LDA[0xA1] = &MPU::IndirectX;
	_LDA[0xB1] = &MPU::IndirectY;
	_LDA[0xB5] = &MPU::ZeroPageX;

	_LDX[0xAE] = &MPU::Absolute;
	_LDX[0xA6] = &MPU::ZeroPage;
	_LDX[0xBE] = &MPU::AbsoluteY;
	_LDX[0xB6] = &MPU::ZeroPageY;
	_LDX[0xA2] = &MPU::Immediate;

	_LDY[0xAC] = &MPU::Absolute;
	_LDY[0xA4] = &MPU::ZeroPage;
	_LDY[0xA0] = &MPU::Immediate;
	_LDY[0xBC] = &MPU::AbsoluteX;
	_LDY[0xB4] = &MPU::ZeroPageX;

	_LSR[0x4A] = &MPU::Accumulator_Source;
	_LSR[0x4E] = &MPU::Absolute_Source;
	_LSR[0x46] = &MPU::ZeroPage_Source;
	_LSR[0x5E] = &MPU::AbsoluteX_Source;
	_LSR[0x56] = &MPU::ZeroPageX_Source;

	_ORA[0x0D] = &MPU::Absolute;
	_ORA[0x05] = &MPU::ZeroPage;
	_ORA[0x09] = &MPU::Immediate;
	_ORA[0x1D] = &MPU::AbsoluteX;
	_ORA[0x19] = &MPU::AbsoluteY;
	_ORA[0x01] = &MPU::IndirectX;
	_ORA[0x11] = &MPU::IndirectY;
	_ORA[0x15] = &MPU::ZeroPage;

	_ROL[0x2A] = &MPU::Accumulator_Source;
	_ROL[0x26] = &MPU::ZeroPage_Source;
	_ROL[0x36] = &MPU::ZeroPageX_Source;
	_ROL[0x2E] = &MPU::Absolute_Source;
	_ROL[0x3E] = &MPU::AbsoluteX_Source;

	_ROR[0x6A] = &MPU::Accumulator_Source;
	_ROR[0x66] = &MPU::ZeroPage_Source;
	_ROR[0x76] = &MPU::ZeroPageX_Source;
	_ROR[0x6E] = &MPU::Absolute_Source;
	_ROR[0x7E] = &MPU::AbsoluteX_Source;

	_SBC[0xED] = &MPU::Absolute;
	_SBC[0xE5] = &MPU::ZeroPage;
	_SBC[0xE9] = &MPU::Immediate;
	_SBC[0xFD] = &MPU::AbsoluteX;
	_SBC[0xF9] = &MPU::AbsoluteY;
	_SBC[0xE1] = &MPU::IndirectX;
	_SBC[0xF1] = &MPU::IndirectY;
	_SBC[0xF5] = &MPU::ZeroPageX;

	_STA[0x8D] = &MPU::Absolute_SourceOnly;
	_STA[0x85] = &MPU::ZeroPage_SourceOnly;
	_STA[0x9D] = &MPU::AbsoluteX_SourceOnly;
	_STA[0x99] = &MPU::AbsoluteY_SourceOnly;
	_STA[0x81] = &MPU::IndirectX_SourceOnly;
	_STA[0x91] = &MPU::IndirectY_SourceOnly;
	_STA[0x95] = &MPU::ZeroPageX_SourceOnly;

	_STX[0x8E] = &MPU::Absolute_SourceOnly;
	_STX[0x86] = &MPU::ZeroPage_SourceOnly;
	_STX[0x96] = &MPU::ZeroPageY_SourceOnly;

	_STY[0x8C] = &MPU::Absolute_SourceOnly;
	_STY[0x84] = &MPU::ZeroPage_SourceOnly;
	_STY[0x94] = &MPU::ZeroPageX_SourceOnly;
	#pragma endregion*/
}

void Assembler::createMap()
{
	opCodeNames.insert(std::make_pair("BRK", KeyValuePair(Implied, 0x00)));
	opCodeNames.insert(std::make_pair("ORA", KeyValuePair(IndirectX, 0x01)));
	opCodeNames.insert(std::make_pair("ORA", KeyValuePair(ZeroPage, 0x05)));
	opCodeNames.insert(std::make_pair("ASL", KeyValuePair(ZeroPage, 0x06)));
	opCodeNames.insert(std::make_pair("PHP", KeyValuePair(Implied, 0x08)));
	opCodeNames.insert(std::make_pair("ORA", KeyValuePair(Immediate, 0x09)));
	opCodeNames.insert(std::make_pair("ASL", KeyValuePair(Accumulator, 0x0A)));
	opCodeNames.insert(std::make_pair("ORA", KeyValuePair(Absolute, 0x0D)));
	opCodeNames.insert(std::make_pair("ASL", KeyValuePair(Absolute, 0x0E)));
	opCodeNames.insert(std::make_pair("BPL", KeyValuePair(Relative, 0x10)));
	opCodeNames.insert(std::make_pair("ORA", KeyValuePair(IndirectY, 0x11)));
	opCodeNames.insert(std::make_pair("ORA", KeyValuePair(ZeroPageX, 0x15)));
	opCodeNames.insert(std::make_pair("ASL", KeyValuePair(ZeroPageX, 0x16)));
	opCodeNames.insert(std::make_pair("CLC", KeyValuePair(Implied, 0x18)));
	opCodeNames.insert(std::make_pair("ORA", KeyValuePair(AbsoluteY, 0x19)));
	opCodeNames.insert(std::make_pair("ORA", KeyValuePair(AbsoluteX, 0x1D)));
	opCodeNames.insert(std::make_pair("ASL", KeyValuePair(AbsoluteX, 0x1E)));
    opCodeNames.insert(std::make_pair("JSR", KeyValuePair(Absolute, 0x20)));
	opCodeNames.insert(std::make_pair("AND", KeyValuePair(IndirectX, 0x21)));
	opCodeNames.insert(std::make_pair("BIT", KeyValuePair(ZeroPage, 0x24)));
	opCodeNames.insert(std::make_pair("AND", KeyValuePair(ZeroPage, 0x25)));
	opCodeNames.insert(std::make_pair("ROL", KeyValuePair(ZeroPage, 0x26)));
	opCodeNames.insert(std::make_pair("PLP", KeyValuePair(Implied, 0x28)));
	opCodeNames.insert(std::make_pair("AND", KeyValuePair(Immediate, 0x29)));
	opCodeNames.insert(std::make_pair("ROL", KeyValuePair(Accumulator, 0x2A)));
	opCodeNames.insert(std::make_pair("BIT", KeyValuePair(Absolute, 0x2C)));
	opCodeNames.insert(std::make_pair("AND", KeyValuePair(Absolute, 0x2D)));
	opCodeNames.insert(std::make_pair("ROL", KeyValuePair(Absolute, 0x2E)));
	opCodeNames.insert(std::make_pair("BMI", KeyValuePair(Relative, 0x30)));
	opCodeNames.insert(std::make_pair("AND", KeyValuePair(IndirectY, 0x31)));
	opCodeNames.insert(std::make_pair("AND", KeyValuePair(ZeroPageX, 0x35)));
	opCodeNames.insert(std::make_pair("ROL", KeyValuePair(ZeroPageX, 0x36)));
	opCodeNames.insert(std::make_pair("SEC", KeyValuePair(Implied, 0x38)));
	opCodeNames.insert(std::make_pair("AND", KeyValuePair(AbsoluteY, 0x39)));
	opCodeNames.insert(std::make_pair("AND", KeyValuePair(AbsoluteX, 0x3D)));
	opCodeNames.insert(std::make_pair("ROL", KeyValuePair(AbsoluteX, 0x3E)));
    opCodeNames.insert(std::make_pair("RTI", KeyValuePair(Implied, 0x40)));
	opCodeNames.insert(std::make_pair("EOR", KeyValuePair(IndirectX, 0x41)));
	opCodeNames.insert(std::make_pair("EOR", KeyValuePair(ZeroPage, 0x45)));
	opCodeNames.insert(std::make_pair("LSR", KeyValuePair(ZeroPage, 0x46)));
	opCodeNames.insert(std::make_pair("PHA", KeyValuePair(Implied, 0x48)));
	opCodeNames.insert(std::make_pair("EOR", KeyValuePair(Immediate, 0x49)));
	opCodeNames.insert(std::make_pair("LSR", KeyValuePair(Accumulator, 0x4A)));
	opCodeNames.insert(std::make_pair("JMP", KeyValuePair(Absolute, 0x4C)));
	opCodeNames.insert(std::make_pair("EOR", KeyValuePair(Absolute, 0x4D)));
	opCodeNames.insert(std::make_pair("LSR", KeyValuePair(Absolute, 0x4E)));
	opCodeNames.insert(std::make_pair("BVC", KeyValuePair(Relative, 0x50)));
	opCodeNames.insert(std::make_pair("EOR", KeyValuePair(IndirectY, 0x51)));
	opCodeNames.insert(std::make_pair("EOR", KeyValuePair(ZeroPageX, 0x55)));
	opCodeNames.insert(std::make_pair("LSR", KeyValuePair(ZeroPageX, 0x56)));
	opCodeNames.insert(std::make_pair("CLI", KeyValuePair(Implied, 0x58)));
	opCodeNames.insert(std::make_pair("EOR", KeyValuePair(AbsoluteY, 0x59)));
	opCodeNames.insert(std::make_pair("EOR", KeyValuePair(AbsoluteX, 0x5D)));
	opCodeNames.insert(std::make_pair("LSR", KeyValuePair(AbsoluteX, 0x5E)));
	opCodeNames.insert(std::make_pair("RTS", KeyValuePair(Implied, 0x60)));
	opCodeNames.insert(std::make_pair("ADC", KeyValuePair(IndirectX, 0x61)));
	opCodeNames.insert(std::make_pair("ADC", KeyValuePair(ZeroPage, 0x65)));
    opCodeNames.insert(std::make_pair("ROR", KeyValuePair(ZeroPage, 0x66)));
	opCodeNames.insert(std::make_pair("PLA", KeyValuePair(Implied, 0x68)));
	opCodeNames.insert(std::make_pair("ADC", KeyValuePair(Immediate, 0x69)));
	opCodeNames.insert(std::make_pair("ROR", KeyValuePair(Accumulator, 0x6A)));
	opCodeNames.insert(std::make_pair("JMP", KeyValuePair(Indirect, 0x6C)));
	opCodeNames.insert(std::make_pair("ADC", KeyValuePair(Absolute, 0x6D)));
    opCodeNames.insert(std::make_pair("ROR", KeyValuePair(Absolute, 0x6E)));
	opCodeNames.insert(std::make_pair("BVS", KeyValuePair(Relative, 0x70)));
	opCodeNames.insert(std::make_pair("ADC", KeyValuePair(IndirectY, 0x71)));
	opCodeNames.insert(std::make_pair("ADC", KeyValuePair(ZeroPageX, 0x75)));
	opCodeNames.insert(std::make_pair("ROR", KeyValuePair(ZeroPageX, 0x76)));
	opCodeNames.insert(std::make_pair("SEI", KeyValuePair(Implied, 0x78)));
	opCodeNames.insert(std::make_pair("ADC", KeyValuePair(AbsoluteY, 0x79)));
	opCodeNames.insert(std::make_pair("ADC", KeyValuePair(AbsoluteX, 0x7D)));
    opCodeNames.insert(std::make_pair("ROR", KeyValuePair(AbsoluteX, 0x7E)));
	opCodeNames.insert(std::make_pair("STA", KeyValuePair(IndirectX, 0x81)));
	opCodeNames.insert(std::make_pair("STY", KeyValuePair(ZeroPage, 0x84)));
	opCodeNames.insert(std::make_pair("STA", KeyValuePair(ZeroPage, 0x85)));
	opCodeNames.insert(std::make_pair("STX", KeyValuePair(ZeroPage, 0x86)));
	opCodeNames.insert(std::make_pair("DEY", KeyValuePair(Implied, 0x88)));
	opCodeNames.insert(std::make_pair("TXA", KeyValuePair(Implied, 0x8A)));
	opCodeNames.insert(std::make_pair("STY", KeyValuePair(Absolute, 0x8C)));
	opCodeNames.insert(std::make_pair("STA", KeyValuePair(Absolute, 0x8D)));
	opCodeNames.insert(std::make_pair("STX", KeyValuePair(Absolute, 0x8E)));
    opCodeNames.insert(std::make_pair("BCC", KeyValuePair(Relative, 0x90)));
	opCodeNames.insert(std::make_pair("STA", KeyValuePair(IndirectY, 0x91)));
	opCodeNames.insert(std::make_pair("STY", KeyValuePair(ZeroPageX, 0x94)));
	opCodeNames.insert(std::make_pair("STA", KeyValuePair(ZeroPageX, 0x95)));
	opCodeNames.insert(std::make_pair("STX", KeyValuePair(ZeroPageY, 0x96)));
	opCodeNames.insert(std::make_pair("TYA", KeyValuePair(Implied, 0x98)));
	opCodeNames.insert(std::make_pair("STA", KeyValuePair(AbsoluteY, 0x99)));
	opCodeNames.insert(std::make_pair("TXS", KeyValuePair(Implied, 0x9A)));
	opCodeNames.insert(std::make_pair("STA", KeyValuePair(AbsoluteX, 0x9D)));
	opCodeNames.insert(std::make_pair("LDY", KeyValuePair(Immediate, 0xA0)));
	opCodeNames.insert(std::make_pair("LDA", KeyValuePair(IndirectX, 0xA1)));
	opCodeNames.insert(std::make_pair("LDX", KeyValuePair(Immediate, 0xA2)));
	opCodeNames.insert(std::make_pair("LDY", KeyValuePair(ZeroPage, 0xA4)));
	opCodeNames.insert(std::make_pair("LDA", KeyValuePair(ZeroPage, 0xA5)));
	opCodeNames.insert(std::make_pair("LDX", KeyValuePair(ZeroPage, 0xA6)));
	opCodeNames.insert(std::make_pair("TAY", KeyValuePair(Implied, 0xA8)));
	opCodeNames.insert(std::make_pair("LDA", KeyValuePair(Immediate, 0xA9)));
	opCodeNames.insert(std::make_pair("TAX", KeyValuePair(Implied, 0xAA)));
	opCodeNames.insert(std::make_pair("LDY", KeyValuePair(Absolute, 0xAC)));
	opCodeNames.insert(std::make_pair("LDA", KeyValuePair(Absolute, 0xAD)));
	opCodeNames.insert(std::make_pair("LDX", KeyValuePair(Absolute, 0xAE)));
	opCodeNames.insert(std::make_pair("BCS", KeyValuePair(Relative, 0xB0)));
	opCodeNames.insert(std::make_pair("LDA", KeyValuePair(IndirectY, 0xB1)));
	opCodeNames.insert(std::make_pair("LDY", KeyValuePair(ZeroPageX, 0xB4)));
	opCodeNames.insert(std::make_pair("LDA", KeyValuePair(ZeroPageX, 0xB5)));
	opCodeNames.insert(std::make_pair("LDX", KeyValuePair(ZeroPageY, 0xB6)));
	opCodeNames.insert(std::make_pair("CLV", KeyValuePair(Implied, 0xB8)));
	opCodeNames.insert(std::make_pair("LDA", KeyValuePair(AbsoluteY, 0xB9)));
	opCodeNames.insert(std::make_pair("TSX", KeyValuePair(Implied, 0xBA)));
	opCodeNames.insert(std::make_pair("LDY", KeyValuePair(AbsoluteX, 0xBC)));
	opCodeNames.insert(std::make_pair("LDA", KeyValuePair(AbsoluteX, 0xBD)));
	opCodeNames.insert(std::make_pair("LDX", KeyValuePair(AbsoluteY, 0xBE)));
	opCodeNames.insert(std::make_pair("CPY", KeyValuePair(Immediate, 0xC0)));
	opCodeNames.insert(std::make_pair("CMP", KeyValuePair(IndirectX, 0xC1)));
	opCodeNames.insert(std::make_pair("CPY", KeyValuePair(ZeroPage, 0xC4)));
	opCodeNames.insert(std::make_pair("CMP", KeyValuePair(ZeroPage, 0xC5)));
	opCodeNames.insert(std::make_pair("DEC", KeyValuePair(ZeroPage, 0xC6)));
	opCodeNames.insert(std::make_pair("INY", KeyValuePair(Implied, 0xC8)));
	opCodeNames.insert(std::make_pair("CMP", KeyValuePair(Immediate, 0xC9)));
	opCodeNames.insert(std::make_pair("DEX", KeyValuePair(Implied, 0xCA)));
	opCodeNames.insert(std::make_pair("CPY", KeyValuePair(Absolute, 0xCC)));
	opCodeNames.insert(std::make_pair("CMP", KeyValuePair(Absolute, 0xCD)));
	opCodeNames.insert(std::make_pair("DEC", KeyValuePair(Absolute, 0xCE)));
	opCodeNames.insert(std::make_pair("BNE", KeyValuePair(Relative, 0xD0)));
	opCodeNames.insert(std::make_pair("CMP", KeyValuePair(IndirectY, 0xD1)));
	opCodeNames.insert(std::make_pair("CMP", KeyValuePair(ZeroPageX, 0xD5)));
	opCodeNames.insert(std::make_pair("DEC", KeyValuePair(ZeroPageX, 0xD6)));
	opCodeNames.insert(std::make_pair("CLD", KeyValuePair(Implied, 0xD8)));
	opCodeNames.insert(std::make_pair("CMP", KeyValuePair(AbsoluteY, 0xD9)));
	opCodeNames.insert(std::make_pair("CMP", KeyValuePair(AbsoluteX, 0xDD)));
	opCodeNames.insert(std::make_pair("DEC", KeyValuePair(AbsoluteX, 0xDE)));
	opCodeNames.insert(std::make_pair("CPX", KeyValuePair(Immediate, 0xE0)));
	opCodeNames.insert(std::make_pair("SBC", KeyValuePair(IndirectX, 0xE1)));
	opCodeNames.insert(std::make_pair("CPX", KeyValuePair(ZeroPage, 0xE4)));
	opCodeNames.insert(std::make_pair("SBC", KeyValuePair(ZeroPage, 0xE5)));
	opCodeNames.insert(std::make_pair("INC", KeyValuePair(ZeroPage, 0xE6)));
	opCodeNames.insert(std::make_pair("INX", KeyValuePair(Implied, 0xE8)));
	opCodeNames.insert(std::make_pair("SBC", KeyValuePair(Immediate, 0xE9)));
	opCodeNames.insert(std::make_pair("NOP", KeyValuePair(Implied, 0xEA)));
	opCodeNames.insert(std::make_pair("CPX", KeyValuePair(Absolute, 0xEC)));
	opCodeNames.insert(std::make_pair("SBC", KeyValuePair(Absolute, 0xED)));
	opCodeNames.insert(std::make_pair("INC", KeyValuePair(Absolute, 0xEE)));
	opCodeNames.insert(std::make_pair("BEQ", KeyValuePair(Relative, 0xF0)));
	opCodeNames.insert(std::make_pair("SBC", KeyValuePair(IndirectY, 0xF1)));
	opCodeNames.insert(std::make_pair("SBC", KeyValuePair(ZeroPageX, 0xF5)));
	opCodeNames.insert(std::make_pair("INC", KeyValuePair(ZeroPageX, 0xF6)));
	opCodeNames.insert(std::make_pair("SED", KeyValuePair(Implied, 0xF8)));
	opCodeNames.insert(std::make_pair("SBC", KeyValuePair(AbsoluteY, 0xF9)));
	opCodeNames.insert(std::make_pair("SBC", KeyValuePair(AbsoluteX, 0xFD)));
	opCodeNames.insert(std::make_pair("INC", KeyValuePair(AbsoluteX, 0xFE)));
}