#include <map>
#include <string>
#include <sstream>
#include <algorithm>

#ifndef _6502ASSEMBLER_
#define _6502ASSEMBLER_

typedef unsigned char BIT_8;
typedef unsigned short int BIT_16;
typedef std::pair<int, BIT_8> KeyValuePair;

class Assembler
{
private:
	void init();
	
	KeyValuePair addressType;
	std::map<std::string, KeyValuePair> opCodeNames;

	enum AddressingType
    {
        Implied = 0,
        Immediate,
        ZeroPage,
        ZeroPageX,
        ZeroPageY,
        Absolute,
        AbsoluteX,
        AbsoluteY,
        Indirect,
        IndirectX,
        IndirectY,
        Accumulator,
        Relative,
    };

	//static Dictionary<int, KeyValuePair<AddressingType, string>> OpCodeNames;

	int assembledIdx;

	// Static will auto-init every element to 0
	BIT_8 assembledArray[0xFFFF];

	// The number of each instruction takes up
	// Opcode is used as the index
	BIT_8 instructionBytes[0xFF];
	
	// Array of function pointers for each instruction
	// Index is the value of the op code
	void (Assembler::*opCodes[0xFF])();

	BIT_8 (Assembler::*_ADC[0x7E])(); // ADC
	BIT_8 (Assembler::*_AND[0x3E])(); // AND
	BIT_8 (Assembler::*_ASL[0x1F])(); // ASL
	BIT_8 (Assembler::*_BIT[0x2D])(); // BIT
	BIT_8 (Assembler::*_CMP[0xDE])(); // CMP
	BIT_8 (Assembler::*_CPX[0xED])(); // CPX
	BIT_8 (Assembler::*_CPY[0xCD])(); // CPY
	BIT_8 (Assembler::*_DEC[0xDF])(); // DEC
	BIT_8 (Assembler::*_EOR[0x5E])(); // EOR
	BIT_8 (Assembler::*_INC[0xFF])(); // INC
	BIT_16 (Assembler::*_JMP[0x6D])(); // JMP
	BIT_8 (Assembler::*_LDA[0xBE])(); // LDA
	BIT_8 (Assembler::*_LDX[0xBF])(); // LDX
	BIT_8 (Assembler::*_LDY[0xBD])(); // LDY
	BIT_8 (Assembler::*_LSR[0x5F])(); // LSR
	BIT_8 (Assembler::*_ORA[0x1E])(); // ORA
	BIT_8 (Assembler::*_ROL[0x3F])(); // ROL
	BIT_8 (Assembler::*_ROR[0x7F])(); // ROR
	BIT_8 (Assembler::*_SBC[0xFE])(); // SBC
	void (Assembler::*_STA[0x9E])(); // STA
	void (Assembler::*_STX[0x97])(); // STX
	void (Assembler::*_STY[0x95])(); // STY

	void createMap();

	void parseLine(std::string line);

public:
	Assembler();
	~Assembler();

	void assembleThis(std::string source);

};

#endif
