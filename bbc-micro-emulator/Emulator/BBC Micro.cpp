#include "BBC Micro.h"
#include <iostream>
#include <sstream>

BBCEM::BBCEM(bool useBASIC)
	: mpu()
	, memory()
{
	loadBASIC = useBASIC;
	load();
}

BBCEM::~BBCEM()
{

}

//Reset the emulator
void BBCEM::reset()
{
    //Clear out the memory
	RAM_ROM::resetMemory(&memory);
	//Clear all registers
	mpu.resetRegisters();
	//Reset number of cycles and number of instructions executed.
	mpu.setTotalCycles(0);
	mpu.setTotalExecuted(0);
    //Load the ROMs
	load();
}

void BBCEM::load()
{
    //Load OS ROM
	memory.loadOS(readOSROM());
	//Load BASIC language ROM
	if(loadBASIC)
	{
		memory.loadROM(readBASICROM(), 15);
	}
	//Set program counter to the start address for OS
	mpu.loadProgram(mpu.formAbsolute(memory[0xFFFD], memory[0xFFFC]));
}

//Get the time taken to run x amount of instructions
int BBCEM::timeToRunXInstructions(int x)
{
	DWORD cTick, nTick;
	std::stringstream ss;
    cTick = GetTickCount();

	while(mpu.getTotalCycles() < x)//mpu.P.B() == false)
	{
		mpu.cycle();
	}

	nTick = GetTickCount();
	return (nTick - cTick);
}

void BBCEM::toggleBASIC(bool b)
{
//	loadBASIC = b;
}

void BBCEM::keyDown(int row, int col)
{
	memory.sysVIAKeyDown(row,col);
}

void BBCEM::keyUp(int row, int col)
{
	memory.sysVIAKeyUp(row,col);
}

void BBCEM::setThrottle(bool b)
{
	mpu.setThrottle(b);
}