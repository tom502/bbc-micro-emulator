/* 
 * File:   Types.h
 * Author: tom
 *
 * Created on 25 June 2011, 16:05
 */

#ifndef TYPES_H
#define	TYPES_H

typedef unsigned char BIT_8;



void decodeInstruction(BIT_8 op)
{
	switch(op)
    {
        //ADC
        case 0x6D:
                break;
        case 0x65:
                break;
        case 0x69:
                break;
        case 0x7D:
                break;
        case 0x79:
                break;
        case 0x61:
                break;
        case 0x71:
                break;
        case 0x75:
                break;

        //AND
        case 0x2D:
                break;
        case 0x25:
                break;
        case 0x29:
                break;
        case 0x3D:
                break;
        case 0x39:
                break;
        case 0x21:
                break;
        case 0x31:
                break;
        case 0x35:
                break;

        //ASL
        case 0x0A:
                break;
        case 0x0E:
                break;
        case 0x06:
                break;
        case 0x1E:
                break;
        case 0x16:
                break;

        //BCC
        case 0x90:
                break;

        //BCS
        case 0xB0:
                break;

        //BEQ
        case 0xF0:
                break;

        //BIT
        case 0x2C:
                break;
        case 0x24:
                break;

        //BMI
        case 0x30:
                break;
        
		//BNE
		case 0xD0:
                break;
				
		//BPL
		case 0x10:
                break;
		
		//BRK
		case 0x00:
                break;

		//BCV
		case 0x50:
                break;

		//BVS
		case 0x70:
                break;
    }
}

#endif	/* TYPES_H */

