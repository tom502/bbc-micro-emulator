#include "VIA.h"


VIA::VIA()
{
}

VIA::~VIA()
{
}

void VIA::writeVIA(short int reg, BIT_8 value)
{
	//Strip out the 0x40 value to leave a range of 0x0 - 0xF
	reg &= 0xF;

	if(reg == 0)
	{
		ORB = value;
		IRB = ORB;
		IFR &= ~16;
		updateIFR();
	}
	else if(reg == 1)
	{
		ORA = value;
		slowDataBusWrite(value);
		IRA = ORA;
		IFR &= 0xfc;
		updateIFR();
	}
	else if(reg == 2)
	{
		DDRB = value;
	}
	else if(reg == 3)
	{
		DDRA = value;
	}
	else if(reg == 4 || reg == 6)
	{
		//This operation is the same for both regsiters 4 and 6
		T1L_L = value;
	}
	else if(reg == 5)
	{
		//Loads latches first
		T1L_H = value;

		//Latches are then loaded into the counters
		T1C_L = T1L_L;
		T1C_H = T1L_H;

		//T1 interrupt flag is reset - bit 6 in IFR
		IFR &= 0x40;

		//If PB7 output is on in ACR then zero PB7
		if ((ACR & 0x80) > 0)
		{
			ORB &= 0x7f;
			IRB &= 0x7f;
		};
		updateIFR();
		timer1hasshot = false;
	}
	else if(reg == 7)
	{
		//Set high order latch
		T1L_H = value;
		IFR &= 0xBF;
		updateIFR();
	}
	else if(reg == 8)
	{
		//Set low order latch
		T2L_L = value;
	}
	else if(reg == 9)
	{
		//Set High order counter
		T2C_H = value;
		//Set low order countr from low order latches
		T2C_L = T2L_L;
		//T2 interrupt flag is reset - bit 5 of IFR
		IFR &= 0xDF;
		updateIFR();
		timer2hasshot = false;
	}
	else if(reg == 10)
	{
		SR = value;
	}
	else if(reg == 11)
	{
		ACR = value;
		//Shift register mode is set from bits 2-4 of ACR
		SRMode = (value & 0x1C);
	}
	else if(reg == 12)
	{
		PCR = value;
	}
	else if(reg == 13)
	{
		IFR &= ~value;
		updateIFR();
	}
	else if(reg == 14)
	{
		//If the 7th Bit is set then each '1' in remaining bits is enabled
		if((value & 0x80) > 0)
		{
			IER |= value;
		}
		//If the 7th Bit is not set then each '1' in remaining bits is disabled
		else
		{
			IER &= ~value;
		}
		IER &= 0x7F;
		updateIFR();
	}
	else if(reg == 15)
	{
		ORA = value;
		IRA = ORA;
		slowDataBusWrite(value);
	}
}

BIT_8 VIA::readVIA(short int reg)
{
	//Strip out the 0x40 value to leave a range of 0x0 - 0xF
	reg &= 0xF;

	BIT_8 ret = 0xFF;

	if(reg == 0)
	{
		ret = IRB & DDRB;
		updateIFR();
	}
	else if(reg == 1)
	{
		ret = IRA & DDRA;
		updateIFR();
	}
	else if(reg == 2)
	{
		ret = DDRB;
	}
	else if(reg == 3)
	{
		ret = DDRA;
	}
	else if(reg == 4)
	{
		//Return low order counter
		ret = T1C_L;
		//T1 interrupt flag is reset - bit 6 in IFR
		IFR &= 0x40;
		updateIFR();
	}
	else if(reg == 5)
	{
		//Return high order counter
		ret = T1C_H;
	}
	else if(reg == 6)
	{
		ret = T1L_L;
		//No reset of interrupt flag
	}
	else if(reg == 7)
	{
		ret = T1L_H;
	}
	else if(reg == 8)
	{
		ret = T2C_L;
		//T2 interrupt flag is reset - bit 5 of IFR
		IFR &= 0xDF;
		updateIFR();
	}
	else if(reg == 9)
	{
		ret = T2C_H;
	}
	else if(reg == 10)
	{
		ret = SR;
	}
	else if(reg == 11)
	{
		ret = ACR;
	}
	else if(reg == 12)
	{
		ret = PCR;
	}
	else if(reg == 13)
	{
		updateIFR();
		ret = IFR;
	}
	else if(reg == 14)
	{
		//BIT 7 is always set for a read
		ret = IER | 0x80;
	}
	else if(reg == 15)
	{
		ret = slowDataBusRead();
	}

	return ret;
}

void VIA::resetVIA()
{
	//Set I/O registers to 255
	IRB = IRA = ORB = ORA = 0xFF;
	//Zero data directions
	DDRB = DDRA = 0;
	//Zero control registers
	ACR = PCR = 0;
	//Zero interrupt flags
	IFR = 0;
	//Only turn on 7th bit
	IER = 0x80;
	//Zero shift register
	SR = 0;
	SRMode = 0;
	//Reset timers to max value as they decrement
	T1C_L = T1L_L = T2C_L = T2L_L =
		T1C_H = T1L_H = T2C_H = T2L_H = 0xFF;
	timer1hasshot = false;
	timer2hasshot = false;
	IC32 = 0;
}

void VIA::VIAUpdate()
{
	if (!timer1hasshot || ((ACR & 0x40) > 0))
	{
		IFR |= 0x40; //Interrupt for T1

		updateIFR();

		if ((ACR & 0x80) > 0)
		{
			ORB ^= 0x80; // Toggle PB7
			IRB ^= 0x80; // Toggle PB7
		}

		timer1hasshot = true;
	}

	if (!timer2hasshot)
	{
			
		IFR |= 0x20; // Timer 2 interrupt

		updateIFR();

		timer2hasshot = true;
	}
}

void VIA::VIAPoll(int cycles)
{
	//Ensure only lowest byte exists
	cycles &= 0xFF;

	if(cycles > T1C_L)
	{
		if(T1C_H == 0)
		{
			VIAUpdate();
		}
		else
		{
			T1C_H--;
		}
	}

	T1C_L -= cycles;

	if (!((ACR & 0x20) > 0))
	{
		if(cycles > T2C_L)
		{
			if(T2C_H == 0)
			{
				VIAUpdate();
			}
			else
			{
				T2C_H--;
			}
		}

		T2C_L -= cycles;
	}
	
	// Ensure that CA2 keyboard interrupt is asserted when key pressed
	//DoKbdIntCheck(); 
}

void VIA::updateIFR()
{
	if ((IFR & (IER & 0x7f)) > 0)
	{
		IFR |= 0x80;
	}
	else
	{
		IFR &= 0x7f;
	}
}

BIT_8 VIA::getIFR()
{
	return IFR;
}

void VIA::writeIC32(BIT_8 val)
{
	int bit;
	
	bit = val & 7;
	if (val & 8)
	{
		IC32 |= (1 << bit);
	}
	else
	{
		IC32 &= 0xFF - (1 << bit);
	}
}

void VIA::slowDataBusWrite(BIT_8 val)
{
	//Implementation specific
}

BIT_8 VIA::slowDataBusRead()
{
	//Implementation specific
	return 0;
}