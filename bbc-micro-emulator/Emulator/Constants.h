#ifndef _CONSTANTS
#define _CONSTANTS

const int KB64 = 65536;
const int KB57 = 57344;
const int KB32 = 32768;
const int KB16 = 16384;
const int KB8 = 8192;

#endif