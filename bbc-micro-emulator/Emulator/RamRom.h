#include "Types.h"
#include "Constants.h"
#include "SystemVIA.h"
#include "UserVIA.h"
#include "Video.h"
#include "RS423Serial.h"

#ifndef _RAMROM_
#define _RAMROM_

const int OS_START_ADDR = 0xC000;
const int ROM_START_ADDR = 0x8000;
const bool READ = true;
const bool WRITE = false;
class MPU;

/****************************************
*RAM/ROM class							*
*Contains memory array, a pointer to	*
*where ROM starts, pointers to reserved *
*I/O memory space						*
****************************************/
class RAM_ROM
{
private:
	static BIT_8 _memory[KB64];
	static BIT_8 _FRED[0x100];
	static BIT_8 _JIM[0x100];
	static BIT_8 _SHEILA[0x100];
	static BIT_8 _ROMSpace[KB16];
	static BIT_8 sideWaysROMs[16][KB16];
	static SystemVIA sysVIA;
	static UserVIA usrVIA;
	static Video video;
	static RS423Serial serial;
	
	//Whether it is read or write, introduced to handle VIA I/O
	//True for read, false for write
	bool operation;

	bool sysVIAWrite;
	bool usrVIAWrite;
	bool _6845Write;
	bool ULAWrite;
	bool ROMLatchWrite;
	bool serialULAWrite;
	
	BIT_8 oldROMLatch;
	
	void copyKB16(BIT_8 *lhs, BIT_8 *rhs);
	
public:
	RAM_ROM();
	~RAM_ROM();

	//Temp reference for I/O reads/writes
	BIT_8& tmpRef;
	BIT_8 t;

	// Square brackets overloaded to allow memory access from non-member non-friend functions
	BIT_8& operator[](const BIT_16 &i);
	void load(BIT_8 *arr, int j);
	void loadOS(BIT_8 *os);
	void loadROM(BIT_8 *os, BIT_8 slot);
	void swapROM(int rom);
	BIT_8& systemVIARW(BIT_8 i);
	BIT_8& userVIARW(BIT_8 i);
	BIT_8& CRTCRW(BIT_8 i);
	BIT_8& ULARW(BIT_8 i);
	BIT_8& SerialULARW(BIT_8 i);
	BIT_8& SHEILAROMLatch(BIT_8 i);
	void updateSysVIARegisters();
	void updateUserVIARegisters();
	void update6845Register();
	void updateULARegisters();
	void updateSerialULARegisters();
	void updateROM();
	void toggleOperation(bool read_write);
	BIT_8 updateSysVIA(int cycles);
	BIT_8 updateUserVIA(int cycles);
	void resetSysVIA();
	void resetUserVIA();
	bool writeToSysVIA();
	bool writeToUserVIA();
	bool writeTo6845();
	bool writeToULA();
	bool writeToSerialULA();
	bool writeToROMLatch();
	void sysVIAKeyDown(int row, int col);
	void sysVIAKeyUp(int row, int col);

	static void resetMemory(RAM_ROM *toReset);
	static void resetROMs();
	
};

#endif