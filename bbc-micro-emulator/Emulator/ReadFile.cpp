#include "ReadFile.h"

BIT_8* readFile(std::string filename)
{
	int length;
	char *buf;
	//Create ifstream object for reading in binary mode. The pointer will be at the end of the file
	std::ifstream file (filename, std::ios::in | std::ios::binary | std::ios::ate);

	//Get location of pointer, it's at the end so it's the length
	length = file.tellg();
	buf = new char[length];
	//Put pointer back to the start so we can read from the file
	file.seekg (0, std::ios::beg);
	//Copy file into buf
	file.read (buf,length);
	//Close stream
	file.close();
	
	return (BIT_8*)buf;
}

BIT_8* readOSROM()
{
	return readFile("OS12.ROM");
}

BIT_8* readBASICROM()
{
	return readFile("BASIC2.ROM");
}