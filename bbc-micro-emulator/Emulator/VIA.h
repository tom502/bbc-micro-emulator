#include "Types.h"

#ifndef __VIA__
#define __VIA__

class VIA
{
protected:

	//IC32 state is an address latch to handle control lines CA1, CA2, CB1 and CB2
	// operation not well documented so taken from BeebEm
	BIT_8 IC32;
	
	/** Registers **/

	//Input and output registers "B"
	//Read
	BIT_8 IRB;
	//Write
	BIT_8 ORB;

	//Input and output registers "A"
	//Read
	BIT_8 IRA;
	//Write
	BIT_8 ORA;
	//Same but no "handshake"
	BIT_8 IRA_n;
	BIT_8 ORA_n;

	//Data Direction Register "B"
	BIT_8 DDRB;
	
	//Data Direction Register "A"
	BIT_8 DDRA;

	//Shift Register - I can't see what this is used for anywhere.
	// As the chip wasn't designed for the Micro explicitly it could be a surplus feature
	// Partially implemented just to keep all registers present
	BIT_8 SR;
	BIT_8 SRMode;

	//Auxilliary Control Register
	BIT_8 ACR;

	//Peripheral Control Register
	BIT_8 PCR;

	//Interrupt Flag Register
	BIT_8 IFR;

	//Interrupt Enable Register
	BIT_8 IER;

	/*Timers
	 * _L and _H define high and low orders
	 * C and L define counters or latches*/
	//Counters
	BIT_8 T1C_L;
	BIT_8 T1C_H;
	BIT_8 T2C_L;
	BIT_8 T2C_H;
	//Latches
	BIT_8 T1L_L;
	BIT_8 T1L_H;
	BIT_8 T2L_L;
	BIT_8 T2L_H;

	bool timer1hasshot;
	bool timer2hasshot;

public:

	VIA();
	virtual ~VIA();

	void resetVIA();
	virtual BIT_8 readVIA(short int reg);
	virtual void writeVIA(short int reg, BIT_8 value);
	void VIAUpdate();
	void VIAPoll(int ncycles);
	void updateIFR();
	BIT_8 getIFR();
	void writeIC32(BIT_8 val);
	virtual BIT_8 slowDataBusRead();
	virtual void slowDataBusWrite(BIT_8 value);
};


#endif
