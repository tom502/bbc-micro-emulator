#include "Types.h"
#include <utility>

#ifndef __Video__
#define __Video__

//This class houses access to the video ULA chip and the 6845 CRTC (cathode ray tube controller) chip
class Video
{
private:
	/* Video ULA */
	//Control register
	BIT_8 ULA_CR;
	//Palette is 64 bit RAM which is used an 8 byte table
	//where top 4 bits of each byte are the logical colour and bottom 4 bits are actual colour
	//To represent this there is a list of pairs where the first value is the key (top 4 bits)
	//and the second value is the actual colour (bottom 4)
	std::pair <BIT_8, BIT_8> palette[8];


	/* Registers of the 6845 */
	//R0 to R13 are write only
	//R14/15 are read and write
	//R16/17 are read only

	//Horizontal total register R0
	BIT_8 R0_HTR;
	//Horizontal display register R1
	BIT_8 R1_HDR;
	//Horizontal sync position register R2
	BIT_8 R2_HSPR;
	//Sync width register R3
	BIT_8 R3_SWR;
	//Vertical total register R4
	BIT_8 R4_VTR;
	//Vertical total adjust register R5
	BIT_8 R5_VTAR;
	//Vertical displayed register R6
	BIT_8 R6_VDR;
	//Vertical sync position R7
	BIT_8 R7_VSP;
	//Interlace and delay register R8
	BIT_8 R8_IDR;
	//Scan lines per character R9
	BIT_8 R9_SLPC;
	//Cursor start register R10
	BIT_8 R10_CSR;
	//Cursor end register R11
	BIT_8 R11_CER;
	//Displayed screen start address register R12 and R13
	//14 bit register
	//R12 - 6 high bits
	BIT_8 R12_DSSAR;
	//R13 - 8 low bits
	BIT_8 R13_DSSAR;
	//Cursor position register R14 and R15
	//14 bit register
	//R14 - 6 high bits
	BIT_8 R14_CPR;
	//R15 - 8 low bits
	BIT_8 R15_CPR;
	//Light pen position register R16 and R17
	//14 bit register
	//R16 - 6 high bits
	BIT_8 R16_LPPR;
	//R17 - 8 low bits
	BIT_8 R17_LPPR;

public:
	Video();
	~Video();

	BIT_8 read6845(BIT_8 reg);
	void write6845(BIT_8 reg, BIT_8 value);
	void writeULA(BIT_8 reg, BIT_8 value);
	BIT_8 readULA(BIT_8 reg);
	void getPalettePair(BIT_8 key, std::pair <BIT_8, BIT_8> **ret);
};

#endif