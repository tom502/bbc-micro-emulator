#include "MPU.h"
#include "RamRom.h"
#include "Types.h"
#include "ReadFile.h"

#ifndef __BEEB__
#define __BEEB__

class BBCEM
{
private:
	bool loadBASIC;
	void boot();
public:
	MPU mpu;
	RAM_ROM memory;
	void reset();
	void load();
	void toggleBASIC(bool b);
	int timeToRunXInstructions(int x);
	void keyDown(int row, int col);
	void keyUp(int row, int col);
	BBCEM(bool useBASIC);
	void setThrottle(bool b);
	~BBCEM();
};

#endif
