#include "SystemVIA.h"


SystemVIA::SystemVIA()
{
	resetKeyStates();
	resetVIA();
}

SystemVIA::~SystemVIA()
{

}

void SystemVIA::resetKeyStates()
{
	for(int i = 0; i < 16; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			keyStates[i][j] = false;
		}
	}
}

BIT_8 SystemVIA::SysVIAPoll(int cycles)
{
	VIAPoll(cycles);
	return getIFR();
}

void SystemVIA::keyDown(int row, int col)
{
	if ((!keyStates[col][row]) && (row != 0))
	{
		keysDown++;
	}

	keyStates[col][row] = true;
	updateKeyboard();
}

void SystemVIA::keyUp(int row, int col)
{
	if (row >= 0 && col >= 0)
	{
		// Update keys down count - unless its shift/control
		if ((keyStates[col][row]) && (row != 0))
		{
			keysDown--;
		}

		keyStates[col][row] = false;
	}
}

bool SystemVIA::getKeyState()
{
	if ((keyCol <= 14) && (keyRow <= 7))
	{
		return keyStates[keyCol][keyRow];
	}
	else
	{
		return false;
	}
}

//Only thing currently using the slow data bus is the keyboard
//Anything else which would be considered here (for example sound) has been omitted
void SystemVIA::slowDataBusWrite(BIT_8 val)
{
	if ((IC32 & 8) == 0)
	{
		keyRow = (val >> 4) & 7;
		keyCol  = (val & 0x0F);
		updateKeyboard();
	}
}

BIT_8 SystemVIA::slowDataBusRead()
{
	BIT_8 ret = 0;
	ret = (IRA & DDRA);
  
	if (getKeyState())
	{
		ret |= 0xF0; 
	}

	if (!(IC32 & 4))
	{
		ret = 0xff;
	}

	if (!((IC32 & 8) > 0))
	{ 
		if (getKeyState())
		{
			ret |= 128;
		}
	}

	return ret;
}

void SystemVIA::updateKeyboard()
{
	if ((keysDown > 0) && ((PCR & 0xC) == 4))
	{
		if ((IC32 & 8)==8)
		{
			IFR |= 1; //Interrupt on CA2
			
			updateIFR();
		}
		else
		{
			if (keyCol < 15)
			{
				int presrow;
				for(presrow = 1; presrow < 8; presrow++)
				{
					if (keyStates[keyCol][presrow])
					{
						IFR |= 1;
						updateIFR();
					}
				} 
			} 
		} 
	} 
}

BIT_8 SystemVIA::readVIA(short int reg)
{
	//Strip out the value to leave a range of 0x0 - 0xF
	reg &= 0xF;

	BIT_8 ret = VIA::readVIA(reg);

	if(reg == 0)
	{
		//Speech system is not present
		ret |= 0x80;
	}
	return ret;
}

void SystemVIA::writeVIA(short int reg, BIT_8 value)
{
	//Strip out the value to leave a range of 0x0 - 0xF
	reg &= 0xF;

	if(reg == 0)
	{
		writeIC32(value);
	}

	//VIA::writeVIA(reg, value);
}