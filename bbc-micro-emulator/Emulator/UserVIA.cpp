#include "UserVIA.h"


UserVIA::UserVIA()
{
}


UserVIA::~UserVIA()
{
}

BIT_8 UserVIA::UserVIAPoll(int cycles)
{
	VIAPoll(cycles);
	return getIFR();
}

void UserVIA::slowDataBusWrite(BIT_8 val)
{
	
}

BIT_8 UserVIA::slowDataBusRead()
{
	BIT_8 ret = 0;
	ret = (IRA & DDRA);

	return ret;
}

BIT_8 UserVIA::readVIA(short int reg)
{
	//Strip out the 0x40 value to leave a range of 0x0 - 0xF
	reg &= 0xF;

	BIT_8 ret = VIA::readVIA(reg);

	return ret;
}

void UserVIA::writeVIA(short int reg, BIT_8 value)
{
	//Strip out the 0x40 value to leave a range of 0x0 - 0xF
	reg &= 0xF;
}