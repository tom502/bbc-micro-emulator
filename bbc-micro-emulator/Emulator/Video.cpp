#include "Video.h"


Video::Video()
{
}


Video::~Video()
{
}

BIT_8 Video::read6845(BIT_8 reg)
{
	//Strip to leave a range of 0x0 - 0xF
	reg &= 0xF;

	BIT_8 ret = 0xFF;
	
	//The only registers that allow a write
	if(reg == 14)
	{
		ret = R14_CPR;
	}
	else if(reg == 15)
	{
		ret = R15_CPR;
	}
	else if(reg == 16)
	{
		ret = R16_LPPR;
	}
	else if(reg == 17)
	{
		ret = R17_LPPR;
	}	

	return ret;
}

void Video::write6845(BIT_8 reg, BIT_8 value)
{
	//Strip to leave a range of 0x0 - 0xF
	reg &= 0xF;

	if(reg == 0)
	{
		R0_HTR = value;
	}
	else if(reg == 1)
	{
		R1_HDR = value;
	}
	else if(reg == 2)
	{
		R2_HSPR = value;
	}
	else if(reg == 3)
	{
		R3_SWR = value;
	}
	else if(reg == 4)
	{
		R4_VTR = value;
	}
	else if(reg == 5)
	{
		//Only a 5 bit register
		R5_VTAR = value & 0x1F;
	}
	else if(reg == 6)
	{
		R6_VDR = value;
	}
	else if(reg == 7)
	{
		R7_VSP = value & 0x7F;
	}
	else if(reg == 8)
	{
		R8_IDR = value;
	}
	else if(reg == 9)
	{
		R9_SLPC = value;
	}
	else if(reg == 10)
	{
		R10_CSR = value;
	}
	else if(reg == 11)
	{
		R11_CER = value;
	}
	else if(reg == 12)
	{
		R12_DSSAR = value;
	}
	else if(reg == 13)
	{
		R13_DSSAR = value;
	}
	else if(reg == 14)
	{
		//Only 6 bits
		R14_CPR = value & 0x3F;
	}
	else if(reg == 15)
	{
		R15_CPR = value;
	}
	/*else if(reg == 16)
	{
		R16_LPPR = value;
	}
	else if(reg == 17)
	{
		R17_LPPR = value;
	}*/	
}

void Video::writeULA(BIT_8 reg, BIT_8 value)
{
	//Control register
	if(reg == 0x20)
	{
		ULA_CR = value;
	}
	//Palette
	else if(reg == 0x21)
	{
		//First 4 bits are the "key" as logical colour
		//Second 4 bits are the value to set
		BIT_8 key = (value & 0xF0) >> 4;
		//Drop higher nibble
		value &= 0x0F;
		//EOR with 7 - inverts 3 colour bits and flash bit
		value ^= 7;

		std::pair <BIT_8, BIT_8> *p;
		getPalettePair(key, &p);
		p->second = value;
	}
}

BIT_8 Video::readULA(BIT_8 reg)
{
	//Read for ULA is undefined, just return reg
	return reg;
}

void Video::getPalettePair(BIT_8 key, std::pair <BIT_8, BIT_8> **ret)
{
	for(int i=0; i < 8; i++)
	{
		if(palette[i].first == key)
		{
			*ret = &palette[i];
		}
	}
}