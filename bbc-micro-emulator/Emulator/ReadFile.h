#include <iostream>
#include <fstream>
#include <string>

#include "Types.h"

#ifndef _READFILE__
#define _READFILE__

BIT_8* readOSROM();
BIT_8* readBASICROM();
BIT_8* readFile(std::string filename);

#endif