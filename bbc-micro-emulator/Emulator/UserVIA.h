#include "VIA.h"

#ifndef _USERVIA_
#define _USERVIA_

class UserVIA : public VIA
{
public:
	UserVIA();
	~UserVIA();

	BIT_8 UserVIAPoll(int cycles);
	BIT_8 slowDataBusRead();
	void slowDataBusWrite(BIT_8 value);
	BIT_8 readVIA(short int reg);
	void writeVIA(short int reg, BIT_8 value);
};

#endif