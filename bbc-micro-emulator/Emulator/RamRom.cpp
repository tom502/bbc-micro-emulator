#include "RamRom.h"
#include <process.h>
#include <windows.h>
#include <stdio.h>

BIT_8 RAM_ROM::_memory[];
BIT_8 RAM_ROM::_FRED[];
BIT_8 RAM_ROM::_JIM[];
BIT_8 RAM_ROM::_SHEILA[];
BIT_8 RAM_ROM::_ROMSpace[];
BIT_8 RAM_ROM::sideWaysROMs[16][KB16];
SystemVIA RAM_ROM::sysVIA;
UserVIA RAM_ROM::usrVIA;
Video RAM_ROM::video;
RS423Serial RAM_ROM::serial;

RAM_ROM::RAM_ROM()
		: tmpRef(t)
{
	//More instructions to read memory than to write to it so default is READ
	operation = READ;

	oldROMLatch = 0;

	for(int i = 0; i < 16; i++)
	{
		for(int j = 0; j < KB16; j++)
		{
			sideWaysROMs[i][j] = 0;
		}
	}

	sysVIAWrite = false;
	usrVIAWrite = false;
	_6845Write = false;
	ULAWrite = false;
	serialULAWrite = false;
}

RAM_ROM::~RAM_ROM()
{
	
}

BIT_8& RAM_ROM::operator[](const BIT_16 &i)
{
	if(i >= 0xFE00 && i <= 0xFEFF)
	{	
		if(i == 0xFE01)
		{
			//6845
			return CRTCRW(_SHEILA[0x00]);
		}
		else if(i == 0xFE08 || i == 0xFE09 || i == 0xFE10)
		{
			return SerialULARW(i);
		}
		else if(i == 0xFE30)
		{
			return SHEILAROMLatch(i);
		}
		else if((i >= 0xFE40) && (i <= 0xFE5F))
		{
			return systemVIARW(i);
		}
		else if((i >= 0xFE60) && (i <= 0xFE7F))
		{
			return userVIARW(i);
		}
		else if(i == 0xFE20 || i == 0xFE21)
		{
			return ULARW(i);
		}
		return _SHEILA[i - 0xFE00];
	}
	else if(i >= 0xFD00 && i <= 0xFDFF)
	{
		return _JIM[i - 0xFD00];
	}
	else if(i >= 0xFC00 && i <= 0xFCFF)
	{
		return _FRED[i - 0xFC00];
	}
	else if(i >= 0x8000  && i <= 0xC000)
	{
		return _ROMSpace[i - 0x8000];
	}
	else
	{
		return _memory[i];
	}
}

void RAM_ROM::swapROM(int rom)
{
	load(sideWaysROMs[rom], ROM_START_ADDR);
}

void RAM_ROM::loadOS(BIT_8 *os)
{
	load(os, OS_START_ADDR);
}

void RAM_ROM::loadROM(BIT_8 *rom, BIT_8 slot)
{
	//load(rom, ROM_START_ADDR);
	copyKB16(sideWaysROMs[slot], rom);
}

void RAM_ROM::load(BIT_8 *arr, int j)
{
	if(j == ROM_START_ADDR)
	{
		copyKB16(_ROMSpace, arr);
	}
	else
	{
		for(int i = 0; i < KB16; i++, j++)
		{
			_memory[j] = arr[i];
		}
	}
}

void RAM_ROM::resetMemory(RAM_ROM *toReset)
{
	for(int i = 0; i < KB64; i++)
	{
		if(i <= 0xFF)
		{
			_FRED[i] = _JIM[i] = _SHEILA[i] = 0;
		}
		_memory[i] = 0;
	}
	resetROMs();
	toReset->resetSysVIA();
	toReset->resetUserVIA();
}

void RAM_ROM::resetROMs()
{
	for(int i = 0; i < 0x10; i++)
	{
		for(int j = 0; j < 0xFF; j++)
		{
			sideWaysROMs[i][j] = 0;
		}
	}
}

void RAM_ROM::copyKB16(BIT_8 *lhs, BIT_8 *rhs)
{
	for(int i = 0; i < KB16; i++)
	{
		lhs[i] = rhs[i];
	}
}

BIT_8& RAM_ROM::systemVIARW(BIT_8 i)
{
	//Strip page off address
	i = i & 0xFF;
	
	if(operation == READ)
	{	
		tmpRef = sysVIA.readVIA(i);
		return tmpRef;
	}
	else
	{
		sysVIAWrite = true;
		return _SHEILA[i];
	}
}

BIT_8& RAM_ROM::userVIARW(BIT_8 i)
{
	//Strip page off address
	i = i & 0xFF;
	
	if(operation == READ)
	{	
		tmpRef = usrVIA.readVIA(i);
		return tmpRef;
	}
	else
	{
		usrVIAWrite = true;
		return _SHEILA[i];
	}
}


BIT_8& RAM_ROM::CRTCRW(BIT_8 i)
{
	//Strip page off address
	i = i & 0xFF;
	
	if(operation == READ)
	{	
		tmpRef = video.read6845(i);
		return tmpRef;
	}
	else
	{
		_6845Write = true;
		return _SHEILA[i];
	}
}

BIT_8& RAM_ROM::ULARW(BIT_8 i)
{
	//Strip page off address
	i = i & 0xFF;
	
	if(operation == WRITE)
	{	
		ULAWrite = true;
		return _SHEILA[i];
	}
	else
	{
		
		//Read from ULA is undefined
		//tmpRef = video.readULA(i);
		tmpRef = 0xBB;
		return tmpRef;
	}
}

BIT_8& RAM_ROM::SerialULARW(BIT_8 i)
{
	//Strip page off address
	i = i & 0xFF;
	
	if(operation == WRITE)
	{	
		serialULAWrite = true;
		return _SHEILA[i];
	}
	else
	{
		tmpRef = serial.readSerial(i);
		return tmpRef;
	}
}

BIT_8& RAM_ROM::SHEILAROMLatch(BIT_8 i)
{
	//Strip page off address
	i = i & 0xFF;

	if(operation == READ)
	{	
		tmpRef = _SHEILA[i];
		return tmpRef;
	}
	else
	{
		ROMLatchWrite = true;
		return _SHEILA[i];
	}
}

void RAM_ROM::updateSysVIARegisters()
{
	for(int i = 0; i < 16; i++)
	{
		sysVIA.writeVIA(i, _SHEILA[i | 0x40]);
	}
	sysVIAWrite = false;
}

void RAM_ROM::update6845Register()
{
	video.write6845(_SHEILA[0x00], _SHEILA[0x01]);
	_6845Write = false;
}

void RAM_ROM::updateULARegisters()
{
	video.writeULA(0x20, _SHEILA[0x20]);
	video.writeULA(0x21, _SHEILA[0x21]);
	ULAWrite = false;
}

void RAM_ROM::updateSerialULARegisters()
{
	serial.writeSerial(0x98, _SHEILA[0x08]);
	serial.writeSerial(0x09, _SHEILA[0x09]);
	serial.writeSerial(0x10, _SHEILA[0x10]);
	serialULAWrite = false;
}

void RAM_ROM::toggleOperation(bool read_write)
{
	operation = read_write;
}

BIT_8 RAM_ROM::updateSysVIA(int cycles)
{
	return sysVIA.SysVIAPoll(cycles);
}

void RAM_ROM::resetSysVIA()
{
	sysVIA.resetKeyStates();
	sysVIA.resetVIA();
}

bool RAM_ROM::writeToSysVIA()
{
	return sysVIAWrite;
}

BIT_8 RAM_ROM::updateUserVIA(int cycles)
{
	return usrVIA.UserVIAPoll(cycles);
}

void RAM_ROM::resetUserVIA()
{
	usrVIA.resetVIA();
}

bool RAM_ROM::writeToUserVIA()
{
	return usrVIAWrite;
}

bool RAM_ROM::writeToULA()
{
	return ULAWrite;
}

bool RAM_ROM::writeTo6845()
{
	return _6845Write;
}

bool RAM_ROM::writeToROMLatch()
{
	return ROMLatchWrite;
}

bool RAM_ROM::writeToSerialULA()
{
	return serialULAWrite;
}

void RAM_ROM::updateROM()
{
	BIT_8 romLatch = _SHEILA[0x30];

	if(romLatch != oldROMLatch && romLatch < 16)
	{
		swapROM(romLatch);
		oldROMLatch = romLatch;
	}
}

void RAM_ROM::sysVIAKeyDown(int row, int col)
{
	sysVIA.keyDown(row,col);
}

void RAM_ROM::sysVIAKeyUp(int row, int col)
{
	sysVIA.keyUp(row,col);
}