#include "Types.h"

#ifndef __RS423__
#define __RS423__

class RS423Serial
{
	BIT_8 TDR;
	BIT_8 RDR;
	BIT_8 ControlR;
	BIT_8 SULAControl;
	BIT_8 Status;

public:
	RS423Serial();
	~RS423Serial();
	BIT_8 readSerial(short int reg);
	void writeSerial(short int reg, BIT_8 value);
};

#endif