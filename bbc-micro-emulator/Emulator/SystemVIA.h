#include "VIA.h"

#ifndef _SYSVIA_
#define _SYSVIA_

class SystemVIA : public VIA
{
private:
	bool keyStates[16][8];
	int keysDown;
	short int keyCol;
	short int keyRow;

public:
	SystemVIA();
	~SystemVIA();

	void keyDown(int row, int col);
	void keyUp(int row, int col);
	BIT_8 SysVIAPoll(int cycles);
	BIT_8 slowDataBusRead();
	void slowDataBusWrite(BIT_8 value);
	void updateKeyboard();
	bool getKeyState();
	BIT_8 readVIA(short int reg);
	void writeVIA(short int reg, BIT_8 value);
	void resetKeyStates();
};

#endif