#include "MPU.h"
#include "Constants.h"
#include <time.h>
#include <string>

/****************
* ProgramCounter
*****************/
#pragma region ProgramCounter

//Constructor
ProgramCounter::ProgramCounter()
{}

//Copy Constructor
ProgramCounter::ProgramCounter(const ProgramCounter& rhs)
{
    //Deep copy
	_PC = rhs._PC;
	_PCL = rhs._PCL;
	_PCH = rhs._PCH;
}

//Set the high and low bytes from 2 byte value
void ProgramCounter::setHighLow()
{
    //Most significant 8 bits lost, lower byte is set
	_PCL = _PC;
	//Shift to get most significant byte - high byte
	_PCH = _PC >> 8;
}

//Get value
BIT_16 ProgramCounter::PC()
{
	return _PC;
}

//Get high byte
BIT_8 ProgramCounter::PCH()
{
	return _PCH;
}

//Get low byte
BIT_8 ProgramCounter::PCL()
{
	return _PCL;
}

//The AND masks zero the high or low bytes
//and save the rest so a simple addition will suffice to update

//Set high byte and then correct 2 byte value
void ProgramCounter::PCH(BIT_8 a)
{
	_PCH = a;
	//_PCH gets shifted left 8 bits to become the same size type as _PC
	_PC = (_PC & 0x00FF) + (_PCH << 8);
}

//Set high byte and then correct 2 byte value
void ProgramCounter::PCH(int a)
{
	_PCH = a;
	//_PCH gets shifted left 8 bits to become the same size type as _PC
	_PC = (_PC & 0x00FF) + (_PCH << 8);
}

//Set low byte and then correct 2 byte value
void ProgramCounter::PCL(BIT_8 a)
{
	_PCL = a;
	_PC = (_PC & 0xFF00) + _PCL;
}

//Set low byte and then correct 2 byte value
void ProgramCounter::PCL(int a)
{
	_PCL = a;
	_PC = (_PC & 0xFF00) + _PCL;
}

//Overload operator so that we can manipulate the object like a value
//They will also update the high/low bytes
ProgramCounter& ProgramCounter::operator=(const ProgramCounter& rhs)
{
	this->_PC = rhs._PC;
	setHighLow();
	return *this;
}

ProgramCounter& ProgramCounter::operator=(const BIT_16& rhs)
{
	this->_PC = rhs;
	setHighLow();
	return *this;
}

ProgramCounter& ProgramCounter::operator++()
{
	this->_PC++;
	setHighLow();
	return *this;
}

ProgramCounter& ProgramCounter::operator++(int)
{
	this->_PC++;
	setHighLow();
	return *this;
}

ProgramCounter& ProgramCounter::operator--()
{
	this->_PC--;
	setHighLow();
	return *this;
}

ProgramCounter& ProgramCounter::operator--(int)
{
	this->_PC--;
	setHighLow();
	return *this;
}

ProgramCounter ProgramCounter::operator+(const int& rhs)
{
	ProgramCounter tmp(*this);
	tmp += rhs;
	setHighLow();
	return tmp;
}

ProgramCounter ProgramCounter::operator-(const int& rhs)
{
	ProgramCounter tmp(*this);
	tmp -= rhs;
	setHighLow();
	return tmp;
}

ProgramCounter& ProgramCounter::operator+=(const int& rhs)
{
	this->_PC += rhs;
	setHighLow();
	return *this;
}

ProgramCounter& ProgramCounter::operator-=(const int& rhs)
{
	this->_PC -= rhs;
	setHighLow();
	return *this;
}

#pragma endregion

/**************************************
* Flags definitions
**************************************/

//5th bit is always 1 so set in initialiser
Flags::Flags()
	: _p(32)
{}

//Get functions
bool Flags::N()
{
	return _p[7];
}
bool Flags::V()
{
	return _p[6];
}
bool Flags::B()
{
	return _p[4];
}
bool Flags::D()
{
	return _p[3];
}
bool Flags::I()
{
	return _p[2];
}
bool Flags::Z()
{
	return _p[1];
}
bool Flags::C()
{
	return _p[0];
}

//Set functions
void Flags::N(bool on)
{
	_p[7] = on ? 1 : 0;
}
void Flags::V(bool on)
{
	_p[6] = on ? 1 : 0;
}
void Flags::B(bool on)
{
	_p[4] = on ? 1 : 0;
}
void Flags::D(bool on)
{
	_p[3] = on ? 1 : 0;
}
void Flags::I(bool on)
{
	_p[2] = on ? 1 : 0;
}
void Flags::Z(bool on)
{
	_p[1] = on ? 1 : 0;
}
void Flags::C(bool on)
{
	_p[0] = on ? 1 : 0;
}

//Get overall value
BIT_8 Flags::getValue()
{
	return _p.to_ulong();
}

//Set overall value
void Flags::setValue(int val)
{
	_p = std::bitset<8>(val);
}


/**************************************
* MPU Definitions
**************************************/

MPU::MPU()
	: P()
	, PC()
	, X(0)
	, Y(0)
	, A(0)
	, S(0xFF)
	, memory()
	, totalCycles(0)
	, totalExecuted(0)
{
	init();
	branch = modifiedPC = false;
	justInterrupted = false;
	writeA = false;
	throttle = false;
	QueryPerformanceFrequency(&f);
	sleep = 0;
	QueryPerformanceFrequency(&li);
	QueryPerformanceFrequency(&li2);
	overhead.QuadPart = li2.QuadPart - li.QuadPart;
}

MPU::~MPU()
{
	delete[] args;
/*	delete[] &opCodes;
	delete[] &_ADC;
	delete[] &_AND;
	delete[] &_ASL;
	delete[] &_BIT;
	delete[] &_CMP;
	delete[] &_CPX;
	delete[] &_CPY;
	delete[] &_DEC;
	delete[] &_EOR;
	delete[] &_INC;
	delete[] &_JMP;
	delete[] &_LDA;
	delete[] &_LDX;
	delete[] &_LDY;
	delete[] &_LSR;
	delete[] &_ORA;
	delete[] &_ROL;
	delete[] &_ROR;
	delete[] &_SBC;
	delete[] &_STA;
	delete[] &_STX;
	delete[] &_STY;*/
}

BIT_8 MPU::bytesToFollow[];
BIT_8 MPU::cycles[];

void MPU::setThrottle(bool b)
{
	throttle = b;
}

//Get number of bytes for given op code
BIT_8 MPU::getBytesToFollow(int index)
{
	return bytesToFollow[index];
}

//Get number of cycles for given op code
BIT_8 MPU::getCycles(int index)
{
	return cycles[index];
}

int MPU::getTotalCycles()
{
	return totalCycles;
}
void MPU::setTotalCycles(int x)
{
	totalCycles = x;
}

int MPU::getTotalExecuted()
{
	return totalExecuted;
}
void MPU::setTotalExecuted(int x)
{
	totalExecuted = x;
}

int MPU::getStatusRegisterValue()
{
	return P.getValue();
}
int MPU::getProgramCounterValue()
{
	return PC.PC();
}

//Get various registers
int MPU::getX()
{
	return X;
}
int MPU::getY()
{
	return Y;
}
int MPU::getA()
{
	return A;
}
int MPU::getS()
{
	return S;
}

//Needed as stack pointer register is a 9bit register
BIT_16 MPU::stackPointer()
{
	return 0x100 + S;
}

void MPU::resetRegisters()
{
	PC = 0;
	X = 0;
	Y = 0;
	A = 0;
	P.setValue(32);
}

void MPU::loadProgram(BIT_16 iMemLoc)
{
    //Set the program counter to location 'iMemloc'
	PC = iMemLoc;
}

//Push data on the stack and move stack pointer
void MPU::addToStack(BIT_8 data)
{
    //Add data to stack
	memory[stackPointer()] = data;
	//Move stack pointer on
	S--;
}

//Pop data from stack and move stack pointer
BIT_8 MPU::popFromStack()
{
    //Move stack pointer first because it is pointing at the next empty slot
	S++;
	//Pull data off - stack pointer is now pointing at this
	BIT_8 data = memory[stackPointer()];
	return data;
}


/**************************************************************
* Memory addressing functions								  *
* Only to be used to use data, not set						  *
* No immediate, just causes function overhead				  *
***************************************************************/

//Absolute addressing
BIT_8 MPU::Absolute(const BIT_8 args[])
{
	//Return the value at the address passed
	return memory[formAbsolute(args[1],args[0])];
}

BIT_16 MPU::AbsoluteJumps(const BIT_8 args[])
{
	//The arguments are the address
	return formAbsolute(args[1],args[0]);
}

BIT_8 MPU::Absolute_Source(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(args[1],args[0]);
	return memory[formAbsolute(args[1],args[0])];
}

BIT_8 MPU::AbsoluteX(const BIT_8 args[])
{
	return memory[formAbsolute(args[1],args[0]) + X];
}

BIT_8 MPU::AbsoluteX_Source(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(args[1],args[0]) + X;
	return memory[formAbsolute(args[1],args[0]) + X];
}

BIT_8 MPU::AbsoluteY(const BIT_8 args[])
{
	return memory[formAbsolute(args[1],args[0]) + Y];
}

BIT_8 MPU::AbsoluteY_Source(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(args[1],args[0]) + Y;
	return memory[formAbsolute(args[1],args[0]) + Y];
}

//Zero page addressing
BIT_8 MPU::ZeroPage(const BIT_8 args[])
{
	return memory[args[0]];
}

BIT_8 MPU::ZeroPage_Source(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = args[0];
	return memory[args[0]];
}

BIT_8 MPU::ZeroPageX(const BIT_8 args[])
{
	return memory[args[0] + X];
}

BIT_8 MPU::ZeroPageX_Source(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = args[0] + X;
	return memory[args[0] + X];
}

BIT_8 MPU::ZeroPageY(const BIT_8 args[])
{
	return memory[args[0] + Y];
}

BIT_8 MPU::ZeroPageY_Source(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = args[0] + Y;
	return memory[args[0] + Y];
}

BIT_16 MPU::Indirect(const BIT_8 args[])
{
	return formAbsolute(memory[formAbsolute(args[1], args[0]+1)], memory[formAbsolute(args[1], args[0])]);
}

BIT_8 MPU::IndirectX(const BIT_8 args[])
{
	return memory[ formAbsolute(memory[args[0] + 1] , memory[args[0]]) + X ];
}

BIT_8 MPU::IndirectX_Source(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(memory[args[0] + 1] , memory[args[0]]) + X ;
	return memory[ formAbsolute(memory[args[0] + 1] , memory[args[0]]) + X ];
}

BIT_8 MPU::IndirectY(const BIT_8 args[])
{
	return memory[ formAbsolute(memory[args[0] + 1] , memory[args[0]]) + Y ];
}

BIT_8 MPU::IndirectY_Source(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(memory[args[0] + 1] , memory[args[0]]) + Y;
	return memory[ formAbsolute(memory[args[0] + 1] , memory[args[0]]) + Y ];
}

BIT_8 MPU::Immediate(const BIT_8 args[])
{
	return args[0];
}

BIT_8 MPU::Accumulator(const BIT_8 args[])
{
	return A;
}

BIT_8 MPU::Accumulator_Source(BIT_16 *sourceAddr, const BIT_8 args[])
{
	writeA = true;
	return A;
}

//Functions to return memory address only

void MPU::Absolute_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(args[1],args[0]);
}

void MPU::ZeroPage_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = args[0];
}

void MPU::AbsoluteX_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(args[1],args[0]) + X;
}

void MPU::AbsoluteY_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(args[1],args[0]) + Y;
}

void MPU::IndirectX_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(memory[args[0] + 1] , memory[args[0]]) + X;
}

void MPU::IndirectY_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = formAbsolute(memory[args[0] + 1] , memory[args[0]]) + Y;
}

void MPU::ZeroPageX_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = args[0] + X;
}

void MPU::ZeroPageY_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[])
{
	*sourceAddr = args[0] + Y;
}

//Write to memory
__forceinline void MPU::write(BIT_16 * src, BIT_8 * d)
{
	//Toggle memory for writing
	memory.toggleOperation(WRITE);
	if(writeA)
	{
		A = *d;
		writeA = false;
	}
	else
	{
		memory[*src] = *d;
	}
	//Toggle memory for reading
	memory.toggleOperation(READ);
}

/*************************************
* Instruction set function definitions
*************************************/

void MPU::ADC(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;
	int temp;

	// Call addressing function using opcode as index
	data = (this->*_ADC[opcode])(args);

	if(!P.D())
	{
		temp = (signed char)A + (signed char)data + P.C();
		A = A + data + P.C();
		//If the most significant bits of A and temp differ then set overflow flag
		P.V((((A & 128) > 0) ^ ((temp & 256)!=0)) > 0);
		negOrZero(A);
	}
	else
	{
		int TmpCarry = 0;
		int ln, hn, olsn, omsn;

		temp = A + data + P.C();
		P.Z((temp & 0xFF) == 0);

		omsn = data & 0xF0;
		olsn = data & 0x0F;

		ln=(A & 0x0F) + olsn + P.C();

		if (ln > 9)
		{
			ln += 6;
			ln &= 0x0F;
			TmpCarry = 0x10;
		}

		hn = (A & 0xF0) + omsn + TmpCarry;
		/* N and V flags are determined before high nibble is adjusted.
		   NOTE: V is not always correct */
		P.N((hn & 128) > 0);
		P.V(((hn ^ A) & 128 && !((A ^ data) & 128)));
		if (hn > 0x90)
		{
			hn += 0x60;
			hn &= 0xF0;
			P.C(1);
		}
		A = hn | ln;
		P.Z(A == 0);
		P.N((A & 128) > 0);
	}
}

void MPU::AND(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;

	// Call addressing function using opcode as index
	data = (this->*_AND[opcode])(args);

	//Perform instruction
	A = A & data;
	//Set flags
	negOrZero(A);
}

void MPU::ASL(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;
	BIT_16 addr, *source = &addr;

	data = (this->*_ASL[opcode])(source, args);

	//Perform instruction
	//If MSB is 1, it falls into carry
	P.C((data & 128) > 0);
	//Shift left by 1
	data *= 2;
	//Place into source
	write(source, &data);
	//Set flags
	negOrZero(data);
}

void MPU::BCC(const BIT_8 args[], const BIT_8 opcode)
{
    //If the carry flag is not set branch
	if(P.C() == 0)
	{
		branchToAddr(args[0]);
	}
}

void MPU::BCS(const BIT_8 args[], const BIT_8 opcode)
{
    //If the carry flag is set branch
	if(P.C() == 1)
	{
		branchToAddr(args[0]);
	}
}

void MPU::BEQ(const BIT_8 args[], const BIT_8 opcode)
{
    //If the zero flag is set branch
	if(P.Z() == 1)
	{
		branchToAddr(args[0]);
	}
}

void MPU::BIT(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;

	data = (this->*_BIT[opcode])(args);

	if((data & A) == 0)
		P.Z(1);
	else
		P.Z(0);

	P.V(((data & 64) > 0));
	P.N(((data & 128) > 0));
}

void MPU::BMI(const BIT_8 args[], const BIT_8 opcode)
{
    //If the negative flag is set branch
	if(P.N() == 1)
	{
		branchToAddr(args[0]);
	}
}

void MPU::BNE(const BIT_8 args[], const BIT_8 opcode)
{
    //If the zero flag is not set branch
	if(P.Z() == 0)
	{
		branchToAddr(args[0]);
	}
}

void MPU::BPL(const BIT_8 args[], const BIT_8 opcode)
{
    //If the negative flag is not set branch
	if(P.N() == 0)
	{
		branchToAddr(args[0]);
	}
}

void MPU::BRK(const BIT_8 args[], const BIT_8 opcode)
{
    //Set break flag and move the program counter on 2
	P.B(1);
	PC += 2;
	//Add that to the stack
	addToStack(PC.PCH());
	addToStack(PC.PCL());
	//Add the status register to the stack
	addToStack(P.getValue());
    //Go to break address
	PC = formAbsolute(memory[0xFFFF], memory[0xFFFE]);
	modifiedPC = true;
}

void MPU::BVC(const BIT_8 args[], const BIT_8 opcode)
{
    //If overflow flag is not set branch
	if(P.V() == 0)
	{
		branchToAddr(args[0]);
	}
}

void MPU::BVS(const BIT_8 args[], const BIT_8 opcode)
{
    //If overflow flag is set branch
	if(P.V() == 1)
	{
		branchToAddr(args[0]);
	}
}

void MPU::CLC(const BIT_8 args[], const BIT_8 opcode)
{
	P.C(0);
}

void MPU::CLD(const BIT_8 args[], const BIT_8 opcode)
{
	P.D(0);
}

void MPU::CLI(const BIT_8 args[], const BIT_8 opcode)
{
	P.I(0);
}

void MPU::CLV(const BIT_8 args[], const BIT_8 opcode)
{
	P.V(0);
}

void MPU::CMP(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data,
		  temp;

	data = (this->*_CMP[opcode])(args);

	if(A >= data)
	{
		P.C(1);
	}
	else
	{
		P.C(0);
	}
	temp = A - data;

	negOrZero(temp);
	//carry(old, data);
}

void MPU::CPX(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data,
		  temp;

	data = (this->*_CPX[opcode])(args);

	if(X >= data)
	{
		P.C(1);
	}
	else
	{
		P.C(0);
	}
	temp = X - data;
	negOrZero(temp);
	//carry(old, data);
}

void MPU::CPY(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data,
		  temp;

	data = (this->*_CPY[opcode])(args);

	if(Y >= data)
	{
		P.C(1);
	}
	else
	{
		P.C(0);
	}
	temp = Y - data;
	negOrZero(temp);
	//carry(old, data);
}

void MPU::DEC(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;
	BIT_16 addr, *source = &addr;

	data = (this->*_DEC[opcode])(source, args);

	data--;
	negOrZero(data);
	write(source, &data);
}

void MPU::DEX(const BIT_8 args[], const BIT_8 opcode)
{
	X--;
	negOrZero(X);
}

void MPU::DEY(const BIT_8 args[], const BIT_8 opcode)
{
	Y--;
	negOrZero(Y);
}

void MPU::EOR(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;

	data = (this->*_EOR[opcode])(args);

	A ^= data;
	negOrZero(A);
}

void MPU::INC(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;
	BIT_16 addr, *source = &addr;

	data = (this->*_INC[opcode])(source, args);

	data++;
	negOrZero(data);
	write(source, &data);
}

void MPU::INX(const BIT_8 args[], const BIT_8 opcode)
{
	X++;
	negOrZero(X);
}

void MPU::INY(const BIT_8 args[], const BIT_8 opcode)
{
	Y++;
	negOrZero(Y);
}

void MPU::JMP(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_16 data;

	data = (this->*_JMP[opcode])(args);

	PC = data;
	modifiedPC = true;
}

void MPU::JSR(const BIT_8 args[], const BIT_8 opcode)
{
	PC += 2;
	addToStack(PC.PCH());
	addToStack(PC.PCL());
	PC = AbsoluteJumps(args);
	modifiedPC = true;
}

void MPU::LDA(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;

	data = (this->*_LDA[opcode])(args);

	A = data;
	negOrZero(A);
}

void MPU::LDX(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;

	data = (this->*_LDX[opcode])(args);

	X = data;
	negOrZero(X);
}

void MPU::LDY(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;

	data = (this->*_LDY[opcode])(args);

	Y = data;
	negOrZero(Y);
}

void MPU::LSR(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;
	BIT_16 addr, *source = &addr;
	P.N(0);

	data = (this->*_LSR[opcode])(source, args);

	P.C(data & 1);
	data >>= 1;
	negOrZero(data);

	write(source, &data);
}

void MPU::NOP(const BIT_8 args[], const BIT_8 opcode)
{

}

void MPU::ORA(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data;

	data = (this->*_ORA[opcode])(args);

	A |= data;
	negOrZero(A);
}

void MPU::PHA(const BIT_8 args[], const BIT_8 opcode)
{
	addToStack(A);
}

void MPU::PHP(const BIT_8 args[], const BIT_8 opcode)
{
	addToStack(P.getValue());
}

void MPU::PLA(const BIT_8 args[], const BIT_8 opcode)
{
	A = popFromStack();
	negOrZero(A);
}

void MPU::PLP(const BIT_8 args[], const BIT_8 opcode)
{
	P.setValue(popFromStack());
}

void MPU::ROL(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data, old;
	BIT_16 addr, *source = &addr;

	data = old = (this->*_ROL[opcode])(source, args);

	__asm
	{
		rol data,1
	}
	
	write(source, &data);
	negOrZero(data);
	carry(old, data);
}

void MPU::ROR(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data, old;
	BIT_16 addr, *source = &addr;

	data = old = (this->*_ROR[opcode])(source, args);

	__asm
	{
		ror data,1
	}

	write(source, &data);
	negOrZero(data);
	carry(old, data);
}

void MPU::RTI(const BIT_8 args[], const BIT_8 opcode)
{
	P.setValue(popFromStack());
	PC.PCL(popFromStack());
	PC.PCH(popFromStack());
	modifiedPC = true;
}

void MPU::RTS(const BIT_8 args[], const BIT_8 opcode)
{
	PC.PCL(popFromStack());
	PC.PCH(popFromStack());
	PC++;
	modifiedPC = true;
}

void MPU::SBC(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_8 data, old = A;
	int temp;

	data = (this->*_SBC[opcode])(args);

	if(!P.D())
	{
		temp = (signed char)A - (signed char)data - (1 - P.C());
		A = A - data - (1 - P.C());
		//If the most significant bits of A and temp differ then set overflow flag
		P.V((((A & 128) > 0) ^ ((temp & 256)!=0)) > 0);
		negOrZero(A);
	}
	else
	{
		BIT_8 msn, lsn;
		int tempV, TmpCarry = 0;
		int ln, hn, olsn, omsn;

		lsn = A & 0x0F;
		msn = A >> 4;

		temp = A - data - (1 - P.C());
		negOrZero(temp);

		omsn = data & 0xF0;
		olsn = data & 0x0F;
		if ((olsn > 9) && ((A & 0x0F) < 10))
		{
			olsn -= 10;
			omsn += 0x10;
		}

		// promote the lower nibble to the next ten, and increase the higher nibble
		ln = (A & 0x0F) - olsn - P.C();
		if (ln < 0)
		{
			if ((A & 0x0F) < 10)
			{
				ln -= 6;
			}
			ln &= 0x0F;
			TmpCarry = 0x10;
		}
		hn = (A & 0xF0) - omsn - TmpCarry;
		/* N and V flags are determined before high nibble is adjusted.
		   NOTE: V is not always correct */
		P.N((hn & 128) == 1);
		tempV = (signed char)A - (signed char)data - (1 - P.C());
		if ((tempV <- 128) || (tempV > 127))
		{
			P.V(1);
		}
		else
		{
			P.V(0);
		}
		if (hn < 0)
		{
			hn -= 0x60;
			hn &= 0xF0;
			P.C(0);
		}

		A = hn | ln;
		if (A == 0)
		{
			P.Z(1);
		}
		P.C((temp & 256) == 0);
	}

	/*overflow(old,A);
	carry(old,A);*/
}

void MPU::SEC(const BIT_8 args[], const BIT_8 opcode)
{
	P.C(1);
}

void MPU::SED(const BIT_8 args[], const BIT_8 opcode)
{
	P.D(1);
}

void MPU::SEI(const BIT_8 args[], const BIT_8 opcode)
{
	P.I(1);
}

void MPU::STA(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_16 addr, *source = &addr;

	(this->*_STA[opcode])(source, args);

	write(source, &A);
}

void MPU::STX(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_16 addr, *source = &addr;

	(this->*_STX[opcode])(source, args);

	write(source, &X);
}

void MPU::STY(const BIT_8 args[], const BIT_8 opcode)
{
	BIT_16 addr, *source = &addr;

	(this->*_STY[opcode])(source, args);

	write(source, &Y);
}

void MPU::TAX(const BIT_8 args[], const BIT_8 opcode)
{
	X = A;
	negOrZero(X);
}

void MPU::TAY(const BIT_8 args[], const BIT_8 opcode)
{
	Y = A;
	negOrZero(Y);
}

void MPU::TSX(const BIT_8 args[], const BIT_8 opcode)
{
	X = S;
	negOrZero(X);
}

void MPU::TXA(const BIT_8 args[], const BIT_8 opcode)
{
	A = X;
	negOrZero(A);
}

void MPU::TXS(const BIT_8 args[], const BIT_8 opcode)
{
	S = X;
}

void MPU::TYA(const BIT_8 args[], const BIT_8 opcode)
{
	A = Y;
	negOrZero(Y);
}

//Called if the opcode does not exist in current context
void MPU::Illegal(const BIT_8 args[], const BIT_8 opcode)
{
	
}

BIT_16 MPU::formAbsolute(BIT_8 MSB, BIT_8 LSB)
{
	// Combine two 8 bit values into 16 bits
	BIT_16 c = MSB;
	c <<= 8;
	c += LSB;
	return c;
}

void MPU::negOrZero(BIT_8 a)
{
	// Check if a is equal or less than zero
	// Set appropriate flags
	// If neither, set flags = 0
	if(a == 0)
	{
		P.Z(1);
		P.N(0);
	}
	else if(isNegative(a))
	{
		P.Z(0);
		P.N(1);
	}
	else
	{
		P.Z(0);
		P.N(0);
	}
}

void MPU::overflow(BIT_8 old, BIT_8 newd)
{
	// If bit 6 differs then an overflow occurred
	// so set the flag
	if((old & 64) != (newd & 64))
	{
		P.V(1);
	}
	else
	{
		P.V(0);
	}
}

void MPU::carry(BIT_8 old, BIT_8 newd)
{
	// If bit 7 differs then a carry occurred
	// so set the flag
	if((old & 128) != (newd & 128))
	{
		P.C(1);
	}
	else
	{
		P.C(0);
	}
}

bool MPU::isNegative(const BIT_8 &val)
{
	//Check value of MSB and return true if 1
	return ((val & 128) > 0);
}

void MPU::branchToAddr(BIT_8 addr)
{
	// Displacement is from the first instruction following branch
	PC++;
	// If the address is negative (MSB is on) subtract from
	// the ProgramCounter, else add
	if(isNegative(addr))
	{
		PC -= (unsigned char)((~addr)+1);
	}
	else
	{
		PC += (addr & 127);
	}
	branch = modifiedPC = true;
}


// The cycle of the CPU
void MPU::cycle()
{
	QueryPerformanceCounter(&li);
  
	// Get the instruction and instruction related data
	opcode = memory[PC.PC()];
	bytes = MPU::bytesToFollow[opcode];
	sCycles = MPU::cycles[opcode];
	args = new BIT_8[bytes];//(BIT_8*)malloc(sizeof(BIT_8) * bytes);

	// If there are parameters following the instruction, insert them into args
	// Use the ProgramCounter to retrieve from memory
	for(int i = 0; i < bytes; i++)
	{
		args[i] = memory[PC.PC() + (i + 1)];
	}

	// Call the instruction from the array of function pointers
	(this->*opCodes[opcode])(args, opcode);

	delete[] args;

	// Finally if not an instruction that modifies PC then
	// increment the ProgramCounter so it can get the next address
	if(!modifiedPC)
	{
		PC += bytes + 1;
	}
	else
	{
		modifiedPC = false;
	}

	if(branch)
	{
		PC += bytes;
		branch = false;
	}

	//This needs to be done straight after execution of instruction
	// can't be done during operation due to overload of operator[] for memory
	// Use else if's because only 1 can be true after each instruction, save the 2 extra checks
	//Send registers back from SHEILA to VIA
	if(memory.writeToSysVIA())
	{
		memory.updateSysVIARegisters();
	}
	//Send registers back from SHEILA to 6845
	else if(memory.writeTo6845())
	{
		memory.update6845Register();
	}
	//Send registers back from SHEILA to ULA
	else if(memory.writeToULA())
	{
		memory.updateULARegisters();
	}
	//Update the ROM latch and load in new ROM if needed
	else if(memory.writeToROMLatch())
	{
		memory.updateROM();
	}
	else if(memory.writeToSerialULA())
	{
		memory.updateSerialULARegisters();
	}
	
	//Update system VIA
	sysVIAIFR = memory.updateSysVIA(sCycles);

	//Check for interrupts
	if(!P.I())
	{
		//The only IRQ's we will get are from the system VIA
		//Only checking the IFR means that if there is an interrupt it will never be serviced as
		//doIRQ() will be called everytime so we need to check if an interrupt has recently been called.
		if((sysVIAIFR & 0x80) > 0 && !justInterrupted)
		{
			doIRQ();
			memory.updateSysVIA(7);
			justInterrupted = true;
		}
		else
		{
			//If true then the interrupt has been cleared and we can reset justInterrupted
			if((oldSysVIAIFR & 0x80) != (sysVIAIFR & 0x80))
			{
				justInterrupted = false;
			}
		}
		oldSysVIAIFR = sysVIAIFR;
	}


	totalCycles += sCycles;
	totalExecuted++;

	if(throttle)
	{
		QueryPerformanceCounter(&li2);
		if(sCycles > 0)
		{
			throttleMPU();
		}
	}
}

void MPU::throttleMPU()
{
	//How many millionths of a second did that instruction take
	diff = ((li2.QuadPart - li.QuadPart - overhead.QuadPart) * 1000000) / f.QuadPart;
	//so x is then how many millionths of a second those cycles took
	float x = diff;
	float div = sCycles / 2;
	while(x < (div))
	{
		QueryPerformanceCounter(&li2);
		x = ((li2.QuadPart - li.QuadPart - overhead.QuadPart) * 1000000) / f.QuadPart;
	}
}

void MPU::doIRQ()
{
	addToStack(PC.PCH());
	addToStack(PC.PCL());
	addToStack(P.getValue());
	PC = formAbsolute(memory[0xFFFF], memory[0xFFFE]);
	P.setValue(0x22);
}

void MPU::init()
{
	// Set bytesToFollow each instruction
	#pragma region bytes
	// ADC
	bytesToFollow[0x69] = 1;
	bytesToFollow[0x65] = 1;
	bytesToFollow[0x75] = 1;
	bytesToFollow[0x6D] = 2;
	bytesToFollow[0x7D] = 2;
	bytesToFollow[0x79] = 2;
	bytesToFollow[0x61] = 1;
	bytesToFollow[0x71] = 1;
	// AND
	bytesToFollow[0x29] = 1;
	bytesToFollow[0x25] = 1;
	bytesToFollow[0x35] = 1;
	bytesToFollow[0x2D] = 2;
	bytesToFollow[0x3D] = 2;
	bytesToFollow[0x39] = 2;
	bytesToFollow[0x21] = 1;
	bytesToFollow[0x31] = 1;
	// ASL
	bytesToFollow[0x0A] = 0;
	bytesToFollow[0x06] = 1;
	bytesToFollow[0x16] = 1;
	bytesToFollow[0x0E] = 2;
	bytesToFollow[0x1E] = 2;
	// BCC
	bytesToFollow[0x90] = 1;
	// BCS
	bytesToFollow[0xB0] = 1;
	// BEQ
	bytesToFollow[0xF0] = 1;
	// BIT
	bytesToFollow[0x24] = 1;
	bytesToFollow[0x2C] = 2;
	// BMI
	bytesToFollow[0x30] = 1;
	// BNE
	bytesToFollow[0xD0] = 1;
	// BPL
	bytesToFollow[0x10] = 1;
	// BRK
	bytesToFollow[0x00] = 0;
	// BVC
	bytesToFollow[0x50] = 1;
	// BVS
	bytesToFollow[0x70] = 1;
	// CLC
	bytesToFollow[0x18] = 0;
	// CLD
	bytesToFollow[0xD8] = 0;
	// CLI
	bytesToFollow[0x58] = 0;
	// CLV
	bytesToFollow[0xB8] = 0;
	// CMP
	bytesToFollow[0xC9] = 1;
	bytesToFollow[0xC5] = 1;
	bytesToFollow[0xD5] = 1;
	bytesToFollow[0xCD] = 2;
	bytesToFollow[0xDD] = 2;
	bytesToFollow[0xD9] = 2;
	bytesToFollow[0xC1] = 1;
	bytesToFollow[0xD1] = 1;
	// CPX
	bytesToFollow[0xE0] = 1;
	bytesToFollow[0xE4] = 1;
	bytesToFollow[0xEC] = 2;
	// CPY
	bytesToFollow[0xC0] = 1;
	bytesToFollow[0xC4] = 1;
	bytesToFollow[0xCC] = 2;
	// DEC
	bytesToFollow[0xC6] = 1;
	bytesToFollow[0xD6] = 1;
	bytesToFollow[0xCE] = 2;
	bytesToFollow[0xDE] = 2;
	// DEX
	bytesToFollow[0xCA] = 0;
	// DEY
	bytesToFollow[0x88] = 0;
	// EOR
	bytesToFollow[0x49] = 1;
	bytesToFollow[0x45] = 1;
	bytesToFollow[0x55] = 1;
	bytesToFollow[0x4D] = 2;
	bytesToFollow[0x5D] = 2;
	bytesToFollow[0x59] = 2;
	bytesToFollow[0x41] = 1;
	bytesToFollow[0x11] = 1;
	// INC
	bytesToFollow[0xE6] = 1;
	bytesToFollow[0xF6] = 1;
	bytesToFollow[0xEE] = 2;
	bytesToFollow[0xFE] = 2;
	// INX
	bytesToFollow[0xC8] = 0;
	// INY
	bytesToFollow[0xE8] = 0;
	// JMP
	bytesToFollow[0x4C] = 2;
	bytesToFollow[0x6C] = 2;
	// JSR
	bytesToFollow[0x20] = 2;
	// LDA
	bytesToFollow[0xA9] = 1;
	bytesToFollow[0xA5] = 1;
	bytesToFollow[0xB5] = 1;
	bytesToFollow[0xAD] = 2;
	bytesToFollow[0xBD] = 2;
	bytesToFollow[0xB9] = 2;
	bytesToFollow[0xA1] = 1;
	bytesToFollow[0xB1] = 1;
	// LDX
	bytesToFollow[0xAE] = 2;
	bytesToFollow[0xA6] = 1;
	bytesToFollow[0xA2] = 1;
	bytesToFollow[0xBE] = 2;
	bytesToFollow[0xB6] = 1;
	// LDY
	bytesToFollow[0xAC] = 2;
	bytesToFollow[0xA4] = 1;
	bytesToFollow[0xA0] = 1;
	bytesToFollow[0xBC] = 2;
	bytesToFollow[0xB4] = 3;
	// LSR
	bytesToFollow[0x4A] = 0;
	bytesToFollow[0x46] = 1;
	bytesToFollow[0x56] = 1;
	bytesToFollow[0x4E] = 2;
	bytesToFollow[0x5E] = 2;
	// NOP
	bytesToFollow[0xEA] = 0;
	// ORA
	bytesToFollow[0x09] = 1;
	bytesToFollow[0x05] = 1;
	bytesToFollow[0x15] = 1;
	bytesToFollow[0x0D] = 2;
	bytesToFollow[0x1D] = 2;
	bytesToFollow[0x19] = 2;
	bytesToFollow[0x01] = 1;
	bytesToFollow[0x11] = 1;
	// PHA
	bytesToFollow[0x48] = 0;
	// PHP
	bytesToFollow[0x08] = 0;
	// PLA
	bytesToFollow[0x68] = 0;
	// PLP
	bytesToFollow[0x28] = 0;
	// ROL
	bytesToFollow[0x2A] = 0;
	bytesToFollow[0x26] = 1;
	bytesToFollow[0x36] = 1;
	bytesToFollow[0x2E] = 2;
	bytesToFollow[0x3E] = 2;
	// ROR
	bytesToFollow[0x6A] = 0;
	bytesToFollow[0x66] = 1;
	bytesToFollow[0x76] = 1;
	bytesToFollow[0x6E] = 2;
	bytesToFollow[0x7E] = 2;
	// RTI
	bytesToFollow[0x40] = 0;
	// RTS
	bytesToFollow[0x60] = 0;
	// SBC
	bytesToFollow[0xE9] = 1;
	bytesToFollow[0xE5] = 1;
	bytesToFollow[0xF5] = 1;
	bytesToFollow[0xED] = 2;
	bytesToFollow[0xFD] = 2;
	bytesToFollow[0xF9] = 2;
	bytesToFollow[0xE1] = 1;
	bytesToFollow[0xF1] = 1;
	// SEC
	bytesToFollow[0x38] = 0;
	// SED
	bytesToFollow[0xF8] = 0;
	// SEI
	bytesToFollow[0x78] = 0;
	// STA
	bytesToFollow[0x85] = 1;
	bytesToFollow[0x95] = 1;
	bytesToFollow[0x8D] = 2;
	bytesToFollow[0x9D] = 2;
	bytesToFollow[0x99] = 2;
	bytesToFollow[0x81] = 1;
	bytesToFollow[0x91] = 1;
	// STX
	bytesToFollow[0x86] = 1;
	bytesToFollow[0x96] = 1;
	bytesToFollow[0x8E] = 2;
	// STY
	bytesToFollow[0x84] = 1;
	bytesToFollow[0x94] = 1;
	bytesToFollow[0x8C] = 2;
	// TAX
	bytesToFollow[0xAA] = 0;
	// TAY
	bytesToFollow[0xA8] = 0;
	// TSX
	bytesToFollow[0xBA] = 0;
	// TXA
	bytesToFollow[0x8A] = 0;
	// TXS
	bytesToFollow[0x9A] = 0;
	// TYA
	bytesToFollow[0x98] = 0;
	#pragma endregion
	// Set cycles for each instruction
	#pragma region cycles
	// ADC
	cycles[0x69] = 2;
	cycles[0x65] = 3;
	cycles[0x75] = 4;
	cycles[0x6D] = 4;
	cycles[0x7D] = 4;
	cycles[0x79] = 4;
	cycles[0x61] = 6;
	cycles[0x71] = 5;
	// AND
	cycles[0x29] = 2;
	cycles[0x25] = 3;
	cycles[0x35] = 4;
	cycles[0x2D] = 4;
	cycles[0x3D] = 4;
	cycles[0x39] = 4;
	cycles[0x21] = 6;
	cycles[0x31] = 5;
	// ASL
	cycles[0x0A] = 2;
	cycles[0x06] = 5;
	cycles[0x16] = 6;
	cycles[0x0E] = 6;
	cycles[0x1E] = 7;
	// BCC
	cycles[0x90] = 2;
	// BCS
	cycles[0xB0] = 2;
	// BEQ
	cycles[0xF0] = 2;
	// BIT
	cycles[0x24] = 2;
	cycles[0x2C] = 3;
	//BMI
	cycles[0x30] = 2;
	// BNE
	cycles[0xD0] = 2;
	// BPL
	cycles[0x10] = 2;
	// BRK
	cycles[0x00] = 7;
	// BVC
	cycles[0x50] = 2;
	// BVS
	cycles[0x70] = 2;
	// CLC
	cycles[0x18] = 2;
	// CLD
	cycles[0xD8] = 2;
	// CLI
	cycles[0x58] = 2;
	// CLV
	cycles[0xB8] = 2;
	// CMP
	cycles[0xC9] = 2;
	cycles[0xC5] = 3;
	cycles[0xD5] = 4;
	cycles[0xCD] = 4;
	cycles[0xDD] = 4;
	cycles[0xD9] = 4;
	cycles[0xC1] = 6;
	cycles[0xD1] = 5;
	// CPX
	cycles[0xE0] = 2;
	cycles[0xE4] = 3;
	cycles[0xEC] = 4;
	// CPY
	cycles[0xC0] = 2;
	cycles[0xC4] = 3;
	cycles[0xCC] = 4;
	// DEC
	cycles[0xC6] = 5;
	cycles[0xD6] = 6;
	cycles[0xCE] = 3;
	cycles[0xDE] = 7;
	// DEX
	cycles[0xCA] = 2;
	// DEY
	cycles[0x88] = 2;
	// EOR
	cycles[0x49] = 2;
	cycles[0x45] = 3;
	cycles[0x55] = 4;
	cycles[0x4D] = 4;
	cycles[0x5D] = 4;
	cycles[0x59] = 4;
	cycles[0x41] = 6;
	cycles[0x11] = 5;
	// INC
	cycles[0xE6] = 5;
	cycles[0xF6] = 6;
	cycles[0xEE] = 6;
	cycles[0xFE] = 7;
	// INX
	cycles[0xC8] = 2;
	// INY
	cycles[0xE8] = 2;
	// JMP
	cycles[0x4C] = 3;
	cycles[0x6C] = 5;
	// JSR
	cycles[0x20] = 6;
	// LDA
	cycles[0xA9] = 2;
	cycles[0xA5] = 3;
	cycles[0xB5] = 4;
	cycles[0xAD] = 4;
	cycles[0xBD] = 4;
	cycles[0xB9] = 4;
	cycles[0xA1] = 6;
	cycles[0xB1] = 5;
	// LDX
	cycles[0xA0] = 2;
	cycles[0xA4] = 3;
	cycles[0xB4] = 4;
	cycles[0xAC] = 4;
	cycles[0xBC] = 4;
	// LSR
	cycles[0x4A] = 2;
	cycles[0x46] = 5;
	cycles[0x56] = 6;
	cycles[0x4E] = 6;
	cycles[0x5E] = 7;
	// NOP
	cycles[0xEA] = 2;
	// ORA
	cycles[0x09] = 2;
	cycles[0x05] = 3;
	cycles[0x15] = 4;
	cycles[0x0D] = 4;
	cycles[0x1D] = 4;
	cycles[0x19] = 4;
	cycles[0x01] = 6;
	cycles[0x11] = 5;
	// PHA
	cycles[0x48] = 3;
	// PHP
	cycles[0x08] = 3;
	// PLA
	cycles[0x68] = 4;
	// PLP
	cycles[0x28] = 4;
	// ROL
	cycles[0x2A] = 2;
	cycles[0x26] = 5;
	cycles[0x36] = 6;
	cycles[0x2E] = 6;
	cycles[0x3E] = 7;
	// ROR
	cycles[0x6A] = 2;
	cycles[0x66] = 5;
	cycles[0x76] = 6;
	cycles[0x6E] = 6;
	cycles[0x7E] = 7;
	// RTI
	cycles[0x40] = 6;
	// RTS
	cycles[0x60] = 6;
	// SBC
	cycles[0xE9] = 2;
	cycles[0xE5] = 3;
	cycles[0xF5] = 4;
	cycles[0xED] = 4;
	cycles[0xFD] = 4;
	cycles[0xF9] = 4;
	cycles[0xE1] = 6;
	cycles[0xF1] = 5;
	// SEC
	cycles[0x38] = 2;
	// SED
	cycles[0xF8] = 2;
	// SEI
	cycles[0x78] = 2;
	// STA
	cycles[0x85] = 3;
	cycles[0x95] = 4;
	cycles[0x8D] = 4;
	cycles[0x9D] = 5;
	cycles[0x99] = 5;
	cycles[0x81] = 6;
	cycles[0x91] = 6;
	// STX
	cycles[0x86] = 3;
	cycles[0x96] = 4;
	cycles[0x8E] = 4;
	// STY
	cycles[0x84] = 3;
	cycles[0x94] = 4;
	cycles[0x8C] = 4;
	// TAX
	cycles[0xAA] = 2;
	// TAY
	cycles[0xA8] = 2;
	// TSX
	cycles[0xBA] = 2;
	// TXA
	cycles[0x8A] = 2;
	// TXS
	cycles[0x9A] = 2;
	// TYA
	cycles[0x98] = 2;
	#pragma endregion
	// Set function pointers
	#pragma region opCodes
	for(int i = 0; i < 0x100; i++)
	{
		opCodes[i] = &MPU::Illegal;
	}
	// Initialise op code list
	opCodes[0x00] = &MPU::BRK;
	opCodes[0x01] = &MPU::ORA;
	opCodes[0x05] = &MPU::ORA;
	opCodes[0x06] = &MPU::ASL;
	opCodes[0x08] = &MPU::PHP;
	opCodes[0x09] = &MPU::ORA;
	opCodes[0x0A] = &MPU::ASL;
	opCodes[0x0D] = &MPU::ORA;
	opCodes[0x0E] = &MPU::ASL;

	opCodes[0x10] = &MPU::BPL;
	opCodes[0x11] = &MPU::ORA;
	opCodes[0x15] = &MPU::ORA;
	opCodes[0x16] = &MPU::ASL;
	opCodes[0x18] = &MPU::CLC;
	opCodes[0x19] = &MPU::ORA;
	opCodes[0x1D] = &MPU::ORA;
	opCodes[0x1E] = &MPU::ASL;

	opCodes[0x20] = &MPU::JSR;
	opCodes[0x21] = &MPU::AND;
	opCodes[0x24] = &MPU::BIT;
	opCodes[0x25] = &MPU::AND;
	opCodes[0x26] = &MPU::ROL;
	opCodes[0x28] = &MPU::PLP;
	opCodes[0x29] = &MPU::AND;
	opCodes[0x2A] = &MPU::ROL;
	opCodes[0x2C] = &MPU::BIT;
	opCodes[0x2D] = &MPU::AND;
	opCodes[0x2E] = &MPU::ROL;

	opCodes[0x30] = &MPU::BMI;
	opCodes[0x31] = &MPU::AND;
	opCodes[0x35] = &MPU::AND;
	opCodes[0x36] = &MPU::ROL;
	opCodes[0x38] = &MPU::SEC;
	opCodes[0x39] = &MPU::AND;
	opCodes[0x3D] = &MPU::AND;
	opCodes[0x3E] = &MPU::ROL;

	opCodes[0x40] = &MPU::RTI;
	opCodes[0x41] = &MPU::EOR;
	opCodes[0x45] = &MPU::EOR;
	opCodes[0x46] = &MPU::LSR;
	opCodes[0x48] = &MPU::PHA;
	opCodes[0x49] = &MPU::EOR;
	opCodes[0x4A] = &MPU::LSR;
	opCodes[0x4C] = &MPU::JMP;
	opCodes[0x4D] = &MPU::EOR;
	opCodes[0x4E] = &MPU::LSR;

	opCodes[0x50] = &MPU::BVC;
	opCodes[0x51] = &MPU::EOR;
	opCodes[0x55] = &MPU::EOR;
	opCodes[0x56] = &MPU::LSR;
	opCodes[0x58] = &MPU::CLI;
	opCodes[0x59] = &MPU::EOR;
	opCodes[0x5D] = &MPU::EOR;
	opCodes[0x5E] = &MPU::LSR;

	opCodes[0x60] = &MPU::RTS;
	opCodes[0x61] = &MPU::ADC;
	opCodes[0x65] = &MPU::ADC;
	opCodes[0x66] = &MPU::ROR;
	opCodes[0x68] = &MPU::PLA;
	opCodes[0x69] = &MPU::ADC;
	opCodes[0x6A] = &MPU::ROR;
	opCodes[0x6C] = &MPU::JMP;
	opCodes[0x6D] = &MPU::ADC;
	opCodes[0x6E] = &MPU::ROR;

	opCodes[0x70] = &MPU::BVS;
	opCodes[0x71] = &MPU::ADC;
	opCodes[0x75] = &MPU::ADC;
	opCodes[0x76] = &MPU::ROR;
	opCodes[0x78] = &MPU::SEI;
	opCodes[0x79] = &MPU::ADC;
	opCodes[0x7D] = &MPU::ADC;
	opCodes[0x7E] = &MPU::ROR;

	opCodes[0x81] = &MPU::STA;
	opCodes[0x84] = &MPU::STY;
	opCodes[0x85] = &MPU::STA;
	opCodes[0x86] = &MPU::STX;
	opCodes[0x88] = &MPU::DEY;
	opCodes[0x8A] = &MPU::TXA;
	opCodes[0x8C] = &MPU::STY;
	opCodes[0x8D] = &MPU::STA;
	opCodes[0x8E] = &MPU::STX;

	opCodes[0x90] = &MPU::BCC;
	opCodes[0x91] = &MPU::STA;
	opCodes[0x94] = &MPU::STY;
	opCodes[0x95] = &MPU::STA;
	opCodes[0x96] = &MPU::STX;
	opCodes[0x98] = &MPU::TYA;
	opCodes[0x99] = &MPU::STA;
	opCodes[0x9A] = &MPU::TXS;
	opCodes[0x9D] = &MPU::STA;

	opCodes[0xA0] = &MPU::LDY;
	opCodes[0xA1] = &MPU::LDA;
	opCodes[0xA2] = &MPU::LDX;
	opCodes[0xA4] = &MPU::LDY;
	opCodes[0xA5] = &MPU::LDA;
	opCodes[0xA6] = &MPU::LDX;
	opCodes[0xA8] = &MPU::TAY;
	opCodes[0xA9] = &MPU::LDA;
	opCodes[0xAA] = &MPU::TAX;
	opCodes[0xAC] = &MPU::LDY;
	opCodes[0xAD] = &MPU::LDA;
	opCodes[0xAE] = &MPU::LDX;

	opCodes[0xB0] = &MPU::BCS;
	opCodes[0xB1] = &MPU::LDA;
	opCodes[0xB4] = &MPU::LDY;
	opCodes[0xB5] = &MPU::LDA;
	opCodes[0xB6] = &MPU::LDX;
	opCodes[0xB8] = &MPU::CLV;
	opCodes[0xB9] = &MPU::LDA;
	opCodes[0xBA] = &MPU::TSX;
	opCodes[0xBC] = &MPU::LDY;
	opCodes[0xBD] = &MPU::LDA;
	opCodes[0xBE] = &MPU::LDX;

	opCodes[0xC0] = &MPU::CPY;
	opCodes[0xC1] = &MPU::CMP;
	opCodes[0xC4] = &MPU::CPY;
	opCodes[0xC5] = &MPU::CMP;
	opCodes[0xC6] = &MPU::DEC;
	opCodes[0xC8] = &MPU::INY;
	opCodes[0xC9] = &MPU::CMP;
	opCodes[0xCA] = &MPU::DEX;
	opCodes[0xCC] = &MPU::CPY;
	opCodes[0xCD] = &MPU::CMP;
	opCodes[0xCE] = &MPU::DEC;

	opCodes[0xD0] = &MPU::BNE;
	opCodes[0xD1] = &MPU::CMP;
	opCodes[0xD5] = &MPU::CMP;
	opCodes[0xD6] = &MPU::DEC;
	opCodes[0xD8] = &MPU::CLD;
	opCodes[0xD9] = &MPU::CMP;
	opCodes[0xDD] = &MPU::CMP;
	opCodes[0xDE] = &MPU::DEC;

	opCodes[0xE0] = &MPU::CPX;
	opCodes[0xE1] = &MPU::SBC;
	opCodes[0xE4] = &MPU::CPX;
	opCodes[0xE5] = &MPU::SBC;
	opCodes[0xE6] = &MPU::INC;
	opCodes[0xE8] = &MPU::INX;
	opCodes[0xE9] = &MPU::SBC;
	opCodes[0xEA] = &MPU::NOP;
	opCodes[0xEC] = &MPU::CPX;
	opCodes[0xED] = &MPU::SBC;
	opCodes[0xEE] = &MPU::INC;

	opCodes[0xF0] = &MPU::BEQ;
	opCodes[0xF1] = &MPU::SBC;
	opCodes[0xF5] = &MPU::SBC;
	opCodes[0xF6] = &MPU::INC;
	opCodes[0xF8] = &MPU::SED;
	opCodes[0xF9] = &MPU::SBC;
	opCodes[0xFD] = &MPU::SBC;
	opCodes[0xFE] = &MPU::INC;
	#pragma endregion
	// Set addressing types
	#pragma region Addressing types
	_ADC[0x61] = &MPU::IndirectX;
	_ADC[0x65] = &MPU::ZeroPage;
	_ADC[0x69] = &MPU::Immediate;
	_ADC[0x6D] = &MPU::Absolute;
	_ADC[0x71] = &MPU::IndirectY;
	_ADC[0x75] = &MPU::ZeroPageX;
	_ADC[0x79] = &MPU::AbsoluteY;
	_ADC[0x7D] = &MPU::AbsoluteX;

	_AND[0x21] = &MPU::IndirectX;
	_AND[0x25] = &MPU::ZeroPage;
	_AND[0x29] = &MPU::Immediate;
	_AND[0x2D] = &MPU::Absolute;
	_AND[0x31] = &MPU::IndirectY;
	_AND[0x35] = &MPU::ZeroPageX;
	_AND[0x39] = &MPU::AbsoluteY;
	_AND[0x3D] = &MPU::AbsoluteX;

	_ASL[0x06] = &MPU::ZeroPage_Source;
	_ASL[0x0A] = &MPU::Accumulator_Source;
	_ASL[0x0E] = &MPU::Absolute_Source;
	_ASL[0x16] = &MPU::ZeroPageX_Source;
	_ASL[0x1E] = &MPU::AbsoluteX_Source;

	_BIT[0x24] = &MPU::ZeroPage;
	_BIT[0x2C] = &MPU::Absolute;

	_CMP[0xCD] = &MPU::Absolute;
	_CMP[0xC5] = &MPU::ZeroPage;
	_CMP[0xC9] = &MPU::Immediate;
	_CMP[0xDD] = &MPU::AbsoluteX;
	_CMP[0xD9] = &MPU::AbsoluteY;
	_CMP[0xC1] = &MPU::IndirectX;
	_CMP[0xD1] = &MPU::IndirectY;
	_CMP[0xD5] = &MPU::ZeroPageX;

	_CPX[0xEC] = &MPU::Absolute;
	_CPX[0xE4] = &MPU::ZeroPage;
	_CPX[0xE0] = &MPU::Immediate;

	_CPY[0xCC] = &MPU::Absolute;
	_CPY[0xC4] = &MPU::ZeroPage;
	_CPY[0xC0] = &MPU::Immediate;

	_DEC[0xCE] = &MPU::Absolute_Source;
	_DEC[0xC6] = &MPU::ZeroPage_Source;
	_DEC[0xDE] = &MPU::AbsoluteX_Source;
	_DEC[0xD6] = &MPU::ZeroPageX_Source;

	_EOR[0x4D] = &MPU::Absolute;
	_EOR[0x45] = &MPU::ZeroPage;
	_EOR[0x49] = &MPU::Immediate;
	_EOR[0x5D] = &MPU::AbsoluteX;
	_EOR[0x59] = &MPU::AbsoluteY;
	_EOR[0x41] = &MPU::IndirectX;
	_EOR[0x51] = &MPU::IndirectY;
	_EOR[0x55] = &MPU::ZeroPageX;

	_INC[0xEE] = &MPU::Absolute_Source;
	_INC[0xE6] = &MPU::ZeroPage_Source;
	_INC[0xFE] = &MPU::AbsoluteX_Source;
	_INC[0xF6] = &MPU::ZeroPageX_Source;

	_JMP[0x4C] = &MPU::AbsoluteJumps;
	_JMP[0x6C] = &MPU::Indirect;

	_LDA[0xAD] = &MPU::Absolute;
	_LDA[0xA5] = &MPU::ZeroPage;
	_LDA[0xA9] = &MPU::Immediate;
	_LDA[0xBD] = &MPU::AbsoluteX;
	_LDA[0xB9] = &MPU::AbsoluteY;
	_LDA[0xA1] = &MPU::IndirectX;
	_LDA[0xB1] = &MPU::IndirectY;
	_LDA[0xB5] = &MPU::ZeroPageX;

	_LDX[0xAE] = &MPU::Absolute;
	_LDX[0xA6] = &MPU::ZeroPage;
	_LDX[0xBE] = &MPU::AbsoluteY;
	_LDX[0xB6] = &MPU::ZeroPageY;
	_LDX[0xA2] = &MPU::Immediate;

	_LDY[0xAC] = &MPU::Absolute;
	_LDY[0xA4] = &MPU::ZeroPage;
	_LDY[0xA0] = &MPU::Immediate;
	_LDY[0xBC] = &MPU::AbsoluteX;
	_LDY[0xB4] = &MPU::ZeroPageX;

	_LSR[0x4A] = &MPU::Accumulator_Source;
	_LSR[0x4E] = &MPU::Absolute_Source;
	_LSR[0x46] = &MPU::ZeroPage_Source;
	_LSR[0x5E] = &MPU::AbsoluteX_Source;
	_LSR[0x56] = &MPU::ZeroPageX_Source;

	_ORA[0x0D] = &MPU::Absolute;
	_ORA[0x05] = &MPU::ZeroPage;
	_ORA[0x09] = &MPU::Immediate;
	_ORA[0x1D] = &MPU::AbsoluteX;
	_ORA[0x19] = &MPU::AbsoluteY;
	_ORA[0x01] = &MPU::IndirectX;
	_ORA[0x11] = &MPU::IndirectY;
	_ORA[0x15] = &MPU::ZeroPage;

	_ROL[0x2A] = &MPU::Accumulator_Source;
	_ROL[0x26] = &MPU::ZeroPage_Source;
	_ROL[0x36] = &MPU::ZeroPageX_Source;
	_ROL[0x2E] = &MPU::Absolute_Source;
	_ROL[0x3E] = &MPU::AbsoluteX_Source;

	_ROR[0x6A] = &MPU::Accumulator_Source;
	_ROR[0x66] = &MPU::ZeroPage_Source;
	_ROR[0x76] = &MPU::ZeroPageX_Source;
	_ROR[0x6E] = &MPU::Absolute_Source;
	_ROR[0x7E] = &MPU::AbsoluteX_Source;

	_SBC[0xED] = &MPU::Absolute;
	_SBC[0xE5] = &MPU::ZeroPage;
	_SBC[0xE9] = &MPU::Immediate;
	_SBC[0xFD] = &MPU::AbsoluteX;
	_SBC[0xF9] = &MPU::AbsoluteY;
	_SBC[0xE1] = &MPU::IndirectX;
	_SBC[0xF1] = &MPU::IndirectY;
	_SBC[0xF5] = &MPU::ZeroPageX;

	_STA[0x8D] = &MPU::Absolute_SourceOnly;
	_STA[0x85] = &MPU::ZeroPage_SourceOnly;
	_STA[0x9D] = &MPU::AbsoluteX_SourceOnly;
	_STA[0x99] = &MPU::AbsoluteY_SourceOnly;
	_STA[0x81] = &MPU::IndirectX_SourceOnly;
	_STA[0x91] = &MPU::IndirectY_SourceOnly;
	_STA[0x95] = &MPU::ZeroPageX_SourceOnly;

	_STX[0x8E] = &MPU::Absolute_SourceOnly;
	_STX[0x86] = &MPU::ZeroPage_SourceOnly;
	_STX[0x96] = &MPU::ZeroPageY_SourceOnly;

	_STY[0x8C] = &MPU::Absolute_SourceOnly;
	_STY[0x84] = &MPU::ZeroPage_SourceOnly;
	_STY[0x94] = &MPU::ZeroPageX_SourceOnly;
	#pragma endregion

}
