#include "RS423Serial.h"


RS423Serial::RS423Serial()
{
}


RS423Serial::~RS423Serial()
{
}

BIT_8 RS423Serial::readSerial(short int reg)
{
	BIT_8 ret = 0xFF;

	if(reg == 0x08)
	{
		ret = Status;
	}
	else if(reg == 0x09)
	{
		ret = RDR;
	}
	else if(reg == 0x10)
	{
		ret = SULAControl;
	}

	return ret;
}

void RS423Serial::writeSerial(short int reg, BIT_8 value)
{
	//Strip out the 0x40 value to leave a range of 0x0 - 0xF
	reg &= 0xF;

	BIT_8 ret = 0xFF;

	if(reg == 0x08)
	{
		ControlR = value;
		Status &= 8;
		Status &= ~(1 << 7);
		Status |= 1 << 2;
	}
	else if(reg == 0x09)
	{
		TDR = value;
	}
	else if(reg == 0x10)
	{
		//Would set buad rates

		SULAControl = value;
	}
}