#include <bitset>
#include <vector>
#include <Windows.h>

#include "Types.h"
#include "RamRom.h"

#ifndef _MPU__
#define _MPU__

/*Class to look after the program counter, high and low bytes included*/
class ProgramCounter
{
private:
	BIT_8 _PCH;
	BIT_8 _PCL;
	BIT_16 _PC;
	void setHighLow();

public:

	ProgramCounter();
	ProgramCounter(const ProgramCounter& rhs);
	BIT_16 PC();
	BIT_8 PCH();
	BIT_8 PCL();
	void PCH(BIT_8 a);
	void PCH(int a);
	void PCL(BIT_8 a);
	void PCL(int a);

	#pragma region Operator Overloads
	ProgramCounter& operator=(const ProgramCounter& rhs);
	ProgramCounter& operator=(const BIT_16& rhs);
	ProgramCounter& operator++();
	ProgramCounter& operator++(int);
	ProgramCounter& operator--();
	ProgramCounter& operator--(int);
	ProgramCounter operator+(const int& rhs);
	ProgramCounter operator-(const int& rhs);
	ProgramCounter& operator+=(const int& rhs);
	ProgramCounter& operator-=(const int& rhs);
	#pragma endregion
};

/*Class to store state flags*/
class Flags
{
private:
	//List of 8 bits
	std::bitset<8> _p;

public:

	Flags();
	//Get functions
	bool N();
	bool V();
	bool B();
	bool D();
	bool I();
	bool Z();
	bool C();

	//Set functions
	void N(bool on);
	void V(bool on);
	void B(bool on);
	void D(bool on);
	void I(bool on);
	void Z(bool on);
	void C(bool on);

	//Get overall value
	BIT_8 getValue();
	//Set overall value
	void setValue(int val);
};

/**************************************
*MPU class							  *
*Contains software implementation of  *
*6502 MPU hardware					  *
**************************************/
class MPU
{
private:
	//Status Register
	Flags P;
	
	ProgramCounter PC;

	//8 bit registers: X, Y, Accumulator, Stack Pointer	
	BIT_8	X,
			Y, 
			A,
			S;

	int totalCycles;
	int totalExecuted;

	// The number of bytes that follow each instruction
	// Opcode is used as the index
	static BIT_8 bytesToFollow[0xFF];
	
	// The number of cycles each instruction takes
	// Opcode is used as the index
	static BIT_8 cycles[0xFF];

	BIT_8 opcode;
	BIT_8 bytes;
	short sCycles;
	BIT_8 *args;

	bool branch;
	bool modifiedPC;

	// Set to true of memory access crosses page boundary
	// Used to add 1 to number of cycles
	bool crossedPage;

	//Write to accumulator not memory
	bool writeA;

	//Interrupt flag register from the system VIA
	BIT_8 sysVIAIFR;
	BIT_8 oldSysVIAIFR;

	//Has there just been an interrupt
	bool justInterrupted;

	// Array of function pointers for each instruction
	// Index is the value of the op code
	void (MPU::*opCodes[0xFF])(const BIT_8 args[], const BIT_8 opcode);
	/*************************************************************************
	* Array of function pointers for different addressing types
	* Index is the again the op code so array will have to be the size of
	* the largest op code even if there are only 2 addressing types
	* 1 Array for each function that has more than 1 addressing type 
	*************************************************************************/
	BIT_8 (MPU::*_ADC[0x7E])(const BIT_8 args[]); // ADC
	BIT_8 (MPU::*_AND[0x3E])(const BIT_8 args[]); // AND
	BIT_8 (MPU::*_ASL[0x1F])(BIT_16 *sourceAddr, const BIT_8 args[]); // ASL
	BIT_8 (MPU::*_BIT[0x2D])(const BIT_8 args[]); // BIT
	BIT_8 (MPU::*_CMP[0xDE])(const BIT_8 args[]); // CMP
	BIT_8 (MPU::*_CPX[0xED])(const BIT_8 args[]); // CPX
	BIT_8 (MPU::*_CPY[0xCD])(const BIT_8 args[]); // CPY
	BIT_8 (MPU::*_DEC[0xDF])(BIT_16 *sourceAddr, const BIT_8 args[]); // DEC
	BIT_8 (MPU::*_EOR[0x5E])(const BIT_8 args[]); // EOR
	BIT_8 (MPU::*_INC[0xFF])(BIT_16 *sourceAddr, const BIT_8 args[]); // INC
	BIT_16 (MPU::*_JMP[0x6D])(const BIT_8 args[]); // JMP
	BIT_8 (MPU::*_LDA[0xBE])(const BIT_8 args[]); // LDA
	BIT_8 (MPU::*_LDX[0xBF])(const BIT_8 args[]); // LDX
	BIT_8 (MPU::*_LDY[0xBD])(const BIT_8 args[]); // LDY
	BIT_8 (MPU::*_LSR[0x5F])(BIT_16 *sourceAddr, const BIT_8 args[]); // LSR
	BIT_8 (MPU::*_ORA[0x1E])(const BIT_8 args[]); // ORA
	BIT_8 (MPU::*_ROL[0x3F])(BIT_16 *sourceAddr, const BIT_8 args[]); // ROL
	BIT_8 (MPU::*_ROR[0x7F])(BIT_16 *sourceAddr, const BIT_8 args[]); // ROR
	BIT_8 (MPU::*_SBC[0xFE])(const BIT_8 args[]); // SBC
	void (MPU::*_STA[0x9E])(BIT_16 *sourceAddr, const BIT_8 args[]); // STA
	void (MPU::*_STX[0x97])(BIT_16 *sourceAddr, const BIT_8 args[]); // STX
	void (MPU::*_STY[0x95])(BIT_16 *sourceAddr, const BIT_8 args[]); // STY

	//Initialise all arrays
	void init();

	//Handle IRQ interrupts
	void doIRQ();

	void throttleMPU();
	//Used in cycle for timings
	LARGE_INTEGER li, li2, f, overhead;
	bool throttle;
	float sleep;
	long long diff;

public:

	MPU();	
	~MPU();
	
	void setThrottle(bool b);

	//Perform 1 CPU cycle
	void cycle();

	//Zero all registers
	void resetRegisters();

	//Put the input memory location into the program counter
	void loadProgram(BIT_16 iMemLoc);

	/* Get the stackpointer, can't just return 8 bit register
	 * pointer is a 9 bit register */
	BIT_16 stackPointer();

	/*Stick data on the stack and move the stack pointer*/
	void addToStack(BIT_8 data);

	/*Remove data from stack and move stack pointer*/
	BIT_8 popFromStack();
	
	// The number of bytes that follow each instruction
	// Opcode is used as the index
	BIT_8 getBytesToFollow(int index);
	
	// The number of cyclces each instruction takes
	// Opcode is used as the index
	BIT_8 getCycles(int index);

	void setTotalCycles(int x);
	int getTotalCycles();

	void setTotalExecuted(int x);
	int getTotalExecuted();

	int getStatusRegisterValue();
	int getProgramCounterValue();

	int getX();
	int getY();
	int getA();
	int getS();

	RAM_ROM memory;

	#pragma region Memory addressing functions
	/******************************************
	* Memory addressing functions
	*
	* args is the array passed to the instruction
	*
	* **pSource will return the memory location
	******************************************/
	BIT_16 AbsoluteJumps(const BIT_8 args[]);
	BIT_8 Absolute(const BIT_8 args[]);
	BIT_8 Absolute_Source(BIT_16 *sourceAddr, const BIT_8 args[]);
	BIT_8 AbsoluteX(const BIT_8 args[]);
	BIT_8 AbsoluteX_Source(BIT_16 *sourceAddr, const BIT_8 args[]);
	BIT_8 AbsoluteY(const BIT_8 args[]);
	BIT_8 AbsoluteY_Source(BIT_16 *sourceAddr, const BIT_8 args[]);
	BIT_8 ZeroPage(const BIT_8 args[]);
	BIT_8 ZeroPage_Source(BIT_16 *sourceAddr, const BIT_8 args[]);
	BIT_8 ZeroPageX(const BIT_8 args[]);
	BIT_8 ZeroPageX_Source(BIT_16 *sourceAddr, const BIT_8 args[]);
	BIT_8 ZeroPageY(const BIT_8 args[]);
	BIT_8 ZeroPageY_Source(BIT_16 *sourceAddr, const BIT_8 args[]);
	BIT_16 Indirect(const BIT_8 args[]);
	BIT_8 IndirectX(const BIT_8 args[]);
	BIT_8 IndirectX_Source(BIT_16 *sourceAddr, const BIT_8 args[]);
	BIT_8 IndirectY(const BIT_8 args[]);
	BIT_8 IndirectY_Source(BIT_16 *sourceAddr, const BIT_8 args[]);
	BIT_8 Immediate(const BIT_8 args[]);
	BIT_8 Accumulator(const BIT_8 args[]);
	BIT_8 Accumulator_Source(BIT_16 *sourceAddr, const BIT_8 args[]);

	void Absolute_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[]);
	void ZeroPage_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[]);
	void AbsoluteX_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[]);
	void AbsoluteY_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[]);
	void IndirectX_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[]);
	void IndirectY_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[]);
	void ZeroPageX_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[]);
	void ZeroPageY_SourceOnly(BIT_16 *sourceAddr, const BIT_8 args[]);

	__forceinline void write(BIT_16 *src, BIT_8 *d);

	#pragma endregion

	BIT_16 formAbsolute(BIT_8 MSB, BIT_8 LSB);
	void negOrZero(BIT_8 a);
	void overflow(BIT_8 old, BIT_8 newd);
	void carry(BIT_8 old, BIT_8 newd);
	bool isNegative(const BIT_8 &val);
	void branchToAddr(BIT_8 addr);
	
	#pragma region Instruction Set
	void ADC(const BIT_8 args[], const BIT_8 opcode);
	void AND(const BIT_8 args[], const BIT_8 opcode);
	void ASL(const BIT_8 args[], const BIT_8 opcode);
	void BCC(const BIT_8 args[], const BIT_8 opcode);
	void BCS(const BIT_8 args[], const BIT_8 opcode);
	void BEQ(const BIT_8 args[], const BIT_8 opcode);
	void BIT(const BIT_8 args[], const BIT_8 opcode);
	void BMI(const BIT_8 args[], const BIT_8 opcode);
	void BNE(const BIT_8 args[], const BIT_8 opcode);
	void BPL(const BIT_8 args[], const BIT_8 opcode);
	void BRK(const BIT_8 args[], const BIT_8 opcode);
	void BVC(const BIT_8 args[], const BIT_8 opcode);
	void BVS(const BIT_8 args[], const BIT_8 opcode);
	void CLC(const BIT_8 args[], const BIT_8 opcode);
	void CLD(const BIT_8 args[], const BIT_8 opcode);
	void CLI(const BIT_8 args[], const BIT_8 opcode);
	void CLV(const BIT_8 args[], const BIT_8 opcode);
	void CMP(const BIT_8 args[], const BIT_8 opcode);
	void CPX(const BIT_8 args[], const BIT_8 opcode);
	void CPY(const BIT_8 args[], const BIT_8 opcode);
	void DEC(const BIT_8 args[], const BIT_8 opcode);
	void DEX(const BIT_8 args[], const BIT_8 opcode);
	void DEY(const BIT_8 args[], const BIT_8 opcode);
	void EOR(const BIT_8 args[], const BIT_8 opcode);
	void INC(const BIT_8 args[], const BIT_8 opcode);
	void INX(const BIT_8 args[], const BIT_8 opcode);
	void INY(const BIT_8 args[], const BIT_8 opcode);
	void JMP(const BIT_8 args[], const BIT_8 opcode);
	void JSR(const BIT_8 args[], const BIT_8 opcode);
	void LDA(const BIT_8 args[], const BIT_8 opcode);
	void LDX(const BIT_8 args[], const BIT_8 opcode);
	void LDY(const BIT_8 args[], const BIT_8 opcode);
	void LSR(const BIT_8 args[], const BIT_8 opcode);
	void NOP(const BIT_8 args[], const BIT_8 opcode);
	void ORA(const BIT_8 args[], const BIT_8 opcode);
	void PHA(const BIT_8 args[], const BIT_8 opcode);
	void PHP(const BIT_8 args[], const BIT_8 opcode);
	void PLA(const BIT_8 args[], const BIT_8 opcode);
	void PLP(const BIT_8 args[], const BIT_8 opcode);
	void ROL(const BIT_8 args[], const BIT_8 opcode);
	void ROR(const BIT_8 args[], const BIT_8 opcode);
	void RTI(const BIT_8 args[], const BIT_8 opcode);
	void RTS(const BIT_8 args[], const BIT_8 opcode);
	void SBC(const BIT_8 args[], const BIT_8 opcode);
	void SEC(const BIT_8 args[], const BIT_8 opcode);
	void SED(const BIT_8 args[], const BIT_8 opcode);
	void SEI(const BIT_8 args[], const BIT_8 opcode);
	void STA(const BIT_8 args[], const BIT_8 opcode);
	void STX(const BIT_8 args[], const BIT_8 opcode);
	void STY(const BIT_8 args[], const BIT_8 opcode);
	void TAX(const BIT_8 args[], const BIT_8 opcode);
	void TAY(const BIT_8 args[], const BIT_8 opcode);
	void TSX(const BIT_8 args[], const BIT_8 opcode);
	void TXA(const BIT_8 args[], const BIT_8 opcode);
	void TXS(const BIT_8 args[], const BIT_8 opcode);
	void TYA(const BIT_8 args[], const BIT_8 opcode);
	void Illegal(const BIT_8 args[], const BIT_8 opcode);
	#pragma endregion
};

#endif