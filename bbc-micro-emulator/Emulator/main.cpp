#include <iostream>
#include <Windows.h>

#include "MPU.h"
#include "Constants.h"
#include "ReadFile.h"

BIT_8 testMem[KB64] = {0};
BIT_8 testROM[KB16];

void time_it(void (*t)())
{
	DWORD cTick, nTick;
    cTick = GetTickCount();

	(*t)();
	
	nTick = GetTickCount();
	std::cout << "ntick: " << nTick << "\n" << std::endl;
    std::cout << (nTick - cTick) << std::endl;
}

void test1()
{
	for(int i = 0, j = 0; i < KB16; i++)
	{
		testROM[i] = j;
		j++;

		if((j % 256) == 0)
		{
			j = 0;
		}		
	}
	//std::cin.get();
	for(int y = 0; y < 1000000; y++)
	{
		for(int x = ROM_START_ADDR; x < (KB16 + ROM_START_ADDR); x++)
		{
			testMem[x] = testROM[x - ROM_START_ADDR - 1];
		}
	}
}

//int main (int argc, char* argv[]) 
//{
//	MPU CPU = MPU();
//	
//	std::cout << "Registers\n";
//	//
//	std::cout << "A: " << (int)CPU.A << "\n";
//	std::cout << "X: " << (int)CPU.X << "\n";
//	std::cout << "Y: " << (int)CPU.Y << "\n";
//
//	std::cout << "Put 5 in X then call STX\n";
//
//	CPU.X = 5;
//	BIT_8 arr[1] = {0x00};
//	CPU.STX(arr, 0x86);
//
//	std::cout << "0: " << (int)CPU.memory[0] << "\n";
//
//	std::cout << "Call STX with absolute address 0x0001\n";
//
//	BIT_8 arr2[2] = {0x00,0x01};
//	CPU.STX(arr2, 0x8E);
//
//	std::cout << "1: " << (int)CPU.memory[1] << "\n";
//
//	std::cout << "Call STX with 0 page, 0x01, Y = 10\n";
//
//	CPU.Y = 10;
//	BIT_8 arr3[1] = {0x01};
//	CPU.STX(arr3, 0x96);
//
//	std::cout << "11: " << (int)CPU.memory[11] << "\n";
//
//	CPU.addToStack(CPU.memory[11]);
//
//	std::cout << "Stack: " << (int)CPU.popFromStack() << "\n";
//	std::cout << "Stack: " << (int)CPU.popFromStack() << "\n";
//
//	//time_it(&test1);
//
//	std::cout << "**********" << std::endl;
//
//	CPU.memory.loadOS(readFile(""));
//
//	for(int j = KB16-1; j < KB32; j++)
//	{
//		std::cout << CPU.memory[j];
//	}
//
//	std::cin.get();
//
//	return 0;
//}
