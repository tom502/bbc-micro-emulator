﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Assembler.Controls
{
    public partial class CustomTextBox : UserControl
    {
        private int iOldLength;
        private SyntaxHighlighter sh;
        private int iLineCount;
        public int LineCount
        {
            get { return iLineCount; }
            set { iLineCount = value; }
        }

        private bool bChangedSinceSave;
        public bool ChangedSinceSave
        {
            get { return bChangedSinceSave; }
            set { bChangedSinceSave = value; }
        }
        
        public CustomTextBox()
        {
            sh = new SyntaxHighlighter("LanguageData.xml");
            iOldLength = 3;
            InitializeComponent();
            numberLabel.Font = new Font(textBox.Font.FontFamily, textBox.Font.Size);
            bChangedSinceSave = false;
        }

        private void updateNumberLabel()
        {
            //we get index of first visible char and number of first visible line
            Point pos = new Point(0, 0);
            int firstIndex = textBox.GetCharIndexFromPosition(pos);
            int firstLine = textBox.GetLineFromCharIndex(firstIndex);

            //now we get index of last visible char and number of last visible line
            pos.X = ClientRectangle.Width;
            pos.Y = ClientRectangle.Height;
            int lastIndex = textBox.GetCharIndexFromPosition(pos);
            int lastLine = textBox.GetLineFromCharIndex(lastIndex);

            //this is point position of last visible char, we'll use its Y value for calculating numberLabel size
            pos = textBox.GetPositionFromCharIndex(lastIndex);


            //finally, renumber label
            numberLabel.Text = "";
            LineCount = 0;
            for (LineCount = firstLine; LineCount <= lastLine + 1; LineCount++)
            {
                numberLabel.Text += LineCount + 1 + "\n";
            }
            if (LineCount.ToString().Length > iOldLength)
            {
                iOldLength = LineCount.ToString().Length;
                splitContainer1.SplitterDistance += iOldLength + 2;
            }

        }
        
        private void textBox_VScroll(object sender, EventArgs e)
        {
            int d = textBox.GetPositionFromCharIndex(0).Y % (textBox.Font.Height + 1);
            numberLabel.Location = new Point(0, d);

            updateNumberLabel();
        }
        private void textBox_Resize(object sender, EventArgs e)
        {
            textBox_VScroll(null, null);
        }
        private void textBox_FontChanged(object sender, EventArgs e)
        {
            updateNumberLabel();
            textBox_VScroll(null, null);
        }
        private void textBox_TextChanged(object sender, EventArgs e)
        {
            updateNumberLabel();
            bChangedSinceSave = true;
        }
        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            int cursorIndex = textBox.SelectionStart;
            Point Pos = new Point(0, 0);
            int index;
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.Down)
            {
                Pos.X = ClientRectangle.Width;
                Pos.Y = ClientRectangle.Height;
                index = textBox.GetCharIndexFromPosition(Pos) + 1;
                if (cursorIndex == index)
                    e.SuppressKeyPress = true;
            }

            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Up)
            {
                index = textBox.GetCharIndexFromPosition(Pos);
                if (cursorIndex == index)
                    e.SuppressKeyPress = true;
            }

        }
                
        public void SetText(string str)
        {
            textBox.Text = str;
            bChangedSinceSave = false;
        }
        public void Save(string fname)
        {
            AdvancedNotepad.FileHelper.StringToFile(textBox.Text,fname);
            bChangedSinceSave = false;
        }
        
        public void Paste()
        {
            if(Clipboard.ContainsText())
            {
                textBox.Paste();
            }
        }
        public void Copy()
        {
            textBox.Copy();
        }
        public void Cut()
        {
            textBox.Cut();
        }

        private void CustomTextBoxUC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (IsTerminalChar(e))
            { }
        }

        private bool IsTerminalChar(KeyPressEventArgs e)
        {
            bool ret = false;
            char[] list = new char[] {'"',' ','{','a'};
            foreach (char item in list)
            {
                if (item == e.KeyChar)
                {
                    MessageBox.Show("Test");
                }
            }
            return ret;
        }
    }
}
