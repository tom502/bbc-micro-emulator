﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Assembler.Controls
{
    public partial class AssemblerTextBox : UserControl
    {
        public AssemblerTextBox()
        {
            InitializeComponent();
        }

        private void splitContainer1_Resize(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = splitContainer1.Width - (splitContainer1.Width / 4);
        }
    }
}
