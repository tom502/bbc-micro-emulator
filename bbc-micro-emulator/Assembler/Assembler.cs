﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Assembler.Controls;

namespace Assembler
{
    public partial class Assembler : Form
    {
        public Assembler()
        {
            AssemblerTextBox atb = new AssemblerTextBox();
            this.Controls.Add(atb);
            atb.Dock = DockStyle.Fill;
            InitializeComponent();
        }
    }
}
