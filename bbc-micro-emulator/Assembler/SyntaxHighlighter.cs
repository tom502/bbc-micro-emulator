﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Assembler
{
	public class SyntaxHighlighter
	{
		private Dictionary<string, List<HighlightingRule>> KeywordDictionary;

		private class HighlightingRule
		{
			public string token;
			public int descriptor_type;
			public int descriptor_recognition;
			public Color colour;
			public string font;
			public bool auto_complete;
			public HighlightingRule next;
			public HighlightingRule()
			{

			}
			public HighlightingRule(string tk, int dt, int dr, Color col, string font, bool autocomplete)
			{
				token = tk;
				descriptor_type = dt;
				descriptor_recognition = dr;
				colour = col;
				this.font = font;
				auto_complete = autocomplete;
			}
		}

		public SyntaxHighlighter(string path)
		{
			//CreateDictionary(path);
		}

		public void CreateDictionary(string filepath)
		{
			KeywordDictionary = new Dictionary<string, List<HighlightingRule>>();
			XmlTextReader xmlrdr = new XmlTextReader(filepath);
			List<HighlightingRule> newList;
			string[] tokens = new string[10];
			int desc_type = 1;
			int desc_rec = 10;
			Color col = Color.FromName("Blue");
			string font = "";
			bool auto = false;

			if (xmlrdr.Name == "Keywords")
			{
				tokens = xmlrdr.Value.Split(',');
			}
			newList = new List<HighlightingRule>();
			foreach (string tk in tokens)
			{
				//add keyword to dictionary
				newList.Add(new HighlightingRule(tk, desc_type, desc_rec, col, font, auto));
			}
			KeywordDictionary.Add("Keywords", newList);
		}

		public HighlightingRule Lookup(string word)
		{
			foreach (HighlightingRule hr in KeywordDictionary["Keywords"])
			{
				if (hr.token == word)
				{
					return hr;
				}
			}

			return null;
		}
	}
}
